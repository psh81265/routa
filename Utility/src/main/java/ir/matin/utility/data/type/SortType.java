package ir.matin.utility.data.type;

public enum SortType {

    ASCENDING("asc"),
    DESCENDING("desc");

    private final String value;

    SortType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
