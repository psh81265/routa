package ir.matin.utility.data.object;

import java.io.Serializable;

/**
 * Rest Result Class,
 * All Rest Methods Have Return Type of RestResult
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class RestResult<T> implements Serializable {
    private T object;
    private SystemError error;
    private String description;

    public Object getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
        this.error = null;
    }

    public SystemError getError() {
        return error;
    }

    public void setError(SystemError error) {
        this.object = null;
        this.error = error;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


