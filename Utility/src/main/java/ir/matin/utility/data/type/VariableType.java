package ir.matin.utility.data.type;

public enum VariableType {
    Boolean(1),
    Character(2),
    Integer(3),
    Double(4),
    String(5),
    Object(6);

    private final int value;

    VariableType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
