package ir.matin.utility.data.type;

public enum DefaultResult {

    SUCCESS(true),
    FAILURE(false);

    private final Boolean value;

    DefaultResult(Boolean value) {
        this.value = value;
    }

    public Boolean getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
