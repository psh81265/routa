package ir.matin.utility.data.type;

public enum TimePeriodType {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
