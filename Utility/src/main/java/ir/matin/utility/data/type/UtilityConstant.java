package ir.matin.utility.data.type;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

public class UtilityConstant {

    public static final String MOBILE_PATTERN = "[0][9][0-9]{9}";
    public static final String PHONE_PATTERN = "[0][1-9]{2}[0-9]{8}";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String ZIP_CODE_PATTERN = "[0-9]{10}";

    public static final String ENGLISH_STRING_PATTERN = "(?=.*[a-zA-Z ]).{4,}";
    public static final String PERSIAN_STRING_PATTERN = "(?=.*[\\u0600-\\u06FF\\uFB8A\\u067E\\u0686\\u06AF\\u200C\\u200F ]).{4,}";

    public static final String BASE_IMAGE_FOLDER = "/opt/Routaa/images";

    public static final Integer DEFAULT_REPORT_PAGE_SIZE = 10;
    public static final Integer DEFAULT_REPORT_PAGE_NUMBER = 1;

    public static final String HASH_FUNCTION = "SHA-256";
    public static final String GOOGLE_API_KEY = "AIzaSyDh3yzsorXDyP-gAlxEGUlzmfJ6nVxizUw";
    public static final String SMS_SERVICE_URL_PREFIX = "http://panel.gamamn.ir:8080/url/send.aspx?username=y.nasr&password=123qwe&farsi=true&from=10006315821732";

    private UtilityConstant() {
        throw new SystemException(SystemError.ILLEGAL_CONSTRUCTOR, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
    }
}
