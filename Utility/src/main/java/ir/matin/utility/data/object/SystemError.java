package ir.matin.utility.data.object;

/**
 * System Error Enum,
 * All System Errors Types are Declared in SystemError Enum
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public enum SystemError {

    /**
     * General
     */
    SERVER_ERROR("خطای داخلی سرور"),
    REQUEST_EXPIRED("زمان درخواست شما منقضی شده است"),
    REQUIRED_FIELDS_NOT_SET("اطلاعات کافی ارسال نشده است"),

    ILLEGAL_CONSTRUCTOR(""),
    ILLEGAL_ARGUMENT(""),
    INVALID_DATA_TYPE("نوع داده ورودی معتبر نیست"),
    DATA_NOT_FOUND("داده مورد نظر موجود نیست"),
    EMPTY_RESULTS("داده مورد نظر موجود نیست"),
    IDENTIFIER_ALREADY_EXIST("شناسه مورد نظر قبلا ثبت شده است"),

    /**
     * Utility
     */
    HASH_FUNCTION_FAILED(""),
    SLEEP_FUNCTION_FAILED(""),
    UPLOADED_FILE_CORRUPTED("فایل آپلود شده مشکل دارد"),
    STORE_FILE_FAILED(""),
    EMAIL_ADDRESS_NOT_VALID("آدرس پست الکترونیکی اشتباه است"),
    EMAIL_SEND_FAILED("ارسال پست الکترونیکی دچار خطا شد"),
    SMS_WEB_SERVICE_NOT_AVAILABLE(""),
    SMS_NOT_RELIABLE(""),
    GOOGLE_API_EXCEPTION(""),
    GOOGLE_NOT_RESPONSE(""),

    /**
     * Database
     */
    STORED_PROCEDURE_INVALID_PARAMETER(""),
    STORED_PROCEDURE_FAILED(""),
    INVALID_DATABASE_DATA(""),

    /**
     * Security
     */
    ACCESS_DENIED("عدم دسترسی"),
    LOGICAL_UN_AUTHORIZED("دسترسی غیر مجاز"),

    TOKEN_CREATION_FAILED(""),
    TOKEN_VERIFICATION_INVALID_TYPE("نوع عبارت امنیتی معتبر نیست"),
    TOKEN_VERIFICATION_EXPIRED("عبارت امنیتی منقضی شده است"),
    TOKEN_VERIFICATION_FAILED("عبارت امنیتی معتبر نیست"),

    INVALID_TOKEN_HEADER("عبارت امنیتی معتبر نیست"),
    INVALID_CLIENT_SERIAL_NUMBER("شماره سریال دستگاه کاربر معتبر نیست"),
    INVALID_CLIENT_TYPE("نوع دستگاه کاربر معتبر نیست"),

    INVALID_VALIDATION_CODE("کد تاییدیه معتبر نیست"),
    VALIDATION_CODE_EXPIRED("کد تاییدیه منقضی شده است"),
    USER_ALREADY_LOGGED_IN("کاربر درحال حاضر لاگین می باشد"),
    USER_BLOCKED("کاربر مورد نظر بلاک شده است"),
    USER_NOT_CONFIRM("کاربر مورد نظر مورد تایید نیست"),
    USER_NOT_ACTIVE("کاربر مورد نظر فعال نیست"),
    USER_NOT_LOGIN("کاربر مورد نظر لاگین نیست"),
    USER_NOT_FOUND("کاربر مورد نظر یافت نشد"),

    /**
     * User
     */
    USERNAME_ALREADY_EXIST("نام کاربری مورد نظر قبلا ثبت شده است"),
    USERNAME_PASSWORD_NOT_MATCH("نام کاربری یا رمز عبور اشتباه است"),

    /**
     * Elastic
     */
    ELASTIC_NOT_RESPOND(""),
    ELASTIC_CONNECTION(""),
    ELASTIC_INTERRUPTED(""),
    ELASTIC_EXECUTION_EXCEPTION(""),

    /**
     * Retail
     */
    DUPLICATE_REQUEST(""),
    ILLEGAL_REQUEST(""),
    UN_SUPPORTED_REQUEST(""),
    INVALID_USER_REQUEST("");

    /**
     * HttpServlet Status *
     * NoContent(204, ""),
     * BadRequest(400, ""),
     * Unauthorized(401, ""),
     * Forbidden(403, "Request is Forbidden"),
     * NotFound(404, "Page not Found"),
     * UnsupportedMediaType(415, ""),
     * InternalServerError(500, ""),
     * ServiceUnavailable(503, ""),
     */

    private final String value;

    SystemError(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
