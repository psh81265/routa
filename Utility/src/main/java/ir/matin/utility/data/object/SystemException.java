package ir.matin.utility.data.object;

/**
 * System Exception Class,
 * A Customized {@link RuntimeException} for Whole System with a proper {@link SystemError} Type
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class SystemException extends RuntimeException {
    private final SystemError userError;
    private final SystemError systemError;
    private final String description;
    private String argument;


    public SystemException(SystemError error, String description) {
        super(description);
        this.systemError = error;
        this.userError = error;
        this.description = description;
    }

    public SystemException(SystemError error, String argument, String description) {
        super(description);
        this.systemError = error;
        this.userError = error;
        this.argument = argument;
        this.description = description;
    }

    public SystemException(SystemError systemError, SystemError userError, String description) {
        super(description);
        this.systemError = systemError;
        this.userError = userError;
        this.description = description;
    }

    public SystemException(SystemError systemError, SystemError userError, String argument, String description) {
        super(description);
        this.systemError = systemError;
        this.userError = userError;
        this.argument = argument;
        this.description = description;
    }

    public SystemError getUserError() {
        return userError;
    }

    public SystemError getSystemError() {
        return systemError;
    }

    public String getArgument() {
        return argument;
    }

    public String getDescription() {
        return description;
    }
}
