package ir.matin.utility.data.dto;

import java.io.Serializable;
import java.util.List;

public class ReportDto<T> implements Serializable {
    private List<T> result;
    private Integer countAll;
    private Integer pageSize;
    private Integer pageNumber;

    public ReportDto() {
    }

    public ReportDto(Integer countAll, Integer pageSize, Integer pageNumber) {
        this.result = null;
        this.countAll = countAll;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public Integer getCountAll() {
        return countAll;
    }

    public void setCountAll(Integer countAll) {
        this.countAll = countAll;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }
}
