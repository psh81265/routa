package ir.matin.utility.data.object;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;


public class MailData {

    private String subject;
    private String content;
    //    private InternetAddress sender;
    private InternetAddress[] recipients;
    private InternetAddress[] CCs;
    private InternetAddress[] BCCs;
    private String[] attachments;

    public MailData(String subject, String content, String[] recipientsAddresses) {
        this.subject = subject;
        this.content = content;
        if (recipientsAddresses.length <= 0)
            throw new SystemException(SystemError.EMAIL_ADDRESS_NOT_VALID, "recipientsAddresses", SystemError.EMAIL_ADDRESS_NOT_VALID.getValue());

        try {
            this.recipients = new InternetAddress[recipientsAddresses.length];
            for (int i = 0; i < recipientsAddresses.length; i++) {
                this.recipients[i] = new InternetAddress(recipientsAddresses[i]);
            }
            this.CCs = null;
            this.BCCs = null;
        } catch (MessagingException e) {
            throw new SystemException(SystemError.EMAIL_ADDRESS_NOT_VALID, "recipientsAddresses", SystemError.EMAIL_ADDRESS_NOT_VALID.getValue());
        }
    }

    public MailData(String subject, String content, String[] recipientsAddresses, String[] attachments) {
        this(subject, content, recipientsAddresses);
        this.attachments = attachments;
    }

    public MailData(String subject, String content, String[] recipientsAddresses,
                    String[] CCsAddresses, String[] BCCsAddresses) {
        this(subject, content, recipientsAddresses);

        try {
            this.CCs = new InternetAddress[CCsAddresses.length];
            for (int i = 0; i < CCsAddresses.length; i++) {
                this.CCs[i] = new InternetAddress(CCsAddresses[i]);
            }
            this.BCCs = new InternetAddress[BCCsAddresses.length];
            for (int i = 0; i < BCCsAddresses.length; i++) {
                this.BCCs[i] = new InternetAddress(BCCsAddresses[i]);
            }
        } catch (MessagingException e) {
            throw new SystemException(SystemError.EMAIL_ADDRESS_NOT_VALID, "recipientsAddresses", SystemError.EMAIL_ADDRESS_NOT_VALID.getValue());
        }
    }

    public MailData(String subject, String content, String[] recipientsAddresses,
                    String[] CCsAddresses, String[] BCCsAddresses, String[] attachments) {
        this(subject, content, recipientsAddresses, CCsAddresses, BCCsAddresses);
        this.attachments = attachments;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public InternetAddress[] getRecipients() {
        return recipients;
    }

    public void setRecipients(InternetAddress[] recipients) {
        this.recipients = recipients;
    }

    public InternetAddress[] getCCs() {
        return CCs;
    }

    public void setCCs(InternetAddress[] CCs) {
        this.CCs = CCs;
    }

    public InternetAddress[] getBCCs() {
        return BCCs;
    }

    public void setBCCs(InternetAddress[] BCCs) {
        this.BCCs = BCCs;
    }

    public String[] getAttachments() {
        return attachments;
    }

    public void setAttachments(String[] attachments) {
        this.attachments = attachments;
    }
}
