package ir.matin.utility.data.type;

public enum EditionStatusType {

    DELETE(-1),
    UPDATE(1),
    SAVE(10);

    private final int value;

    EditionStatusType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
