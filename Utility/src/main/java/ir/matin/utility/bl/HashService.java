package ir.matin.utility.bl;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.UtilityConstant;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * The Hash Service Class,
 * Containing Methods about Hashing and Base64 Encoding
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class HashService {

    /**
     * Get Secure Hashed Version of Input Value
     *
     * @param value A {@link String} Instance Representing Input Value
     * @return A {@link String} Instance Representing Hashed Value of Input Value
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#HASH_FUNCTION_FAILED} when Hash Algorithm does not Support
     */
    public String hash(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(UtilityConstant.HASH_FUNCTION);
            messageDigest.update(value.getBytes());
            return this.encode(messageDigest.digest());
        } catch (NoSuchAlgorithmException | RuntimeException e) {
            throw new SystemException(SystemError.HASH_FUNCTION_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    /**
     * Encode Input Value with Base64 Algorithm
     *
     * @param value A {@link byte[]} Instance Representing Input Value
     * @return A {@link String} Instance Representing Base64 Encoded Value of Input Value
     */
    public String encode(byte[] value) {
        return Base64.getEncoder().withoutPadding().encodeToString(value);
    }

    /**
     * Decode Input Value with Base64 Algorithm
     *
     * @param value A {@link String} Instance Representing Input Value
     * @return A {@link byte[]} Instance Representing Base64 Decoded Value of Input Value
     */
    public byte[] decode(String value) {
        return Base64.getDecoder().decode(value);
    }
}
