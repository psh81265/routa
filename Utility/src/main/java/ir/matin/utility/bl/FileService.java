package ir.matin.utility.bl;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * The File Service Class,
 * Containing Methods about File Management
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class FileService {

    /**
     * Store a File with a Specified Name in a Folder in Server
     *
     * @param file   A {@link MultipartFile} Instance Representing Input File
     * @param folder A {@link String} Instance Representing Folder Containing the File
     * @return A {@link String} Instance Representing Name of File in Server
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#ILLEGAL_ARGUMENT} when Input Data Not Set
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#UPLOADED_FILE_CORRUPTED} when Input File is Not Valid
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#STORE_FILE_FAILED} when Email Addresses is Not Valid
     */
    public String storeFile(MultipartFile file, Integer id, String folder) {
        if (file == null || id == null || folder == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        if (file.isEmpty())
            throw new SystemException(SystemError.UPLOADED_FILE_CORRUPTED, SystemError.UPLOADED_FILE_CORRUPTED.getValue());

        try {
            String extension = FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase();
            String fileName = id.toString() + "." + extension;
            Path rootLocation = Paths.get(folder);
            Files.copy(file.getInputStream(), rootLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
            return fileName;
        } catch (IOException e) {
            throw new SystemException(SystemError.STORE_FILE_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    public InputStream loadResourceAsStream(String name) {
        if (name == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        return this.getClass().getClassLoader().getResourceAsStream("/" + name);
    }

    /**
     * Load a File with a Specified Name in Resource Folder
     *
     * @param name A {@link String} Instance Representing Name of File
     * @return A {@link File} Instance Representing Desired File
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#ILLEGAL_ARGUMENT} when Input Data Not Set
     */
    public File loadResourceAsFile(String name) {
        if (name == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        return new File(this.getClass().getClassLoader().getResource("/" + name).getFile());
    }

    /**
     * Load a File in Given Path
     *
     * @param path A {@link String} Instance Representing Path and Name of File
     * @return A {@link File} Instance Representing Desired File
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#ILLEGAL_ARGUMENT} when Input Data Not Set
     */
    public File loadFile(String path) {
        if (path == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        return Paths.get(path).toFile();
    }

}
