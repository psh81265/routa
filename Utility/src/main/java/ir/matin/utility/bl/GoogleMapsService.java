package ir.matin.utility.bl;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.LatLng;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.UtilityConstant;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

@Service
public class GoogleMapsService {

    public DirectionsResult routeWithOptimizeWayPoints(LatLng source, LatLng[] midLatLng) {
        try {
            GeoApiContext context = new GeoApiContext.Builder()
                    .apiKey(UtilityConstant.GOOGLE_API_KEY)
                    .build();
            DateTime now = new DateTime();
            return DirectionsApi.newRequest(context)
                    .region("ir")
                    .origin(source)
                    .destination(source)
                    .optimizeWaypoints(true)
                    .waypoints(midLatLng)
                    .departureTime(now).await();
        } catch (Exception e) {
            throw new SystemException(SystemError.GOOGLE_NOT_RESPONSE, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    public int[] getRoutePointsOrder(LatLng source, LatLng[] midLatLng) {
        DirectionsResult result = routeWithOptimizeWayPoints(source, midLatLng);
        if (result == null)
            throw new SystemException(SystemError.GOOGLE_NOT_RESPONSE, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        if (result.routes.length == 0)
            throw new SystemException(SystemError.GOOGLE_NOT_RESPONSE, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        return result.routes[0].waypointOrder;
    }
}
