package ir.matin.utility.bl;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.TimePeriodType;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class TimeService {

    /**
     * Get Time Value of Desired Time Period Type
     *
     * @param timePeriodType A {@link TimePeriodType} Instance Representing Type of Time Period
     * @return A {@link Timestamp} Instance Representing Desired Time Value
     */
    public Timestamp fromTime(TimePeriodType timePeriodType) {
        Timestamp time = Timestamp.valueOf(LocalDateTime.now());
        switch (timePeriodType) {
            case DAILY:
                time = Timestamp.valueOf(LocalDateTime.now().minusHours(24));
                break;
            case WEEKLY:
                time = Timestamp.valueOf(LocalDateTime.now().minusDays(7));
                break;
            case MONTHLY:
                time = Timestamp.valueOf(LocalDateTime.now().minusDays(30));
                break;
            case YEARLY:
                time = Timestamp.valueOf(LocalDateTime.now().minusDays(365));
                break;
        }
        return time;
    }

    /**
     * Causes the Currently Executing Thread to Sleep for the Specified Number of Milliseconds
     *
     * @param seconds A {@link long} Instance Representing Number of Milliseconds
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#SLEEP_FUNCTION_FAILED} when Currently Executing Thread can not Sleep
     */
    public void sleep(long seconds) {
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            throw new SystemException(SystemError.SLEEP_FUNCTION_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }
}
