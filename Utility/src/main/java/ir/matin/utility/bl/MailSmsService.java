package ir.matin.utility.bl;

import ir.matin.utility.data.object.MailData;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.UtilityConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.concurrent.Future;

/**
 * The Mail Service Class,
 * Containing Methods about Mail Management
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class MailSmsService {

    /**
     * The {@link ValidationService} Instance Representing Validation Service
     */
    @Autowired
    private ValidationService validationService;

    /**
     * The {@link FileService} Instance Representing File Service
     */
    @Autowired
    private FileService fileService;

    /**
     * The {@link JavaMailSender} Instance Representing Mail Sender
     */
    @Autowired
    private JavaMailSender mailSender;

    /**
     * Send a Code to a User's Mobile Number with www.gamamn.ir Sms Web Service
     * <ul><li> Possible {@link SystemException} in Function Call of {@link ValidationService#validateLong(String)} </li></ul>
     *
     * @param mobile  A {@link String} Instance Representing User's Mobile Number
     * @param message An {@link Integer} Instance Representing Message which Must be Send
     * @return A {@link Long} Instance Representing Id of Sent SMS
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#SMS_WEB_SERVICE_NOT_AVAILABLE} when SMS Web Service couldn't Reached
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#SMS_NOT_RELIABLE} when SMS Web Service won't Return Id of SMS
     * @see <a href="http://www.gamamn.ir">http://www.gamamn.ir</a>
     */
    @Async
    public Future<Long> sendSms(String mobile, String message) {
        if (mobile == null || mobile.equals("") || message == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

        String url = UtilityConstant.SMS_SERVICE_URL_PREFIX + "&to=" + mobile + "&message=" + message;
        RestTemplate restTemplate = new RestTemplate();
        String result;
        try {
            result = restTemplate.getForObject(url, String.class);
        } catch (RestClientException e) {
            throw new SystemException(SystemError.SMS_WEB_SERVICE_NOT_AVAILABLE, SystemError.SERVER_ERROR, "url", SystemError.SERVER_ERROR.getValue());
        }

        int beginIndex = result.indexOf("<ReturnIDs>");
        if (beginIndex == -1)
            throw new SystemException(SystemError.SMS_NOT_RELIABLE, SystemError.SERVER_ERROR, "ReturnIDs", SystemError.SERVER_ERROR.getValue());

        int endIndex = result.indexOf("</ReturnIDs>");
        try {
            Long trackingCode = this.validationService.validateLong(result.substring(beginIndex + 11, endIndex));
            return new AsyncResult<>(trackingCode);
        } catch (SystemException e) {
            throw new SystemException(SystemError.SMS_NOT_RELIABLE, SystemError.SERVER_ERROR, "trackingCode", SystemError.SERVER_ERROR.getValue());
        }
    }

    /**
     * Send A Simple Text Mail to Recipients
     *
     * @param mailData A {@link MailData} Instance Representing Mail Data
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#ILLEGAL_ARGUMENT} when Mail Data Not Set
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#EMAIL_ADDRESS_NOT_VALID} when Email Addresses is Not Valid
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#EMAIL_SEND_FAILED} when Sending Email Failed
     */
    @Async
    public void sendMailText(MailData mailData) {
        try {
            if (mailData == null || mailData.getRecipients() == null || mailData.getSubject() == null || mailData.getContent() == null)
                throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, false, "UTF-8");

            helper.setTo(mailData.getRecipients());
            if (mailData.getCCs() != null)
                helper.setCc(mailData.getCCs());
            if (mailData.getBCCs() != null)
                helper.setBcc(mailData.getBCCs());
            helper.setSubject(mailData.getSubject());
            helper.setText(mailData.getContent(), true);

            this.mailSender.send(message);
        } catch (MessagingException e) {
            throw new SystemException(SystemError.EMAIL_ADDRESS_NOT_VALID, SystemError.EMAIL_ADDRESS_NOT_VALID.getValue());
        } catch (MailException e) {
            throw new SystemException(SystemError.EMAIL_SEND_FAILED, SystemError.EMAIL_SEND_FAILED.getValue());
        }
    }

    /**
     * Send A Customized Mail with possible Attachments to Recipients
     * <br />Possible {@link SystemException} in Function Call of {@link FileService#loadResourceAsFile(String)}
     *
     * @param mailData A {@link MailData} Instance Representing Mail Data
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#ILLEGAL_ARGUMENT} when Mail Data Not Set
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#EMAIL_ADDRESS_NOT_VALID} when Email Addresses is Not Valid
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#EMAIL_SEND_FAILED} when Sending Email Failed
     */
    @Async
    public void sendMailMultimedia(MailData mailData) {
        try {
            if (mailData == null || mailData.getRecipients() == null || mailData.getSubject() == null || mailData.getContent() == null)
                throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

            MimeMessage message = this.mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");

            helper.setTo(mailData.getRecipients());
            if (mailData.getCCs() != null)
                helper.setCc(mailData.getCCs());
            if (mailData.getBCCs() != null)
                helper.setBcc(mailData.getBCCs());
            helper.setSubject(mailData.getSubject());

            String content = "<p style='direction: rtl'>سلام <img style='float: left; width: 50px; height: 50px;' src='cid:logo'></p><p>" + mailData.getContent() + "</p>";
            File logo = this.fileService.loadResourceAsFile("logo.png");
            helper.setText(content, true);
            helper.addInline("logo", logo);

            if (mailData.getAttachments() != null) {
                for (String path : mailData.getAttachments()) {
                    File file = this.fileService.loadFile(path);
                    helper.addAttachment(file.getName(), file);
                }
            }

            this.mailSender.send(message);
        } catch (MessagingException e) {
            throw new SystemException(SystemError.EMAIL_ADDRESS_NOT_VALID, SystemError.EMAIL_ADDRESS_NOT_VALID.getValue());
        } catch (MailException e) {
            throw new SystemException(SystemError.EMAIL_SEND_FAILED, SystemError.EMAIL_SEND_FAILED.getValue());
        }
    }
}
