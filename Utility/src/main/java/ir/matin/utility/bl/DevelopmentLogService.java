package ir.matin.utility.bl;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

public class DevelopmentLogService {

    private DevelopmentLogService() {
        throw new SystemException(SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
    }

    public static String createConfigLogPattern(String methodName, String description) {
        return "method:" + methodName + " description:" + description + " ";
    }

    public static String createCriticalLogPattern(String className, String methodName, String argument) {
        return className + " " + methodName + " " + argument + " ";
    }

    public static String createServerErrorLogPattern(String className, String methodName, String message) {
        return className + " " + methodName + " " + message + " ";
    }

    public static String createAuthLogPattern(String methodName, String type) {
        return "method:" + methodName + " type:" + type + " ";
    }
}
