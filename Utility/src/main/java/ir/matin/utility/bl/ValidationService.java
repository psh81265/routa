package ir.matin.utility.bl;

import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SortOption;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.CertificateType;
import ir.matin.utility.data.type.SortType;
import ir.matin.utility.data.type.TimePeriodType;
import ir.matin.utility.data.type.UtilityConstant;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * The Validation Service Class,
 * Containing Methods about String Patterns and Input Validation
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class ValidationService {

    public <T> List<String> fieldNames(Class<T> cls) {
        List<String> names = new ArrayList<>();
        if (cls.getSuperclass() != null)
            names = this.fieldNames(cls.getSuperclass());
        Field[] fields = cls.getDeclaredFields();
        for (Field field : fields)
            names.add(field.getName());
        return names;
    }

    public Pair<Class, Object> validateGeneral(String value) {
        Integer integerValue;
        Long longValue;
        Double doubleValue;
        Timestamp timestampValue;
        try {
            integerValue = Integer.valueOf(value);
        } catch (NumberFormatException e1) {
            try {
                longValue = Long.valueOf(value);
            } catch (NumberFormatException e2) {
                try {
                    doubleValue = Double.valueOf(value);
                } catch (NumberFormatException e3) {
                    try {
                        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//                        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                        Date date = formatter.parse(value);
                        timestampValue = new Timestamp(date.getTime());
                    } catch (ParseException e) {
                        return new Pair<>(String.class, value);
                    }
                    return new Pair<>(Timestamp.class, timestampValue);
                }
                return new Pair<>(Double.class, doubleValue);
            }
            return new Pair<>(Long.class, longValue);
        }
        return new Pair<>(Integer.class, integerValue);
    }

    /* ****************************************************************************************************************** */

    public Boolean validateBoolean(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "true":
            case "1":
                return Boolean.TRUE;
            case "false":
            case "0":
                return Boolean.FALSE;
            default:
                throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    public Character validateCharacter(String value) {
        if (value == null || value.equals("") || value.length() != 1)
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value.charAt(0);
    }

    public Integer validateInteger(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        try {
            return Integer.valueOf(value);
        } catch (NumberFormatException e) {
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    public Long validateLong(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        try {
            return Long.valueOf(value);
        } catch (NumberFormatException e) {
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    public Double validateDouble(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        try {
            return Double.valueOf(value);
        } catch (NumberFormatException e) {
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    public String validateEnglishString(String value) {
//        if (literalString == null || !literalString.matches(this.literalStringPattern))
//            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    public String validatePersianString(String value) {
//        if (literalString == null || !literalString.matches(this.literalStringPattern))
//            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    public Timestamp validateTimestamp(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date date = formatter.parse(value);
            return new Timestamp(date.getTime());
        } catch (ParseException e) {
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    @SuppressWarnings("unchecked")
    public List<Integer> validateArrayInteger(Object value) {
        try {
            return (List<Integer>) value;
        } catch (ClassCastException e) {
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    @SuppressWarnings("unchecked")
    public Map<Integer, Integer> validateMapInteger(Map value) {
        try {
            return (Map<Integer, Integer>) value;
        } catch (ClassCastException e) {
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    /* ****************************************************************************************************************** */

    public SortType validateSortType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "ascending":
            case "asc":
                return SortType.ASCENDING;
            case "descending":
            case "desc":
                return SortType.DESCENDING;
            default:
                throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    public TimePeriodType validateTimePeriodType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "day":
            case "daily":
                return TimePeriodType.DAILY;
            case "week":
            case "weekly":
                return TimePeriodType.WEEKLY;
            case "month":
            case "monthly":
                return TimePeriodType.MONTHLY;
            case "year":
            case "yearly":
                return TimePeriodType.YEARLY;
            default:
                throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    /**
     * Validate Email of User
     *
     * @param value A {@link String} Instance Representing Email of User
     * @return A {@link String} Instance Representing Validated email
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_DATA_TYPE} when email is not valid
     */
    public String validateMobile(String value) {
        if (value == null || value.equals("") || !value.matches(UtilityConstant.MOBILE_PATTERN))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    public String validatePhoneFax(String value) {
        if (value == null || value.equals("") || !value.matches(UtilityConstant.PHONE_PATTERN))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    /**
     * Validate Email of User
     *
     * @param value A {@link String} Instance Representing Email of User
     * @return A {@link String} Instance Representing Validated email
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_DATA_TYPE} when email is not valid
     */
    public String validateEmail(String value) {
        Pattern pattern = Pattern.compile(UtilityConstant.EMAIL_PATTERN);
        if (value == null || value.equals("") || !pattern.matcher(value).matches())
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    /**
     * Validate Secret Serial Number
     *
     * @param value A {@link String} Instance Representing Zip Code of Address
     * @return A {@link String} Instance Representing Validated Zip Code
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_DATA_TYPE} when zipCode is not valid
     */
    public String validateZipCode(String value) {
        if (value == null || value.equals("") || !value.matches(UtilityConstant.ZIP_CODE_PATTERN))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    public CertificateType validateCertificateType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "l":
            case "license":
                return CertificateType.LICENSE;
            case "c":
            case "certificate":
                return CertificateType.CERTIFICATE;
        }
        throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
    }

    /* ****************************************************************************************************************** */

    public Pair<Class, Object> validateOptionalGeneral(Optional<String> value) {
        return value.map(this::validateGeneral).orElse(null);
    }

    public Boolean validateOptionalBoolean(Optional<String> value) {
        return value.map(this::validateBoolean).orElse(null);
    }

    public Character validateOptionalCharacter(Optional<String> value) {
        return value.map(this::validateCharacter).orElse(null);
    }

    public Integer validateOptionalInteger(Optional<String> value) {
        return value.map(this::validateInteger).orElse(null);
    }

    public Long validateOptionalLong(Optional<String> value) {
        return value.map(this::validateLong).orElse(null);
    }

    public Double validateOptionalDouble(Optional<String> value) {
        return value.map(this::validateDouble).orElse(null);
    }

    public String validateOptionalEnglishString(Optional<String> value) {
        return value.map(this::validateEnglishString).orElse(null);
    }

    public String validateOptionalPersianString(Optional<String> value) {
        return value.map(this::validatePersianString).orElse(null);
    }

    public Timestamp validateOptionalTimestamp(Optional<String> value) {
        return value.map(this::validateTimestamp).orElse(null);
    }

    /* ****************************************************************************************************************** */

    public SortType validateOptionalSortType(Optional<String> value) {
        return value.map(this::validateSortType).orElse(null);
    }

    public TimePeriodType validateOptionalTimePeriodType(Optional<String> value) {
        return value.map(this::validateTimePeriodType).orElse(null);
    }

    public String validateOptionalMobile(Optional<String> value) {
        return value.map(this::validateMobile).orElse(null);
    }

    public String validateOptionalPhoneFax(Optional<String> value) {
        return value.map(this::validatePhoneFax).orElse(null);
    }

    public String validateOptionalEmail(Optional<String> value) {
        return value.map(this::validateEmail).orElse(null);
    }

    public String validateOptionalZipCode(Optional<String> value) {
        return value.map(this::validateZipCode).orElse(null);
    }

    public CertificateType validateOptionalCertificateType(Optional<String> value) {
        return value.map(this::validateCertificateType).orElse(null);
    }

    /* ****************************************************************************************************************** */

    public Integer validatePageSize(Optional<String> value) {
        if (value == null || value.equals(""))
            return UtilityConstant.DEFAULT_REPORT_PAGE_SIZE;
        try {
            return Integer.valueOf(value.orElseGet(UtilityConstant.DEFAULT_REPORT_PAGE_SIZE::toString));
        } catch (NumberFormatException e) {
            return UtilityConstant.DEFAULT_REPORT_PAGE_SIZE;
        }
    }

    public Integer validatePageNumber(Optional<String> value) {
        if (value == null || value.equals(""))
            return UtilityConstant.DEFAULT_REPORT_PAGE_NUMBER;
        try {
            return Integer.valueOf(value.orElseGet(UtilityConstant.DEFAULT_REPORT_PAGE_NUMBER::toString));
        } catch (NumberFormatException e) {
            return UtilityConstant.DEFAULT_REPORT_PAGE_NUMBER;
        }
    }

    public List<SortOption> validateSortBy(Optional<String[]> value) {
        List<String> list = value.map(Arrays::asList).orElse(null);
        List<SortOption> result = new ArrayList<>();
        if (list != null) {
            for (String item : list) {
                Integer underline = item.indexOf("_");
                String name, typeStr;
                SortType type;
                if (underline > 0) {
                    name = item.substring(0, underline);
                    typeStr = item.substring(underline + 1);
                    type = (typeStr.toLowerCase().equals("desc")) ? SortType.DESCENDING : SortType.ASCENDING;
                    result.add(new SortOption(name, type));
                } else {
                    result.add(new SortOption(item, SortType.ASCENDING));
                }
            }
        }
        return result;
    }

    public Boolean validateExport(Optional<String> value) {
        if (value.isPresent()) {
            switch (value.get()) {
                case "true":
                case "1":
                    return Boolean.TRUE;
                case "false":
                case "0":
                    return Boolean.FALSE;
            }
        }
        return Boolean.FALSE;
    }
}
