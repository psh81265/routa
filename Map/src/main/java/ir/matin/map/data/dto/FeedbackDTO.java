package ir.matin.map.data.dto;

import ir.matin.database.entity.FeedbackEntity;

import java.io.Serializable;
import java.sql.Timestamp;

public class FeedbackDTO implements Serializable {

    private int feedbackID;
    private Integer customerID;
    private Integer typeReviewStatusID;
    private String comment;
    private Integer rate;
    private Timestamp date;
    private Double latitude;
    private Double longitude;

    public FeedbackDTO(FeedbackEntity feedbackEntity) {
        setFeedbackID(feedbackEntity.getFeedbackID());
        setCustomerID(feedbackEntity.getPersonID());
        setTypeReviewStatusID(feedbackEntity.getTypeFeedbackStatusId());
        setComment(feedbackEntity.getComment());
        setRate(feedbackEntity.getRate());
        setDate(feedbackEntity.getDate());
        setLatitude(feedbackEntity.getLatitude());
        setLongitude(feedbackEntity.getLongitude());
    }

    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public Integer getTypeReviewStatusID() {
        return typeReviewStatusID;
    }

    public void setTypeReviewStatusID(Integer typeReviewStatusID) {
        this.typeReviewStatusID = typeReviewStatusID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
