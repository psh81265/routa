package ir.matin.map.data.dto;

import ir.matin.database.entity.ProblemEntity;

import java.io.Serializable;
import java.sql.Timestamp;

public class ProblemDTO implements Serializable {

    private int problemID;
    private Integer typeProblemID;
    private Integer customerID;
    private Integer typeProblemStatusID;

    private Double latitude;
    private Double longitude;
    private Timestamp date;


    public ProblemDTO(ProblemEntity problemEntity) {
        setProblemID(problemEntity.getProblemID());
        setTypeProblemID(problemEntity.getTypeProblemID());
        setCustomerID(problemEntity.getCustomerID());
        setTypeProblemStatusID(problemEntity.getTypeProblemStatusID());
        setLatitude(problemEntity.getLatitude());
        setLongitude(problemEntity.getLongitude());
        setDate(problemEntity.getDate());
    }

    public int getProblemID() {
        return problemID;
    }

    public void setProblemID(int problemID) {
        this.problemID = problemID;
    }

    public Integer getTypeProblemID() {
        return typeProblemID;
    }

    public void setTypeProblemID(Integer typeProblemID) {
        this.typeProblemID = typeProblemID;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public Integer getTypeProblemStatusID() {
        return typeProblemStatusID;
    }

    public void setTypeProblemStatusID(Integer typeProblemStatusID) {
        this.typeProblemStatusID = typeProblemStatusID;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
