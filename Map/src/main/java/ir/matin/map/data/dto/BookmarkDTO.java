package ir.matin.map.data.dto;

import ir.matin.database.entity.BookmarkEntity;
import ir.matin.database.entity.SpecialBookmarkEntity;

import java.io.Serializable;

public class BookmarkDTO implements Serializable {
    private int bookmarkID;
    private Integer personID;
    private Integer typeBookmarkID;
    private String name;
    private Double latitude;
    private Double longitude;

    public BookmarkDTO(BookmarkEntity entity) {
        setBookmarkID(entity.getBookmarkID());
        setPersonID(entity.getPersonID());
        setLatitude(entity.getLatitude());
        setLongitude(entity.getLongitude());
        setName(entity.getName());
        setTypeBookmarkID(entity.getTypeBookmarkID());
    }

    public BookmarkDTO(SpecialBookmarkEntity entity) {
        setBookmarkID(entity.getSpecialBookmarkID());
        setPersonID(entity.getCustomerID());
        setLatitude(entity.getLatitude());
        setLongitude(entity.getLongitude());
        setName(entity.getName());
        setTypeBookmarkID(entity.getTypeBookmarkID());
    }

    public int getBookmarkID() {
        return bookmarkID;
    }

    public void setBookmarkID(int bookmarkID) {
        this.bookmarkID = bookmarkID;
    }

    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    public Integer getTypeBookmarkID() {
        return typeBookmarkID;
    }

    public void setTypeBookmarkID(Integer typeBookmarkID) {
        this.typeBookmarkID = typeBookmarkID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
