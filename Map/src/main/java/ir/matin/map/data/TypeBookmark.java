package ir.matin.map.data;

public enum TypeBookmark {
    OTHER("other"),
    HOME("home"),
    WORK("work");

    private final String value;

    TypeBookmark(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
