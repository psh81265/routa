package ir.matin.map.data.dto;

import ir.matin.database.entity.AnnouncementEntity;

import java.sql.Timestamp;

public class AnnouncementDTO {

    private int announcementID;
    private Integer typeAnnouncementID;
    private Integer customerID;
    private Integer typeAnnouncementStatusID;

    private Double latitude;
    private Double longitude;

    private Timestamp date;

    public AnnouncementDTO(AnnouncementEntity announcementEntity) {
        setAnnouncementID(announcementEntity.getAnnouncementID());
        setCustomerID(announcementEntity.getCustomerID());
        setDate(announcementEntity.getDate());
        setLatitude(announcementEntity.getLatitude());
        setLongitude(announcementEntity.getLongitude());
        setTypeAnnouncementID(announcementEntity.getTypeAnnouncementID());
        setTypeAnnouncementStatusID(announcementEntity.getTypeAnnouncementStatusID());
    }
    public int getAnnouncementID() {
        return announcementID;
    }

    public void setAnnouncementID(int announcementID) {
        this.announcementID = announcementID;
    }

    public Integer getTypeAnnouncementID() {
        return typeAnnouncementID;
    }

    public void setTypeAnnouncementID(Integer typeAnnouncementID) {
        this.typeAnnouncementID = typeAnnouncementID;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public Integer getTypeAnnouncementStatusID() {
        return typeAnnouncementStatusID;
    }

    public void setTypeAnnouncementStatusID(Integer typeAnnouncementStatusID) {
        this.typeAnnouncementStatusID = typeAnnouncementStatusID;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }
}
