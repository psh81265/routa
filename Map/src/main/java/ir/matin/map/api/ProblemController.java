package ir.matin.map.api;

import ir.matin.database.entity.PersonEntity;
import ir.matin.map.bl.ProblemService;
import ir.matin.map.data.dto.ProblemDTO;
import ir.matin.security.bl.AccessService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
@RestController
@RequestMapping(path = "/rest/map/problem")
public class ProblemController {

    @Autowired
    ProblemService problemService;

    @Autowired
    private AccessService accessService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> add(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            problemService.add(customerID, postParameters);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping(path = "/{problemID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ProblemDTO> get(@PathVariable(name = "problemID") int problemID,
                                      HttpServletRequest request) {
        RestResult<ProblemDTO> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(problemService.get(customerID, problemID));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @PutMapping(path = "/{problemID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> update(@PathVariable(name = "problemID") int problemID,
                                            @RequestBody Map<String, Object> requestParams,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            problemService.update(customerID, problemID, requestParams);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @DeleteMapping(path = "/{problemID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> delete(@PathVariable(name = "problemID") int problemID,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            problemService.delete(customerID, problemID);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping
    public RestResult<List<ProblemDTO>> getAll(HttpServletRequest request) {
        RestResult<List<ProblemDTO>> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            List<ProblemDTO> problemDTOList = problemService.getAll(customerID);
            result.setObject(problemDTOList);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

}