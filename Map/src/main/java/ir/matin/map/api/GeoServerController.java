package ir.matin.map.api;

import ir.matin.database.entity.PersonEntity;
import ir.matin.map.bl.GeoService;
import ir.matin.security.bl.AccessService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@SuppressWarnings("Duplicates")
@RestController
@RequestMapping(path = "/rest/map/geo")
public class GeoServerController {

    @Autowired
    GeoService geoService;

    @Autowired
    private AccessService accessService;


    @GetMapping(path = "/point/to/address", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Object> getAddress(@RequestParam Map<String,Object> params ,  HttpServletRequest request) {
        if (!params.containsKey("latitude"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        if (!params.containsKey("longitude"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        double latitude = Double.parseDouble(params.get("latitude").toString());
        double longitude = Double.parseDouble(params.get("longitude").toString());
        RestResult<Object> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            Object obj = geoService.getPointToAddress(latitude,longitude);
            result.setObject(obj);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/address/to/point", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Object> getPoint(@RequestParam Map<String,Object> params ,  HttpServletRequest request) {
        if (!params.containsKey("address"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        if (!params.containsKey("viewbox"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        RestResult<Object> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            Object obj = geoService.getAddressToPoint(params.get("address").toString(),params.get("viewbox").toString());
            result.setObject(obj);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/search/point", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Object> searchSimpleAddress(@RequestParam Map<String,Object> params ,  HttpServletRequest request) {
        if (!params.containsKey("latitude"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        if (!params.containsKey("longitude"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        double latitude = Double.parseDouble(params.get("latitude").toString());
        double longitude = Double.parseDouble(params.get("longitude").toString());
        RestResult<Object> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            Object obj = geoService.searchSimpleAddress(latitude,longitude);
            result.setObject(obj);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/direction", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Object> getDirection(@RequestParam Map<String,Object> params ,  HttpServletRequest request) {
        if (!params.containsKey("origin"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "orgin", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        if (!params.containsKey("destination"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "destination", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        if (!params.containsKey("oddeven"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "oddeven", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        if (!params.containsKey("trafficzone"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "trafficzone", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        String origin = params.get("origin").toString();
        String destination = params.get("destination").toString();
        boolean oddeven = (Boolean)params.get("oddeven");
        boolean trafficzone = (Boolean)params.get("trafficzone");
        RestResult<Object> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            Object obj = geoService.getDirection(origin,destination,oddeven,trafficzone);
            result.setObject(obj);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/distance", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Object> getDistance(@RequestParam Map<String,Object> params ,  HttpServletRequest request) {
        if (!params.containsKey("points"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        String points = params.get("points").toString();
        RestResult<Object> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            Object obj = geoService.getDistance(points);
            result.setObject(obj);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/map/match", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Object> getMapMathing(@RequestParam Map<String,Object> params ,  HttpServletRequest request) {
        if (!params.containsKey("points"))
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        String points = params.get("points").toString();
        RestResult<Object> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            Object obj = geoService.getMapMaching(points);
            result.setObject(obj);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}