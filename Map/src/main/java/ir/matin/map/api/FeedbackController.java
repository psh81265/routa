package ir.matin.map.api;

import ir.matin.database.entity.PersonEntity;
import ir.matin.map.bl.FeedbackService;
import ir.matin.map.data.dto.FeedbackDTO;
import ir.matin.security.bl.AccessService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
@RestController
@RequestMapping(path = "/rest/map/feedback")
public class FeedbackController {

    @Autowired
    FeedbackService feedbackService;

    @Autowired
    private AccessService accessService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> add(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            feedbackService.add(customerID, postParameters);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping(path = "/{feedbackID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<FeedbackDTO> get(@PathVariable(name = "feedbackID") int reviewID,
                                       HttpServletRequest request) {
        RestResult<FeedbackDTO> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(feedbackService.get(customerID, reviewID));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @PutMapping(path = "/{feedbackID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> update(@PathVariable(name = "feedbackID") int reviewID,
                                            @RequestBody Map<String, Object> requestParams,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            feedbackService.update(customerID, reviewID, requestParams);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @DeleteMapping(path = "/{feedbackID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> delete(@PathVariable(name = "feedbackID") int reviewID,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            feedbackService.delete(customerID, reviewID);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping
    public RestResult<List<FeedbackDTO>> getAll(HttpServletRequest request) {
        RestResult<List<FeedbackDTO>> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(feedbackService.getAll(customerID));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

}