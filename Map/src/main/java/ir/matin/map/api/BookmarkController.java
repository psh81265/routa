package ir.matin.map.api;

import ir.matin.database.entity.PersonEntity;
import ir.matin.map.bl.BookmarkService;
import ir.matin.map.data.dto.BookmarkDTO;
import ir.matin.security.bl.AccessService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
@RestController
@RequestMapping(path = "/rest/map/bookmark")
public class BookmarkController {

    @Autowired
    BookmarkService bookmarkService;

    @Autowired
    private AccessService accessService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> add(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            bookmarkService.add(customerID, postParameters);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping(path = "/{type}/{bookmarkID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<BookmarkDTO> getByBookmarkID(@PathVariable(name = "type") String type,
                                                   @PathVariable(name = "bookmarkID") int bookmarkID,
                                                   HttpServletRequest request) {
        RestResult<BookmarkDTO> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(bookmarkService.get(customerID, bookmarkID, type));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @PutMapping(path = "/{type}/{bookmarkID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> update(@PathVariable(name = "type") String type,
                                            @PathVariable(name = "bookmarkID") int bookmarkID,
                                            @RequestBody Map<String, Object> requestParams,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            bookmarkService.update(customerID, bookmarkID, type, requestParams);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @DeleteMapping(path = "/{type}/{bookmarkID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> delete(@PathVariable(name = "type") String type,
                                            @PathVariable(name = "bookmarkID") int bookmarkID,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            bookmarkService.delete(customerID, bookmarkID, type);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping(path = "/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<BookmarkDTO>> getByType(@PathVariable(name = "type") String type,
                                                   HttpServletRequest request) {
        RestResult<List<BookmarkDTO>> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(bookmarkService.getAllByType(customerID, type));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<BookmarkDTO>> getCustomerAllBookmarks(HttpServletRequest request) {
        RestResult<List<BookmarkDTO>> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(bookmarkService.getAll(customerID));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

}