package ir.matin.map.api;

import ir.matin.database.entity.PersonEntity;
import ir.matin.map.bl.AnnouncementService;
import ir.matin.map.data.dto.AnnouncementDTO;
import ir.matin.security.bl.AccessService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@SuppressWarnings("Duplicates")
@RestController
@RequestMapping(path = "/rest/map/announcement")
public class AnnouncementController {

    private AnnouncementService announcementService;
    private AccessService accessService;

    AnnouncementController(AnnouncementService announcementService, AccessService accessService) {
        this.announcementService = announcementService;
        this.accessService = accessService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> add(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            announcementService.add(customerID, postParameters);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping(path = "/{announcementID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<AnnouncementDTO> get(@PathVariable(name = "announcementID") int announcementID,
                                           HttpServletRequest request) {
        RestResult<AnnouncementDTO> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(announcementService.get(customerID, announcementID));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @PutMapping(path = "/{announcementID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> update(@PathVariable(name = "announcementID") int announcementID,
                                            @RequestBody Map<String, Object> requestParams,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            announcementService.update(customerID, announcementID, requestParams);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @DeleteMapping(path = "/{announcementID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> delete(@PathVariable(name = "announcementID") int announcementID,
                                            HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            announcementService.delete(customerID, announcementID);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @GetMapping
    public RestResult<List<AnnouncementDTO>> getAll(HttpServletRequest request) {
        RestResult<List<AnnouncementDTO>> result = new RestResult<>();
        try {
            PersonEntity personEntity = accessService.getLoggedInUser(request);
            int customerID = personEntity.getPersonid();
            result.setObject(announcementService.getAll(customerID));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

}