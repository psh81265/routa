package ir.matin.map.bl;

import ir.matin.database.dao.*;
import ir.matin.database.entity.*;
import ir.matin.map.data.TypeBookmark;
import ir.matin.map.data.dto.BookmarkDTO;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BookmarkService {

    @Autowired
    BookmarkDao bookmarkDao;

    @Autowired
    TypeBookmarkDao typeBookmarkDao;

    @Autowired
    CustomerDao personDao;

    @Autowired
    ValidationService validationService;

    @Autowired
    SpecialBookmarkDao specialBookmarkDao;

    private static final String BOOKMARK_ID = "bookmarkID";
    private static final String SPECIAL_BOOKMARK_ID = "bookmarkID";
    private static final String PERSON_ID = "personID";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String TYPE = "type";
    private static final String NAME = "name";

    public void add(int personID, Map<String, Object> map) {

        if (!map.containsKey(LATITUDE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LATITUDE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        } else if (!map.containsKey(LONGITUDE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LONGITUDE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        } else if (!map.containsKey(TYPE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, TYPE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        }

        String type = validationService.validateEnglishString(map.get(TYPE).toString().toLowerCase());
        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);
        if (typeBookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE, SystemError.DATA_NOT_FOUND.getValue());
        }

        if (typeBookmarkEntity.getName().equals(TypeBookmark.HOME.getValue()) ||
                typeBookmarkEntity.getName().equals(TypeBookmark.WORK.getValue())) {

            SpecialBookmarkEntity specialBookmarkEntity = new SpecialBookmarkEntity();
            specialBookmarkEntity.setCustomerID(personID);
            specialBookmarkEntity.setTypeBookmarkID(typeBookmarkEntity.getTypeBookmarkID());
            setParams(specialBookmarkEntity, map);
            specialBookmarkEntity.setName("home");
            specialBookmarkDao.add(specialBookmarkEntity);

        } else if (typeBookmarkEntity.getName().equals(TypeBookmark.OTHER.getValue())) {

            BookmarkEntity bookmarkEntity = new BookmarkEntity();
            bookmarkEntity.setPersonID(personID);
            bookmarkEntity.setTypeBookmarkID(typeBookmarkEntity.getTypeBookmarkID());
            setParams(bookmarkEntity, map);
            if (map.containsKey(NAME)) {
                String name = map.get(NAME).toString();
                bookmarkEntity.setName(name);
            }
            bookmarkDao.add(bookmarkEntity);

        } else {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, TYPE, SystemError.ILLEGAL_ARGUMENT.getValue());
        }
    }

    private void setParams(BookmarkEntity entity, Map<String, Object> map) {
        if (map.containsKey(NAME)) {
            String name = map.get(NAME).toString();
            entity.setName(name);
        }
        if (map.containsKey(LATITUDE)) {
            double lat = Double.parseDouble(map.get(LATITUDE).toString());
            entity.setLatitude(lat);
        }

        if (map.containsKey(LONGITUDE)) {
            double lon = Double.parseDouble(map.get(LONGITUDE).toString());
            entity.setLongitude(lon);
        }
    }

    private void setParams(SpecialBookmarkEntity entity, Map<String, Object> map) {
        if (map.containsKey(NAME)) {
            String name = map.get(NAME).toString();
            entity.setName(name);
        }
        if (map.containsKey(LATITUDE)) {
            double lat = Double.parseDouble(map.get(LATITUDE).toString());
            entity.setLatitude(lat);
        }

        if (map.containsKey(LONGITUDE)) {
            double lon = Double.parseDouble(map.get(LONGITUDE).toString());
            entity.setLongitude(lon);
        }
    }

    public BookmarkDTO get(int customerID, int bookmarkOrSpecialBookmarkID, String type) {
        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);
        if (typeBookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE, SystemError.DATA_NOT_FOUND.getValue());
        }

        if (typeBookmarkEntity.getName().equals(TypeBookmark.HOME.getValue()) ||
                typeBookmarkEntity.getName().equals(TypeBookmark.WORK.getValue())) {

            SpecialBookmarkEntity specialBookmarkEntity = specialBookmarkDao.get(bookmarkOrSpecialBookmarkID);
            if (specialBookmarkEntity.getCustomerID() != customerID) {
                throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
            }
            return new BookmarkDTO(specialBookmarkEntity);

        } else if (typeBookmarkEntity.getName().equals(TypeBookmark.OTHER.getValue())) {

            BookmarkEntity bookmarkEntity = bookmarkDao.get(bookmarkOrSpecialBookmarkID);
            if (bookmarkEntity.getPersonID() != customerID) {
                throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
            }
            return new BookmarkDTO(bookmarkEntity);

        } else
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, TYPE, SystemError.ILLEGAL_ARGUMENT.getValue());
    }

//    public SpecialBookmarkEntity getSpecialBookmarkEntity(int customerID, int specialBookmarkID) {
//        SpecialBookmarkEntity specialBookmarkEntity = specialBookmarkDao.get(specialBookmarkID);
//        if (specialBookmarkEntity == null) {
//            throw new SystemException(SystemError.DATA_NOT_FOUND, "specialBookmarkID", SystemError.DATA_NOT_FOUND.getValue());
//        } else if (specialBookmarkEntity.getCustomerID() != customerID) {
//            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, "customerID", SystemError.DATA_NOT_FOUND.getValue());
//        }
//
//        return specialBookmarkEntity;
//    }

//    public List<BookmarkEntity> getCustomerBookmarksByType(int customerID, String type) {
//        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);
//        return bookmarkDao.getByCustomerIDAndType(customerID, typeBookmarkEntity.getTypeBookmarkID());
//    }
//
//    public List<SpecialBookmarkEntity> getCustomerSpecialBookmarksByType(int customerID, String type) {
//        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);
//        return specialBookmarkDao.getByCustomerIDAndType(customerID, typeBookmarkEntity.getTypeBookmarkID());
//    }
//
//    public BookmarkEntity getBookmarkEntity(int customerID, int bookmarkID) {
//        BookmarkEntity bookmarkEntity = bookmarkDao.get(bookmarkID);
//        if (bookmarkEntity == null) {
//            throw new SystemException(SystemError.DATA_NOT_FOUND, "bookmarkID", SystemError.DATA_NOT_FOUND.getValue());
//        } else if (bookmarkEntity.getCustomerID() != customerID) {
//            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, "customerID", SystemError.DATA_NOT_FOUND.getValue());
//        }
//
//        return bookmarkEntity;
//    }

    public void update(int customerID, int bookmarkOrSpecialBookmarkID, String type, Map<String, Object> map) {
        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);

        if (typeBookmarkEntity == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, TYPE, SystemError.ILLEGAL_ARGUMENT.getValue());
        }

        if (typeBookmarkEntity.getName().equals(TypeBookmark.OTHER.getValue())) {
            updateBookmarkEntity(customerID, bookmarkOrSpecialBookmarkID, map);
        } else if (typeBookmarkEntity.getName().equals(TypeBookmark.HOME.getValue()) ||
                typeBookmarkEntity.getName().equals(TypeBookmark.WORK.getValue())) {
            updateSpecialBookmarkEntity(customerID, bookmarkOrSpecialBookmarkID, map);
        }
    }

    public void delete(int customerID, int bookmarkOrSpecialBookmarkID, String type) {
        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);

        if (typeBookmarkEntity == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, TYPE, SystemError.ILLEGAL_ARGUMENT.getValue());
        }

        if (typeBookmarkEntity.getName().equals(TypeBookmark.OTHER.getValue())) {
            deleteBookmarkEntity(customerID, bookmarkOrSpecialBookmarkID);
        } else if (typeBookmarkEntity.getName().equals(TypeBookmark.HOME.getValue()) ||
                typeBookmarkEntity.getName().equals(TypeBookmark.WORK.getValue())) {
            deleteSpecialBookmarkEntity(customerID, bookmarkOrSpecialBookmarkID);
        }
    }

    private void updateBookmarkEntity(int customerID, int bookmarkID, Map<String, Object> map) {
        BookmarkEntity bookmarkEntity = bookmarkDao.get(bookmarkID);
        if (bookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, BOOKMARK_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (bookmarkEntity.getPersonID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.DATA_NOT_FOUND.getValue());
        }
        setParams(bookmarkEntity, map);
        bookmarkDao.update(bookmarkEntity);
    }

    private void deleteBookmarkEntity(int customerID, int bookmarkID) {
        BookmarkEntity bookmarkEntity = bookmarkDao.get(bookmarkID);
        if (bookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, BOOKMARK_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (bookmarkEntity.getPersonID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }
        bookmarkDao.delete(bookmarkEntity.getBookmarkID());
    }

    private void updateSpecialBookmarkEntity(int customerID, int specialBookmarkID, Map<String, Object> map) {
        SpecialBookmarkEntity specialBookmarkEntity = specialBookmarkDao.get(specialBookmarkID);
        if (specialBookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, SPECIAL_BOOKMARK_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (specialBookmarkEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.DATA_NOT_FOUND.getValue());
        }
        setParams(specialBookmarkEntity, map);
        specialBookmarkDao.update(specialBookmarkEntity);
    }

    private void deleteSpecialBookmarkEntity(int customerID, int specialBookmarkID) {
        SpecialBookmarkEntity specialBookmarkEntity = specialBookmarkDao.get(specialBookmarkID);
        if (specialBookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, SPECIAL_BOOKMARK_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (specialBookmarkEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }
        bookmarkDao.delete(specialBookmarkEntity.getSpecialBookmarkID());
    }

    public List<BookmarkDTO> getAll(int customerID) {

        List<BookmarkEntity> bookmarkEntities = bookmarkDao.getBookmarkByCustomerID(customerID);
        List<SpecialBookmarkEntity> specialBookmarkEntities = specialBookmarkDao.getSpecialBookmarkByCustomerID(customerID);
        List<BookmarkDTO> bookmarkDTOList = new ArrayList<>();

        for (BookmarkEntity bookmarkEntity : bookmarkEntities) {
            bookmarkDTOList.add(new BookmarkDTO(bookmarkEntity));
        }

        for (SpecialBookmarkEntity specialBookmarkEntity : specialBookmarkEntities) {
            bookmarkDTOList.add(new BookmarkDTO(specialBookmarkEntity));
        }

        return bookmarkDTOList;
    }

    public List<BookmarkDTO> getAllByType(int customerID, String type) {
        TypeBookmarkEntity typeBookmarkEntity = typeBookmarkDao.getTypeBookmarkByName(type);
        if (typeBookmarkEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE, SystemError.DATA_NOT_FOUND.getValue());
        }

        if (typeBookmarkEntity.getName().equals(TypeBookmark.HOME.getValue()) ||
                typeBookmarkEntity.getName().equals(TypeBookmark.WORK.getValue())) {
            List<SpecialBookmarkEntity> specialBookmarkEntities = specialBookmarkDao.getByCustomerIDAndType(customerID, typeBookmarkEntity.getTypeBookmarkID());
            List<BookmarkDTO> bookmarkDTOList = new ArrayList<>();
            for (SpecialBookmarkEntity entity: specialBookmarkEntities) {
                bookmarkDTOList.add(new BookmarkDTO(entity));
            }
            return bookmarkDTOList;
        } else if (typeBookmarkEntity.getName().equals(TypeBookmark.OTHER.getValue())) {
            List<BookmarkEntity> bookmarkEntities = bookmarkDao.getByCustomerIDAndType(customerID, typeBookmarkEntity.getTypeBookmarkID());
            List<BookmarkDTO> bookmarkDTOList = new ArrayList<>();
            for (BookmarkEntity entity: bookmarkEntities) {
                bookmarkDTOList.add(new BookmarkDTO(entity));
            }
            return bookmarkDTOList;
        } else
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, TYPE, SystemError.ILLEGAL_ARGUMENT.getValue());
    }


}
