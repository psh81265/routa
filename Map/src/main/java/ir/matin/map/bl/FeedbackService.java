package ir.matin.map.bl;

import ir.matin.database.dao.*;
import ir.matin.database.entity.FeedbackEntity;
import ir.matin.map.data.dto.FeedbackDTO;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class FeedbackService {

    @Autowired
    GeoPlaceDao geoPlaceDao;

    @Autowired
    FeedbackDao feedbackDao;

    @Autowired
    TypeFeedbackStatusDao typeFeedbackStatusDao;

    @Autowired
    TypeFeedbackDao typeFeedbackDao;

    @Autowired
    ValidationService validationService;

    private static final String PERSON_ID = "personID";
    private static final String REVIEW_ID = "feedbackID";
    private static final String TYPE_FEEDBACK_STATUS_ID = "typeFeedbackStatusId";
    private static final String COMMENT = "comment";
    private static final String RATE = "rate";
    private static final String DATE = "date";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String TYPE_FEEDBACK_ID = "typeFeedbackId";



    public void add(int personID, Map<String, Object> map) {
        FeedbackEntity feedbackEntity = new FeedbackEntity();

        if (!map.containsKey(COMMENT)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, COMMENT, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        }

        feedbackEntity.setPersonID(personID);
        feedbackEntity.setDate(new Timestamp(System.currentTimeMillis()));
        setComment(feedbackEntity, map);
        setRate(feedbackEntity, map);
        setTypeReviewStatusID(feedbackEntity, map);
        feedbackEntity.setLatitude(Double.parseDouble(map.get(LATITUDE).toString()));
        feedbackEntity.setLongitude(Double.parseDouble(map.get(LONGITUDE).toString()));
        setTypeFeedbackId(feedbackEntity,map);
        feedbackDao.add(feedbackEntity);
    }

    private void setTypeFeedbackId(FeedbackEntity feedbackEntity, Map<String, Object> map) {
        if (map.containsKey(TYPE_FEEDBACK_ID)) {
            int typeFeedbackID = validationService.validateInteger(map.get(TYPE_FEEDBACK_ID).toString());
            if (typeFeedbackDao.get(typeFeedbackID) == null)
                throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE_FEEDBACK_STATUS_ID, SystemError.DATA_NOT_FOUND.getValue());
            feedbackEntity.setTypeFeedbackId(typeFeedbackID);
        }
    }

    private void setComment(FeedbackEntity feedbackEntity, Map<String, Object> map) {
        if (map.containsKey(COMMENT)) {
            String comment = validationService.validatePersianString(map.get(COMMENT).toString());
            feedbackEntity.setComment(comment);
        }
    }

    private void setTypeReviewStatusID(FeedbackEntity feedbackEntity, Map<String, Object> map) {
        if (map.containsKey(TYPE_FEEDBACK_STATUS_ID)) {
            int typeProblemID = validationService.validateInteger(map.get(TYPE_FEEDBACK_STATUS_ID).toString());
            if (typeFeedbackStatusDao.get(typeProblemID) == null)
                throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE_FEEDBACK_STATUS_ID, SystemError.DATA_NOT_FOUND.getValue());
            feedbackEntity.setTypeFeedbackStatusId(typeProblemID);
        }
    }

    private void setRate(FeedbackEntity feedbackEntity, Map<String, Object> map) {
        if (map.containsKey(RATE)) {
            int typeProblemStatusID = validationService.validateInteger(map.get(RATE).toString());
            feedbackEntity.setRate(typeProblemStatusID);
        }
    }

    public void update(int personID, int reviewID, Map<String, Object> map) {
        FeedbackEntity feedbackEntity = feedbackDao.get(reviewID);

        if (feedbackEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, REVIEW_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (feedbackEntity.getPersonID() != personID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, REVIEW_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }

        setComment(feedbackEntity, map);
        setRate(feedbackEntity, map);
        setTypeReviewStatusID(feedbackEntity, map);
        feedbackEntity.setLatitude(Double.parseDouble(map.get(LATITUDE).toString()));
        feedbackEntity.setLongitude(Double.parseDouble(map.get(LONGITUDE).toString()));
        setTypeFeedbackId(feedbackEntity,map);
        feedbackDao.update(feedbackEntity);
    }

    private FeedbackEntity getFeedbackEntity(int personID, int reviewID) {
        FeedbackEntity feedbackEntity = feedbackDao.get(reviewID);
        if (feedbackEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, REVIEW_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (feedbackEntity.getPersonID() != personID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, REVIEW_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }

        return feedbackEntity;
    }

    public FeedbackDTO get(int personID, int reviewID) {
        return new FeedbackDTO(getFeedbackEntity(personID, reviewID));
    }

    public void delete(int personID, int problemID) {
        FeedbackEntity feedbackEntity = feedbackDao.get(problemID);

        if (feedbackEntity == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, REVIEW_ID, SystemError.ILLEGAL_ARGUMENT.getValue());
        }

        if (feedbackEntity.getPersonID() != personID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PERSON_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }

        feedbackDao.delete(problemID);
    }


    public List<FeedbackDTO> getAll(int personID) {

        List<FeedbackEntity> feedbackEntities = feedbackDao.getFeedbackByPersonID(personID);
        List<FeedbackDTO> feedbackDTOList = new ArrayList<>();

        for (FeedbackEntity feedbackEntity : feedbackEntities) {
            feedbackDTOList.add(new FeedbackDTO(feedbackEntity));
        }

        return feedbackDTOList;
    }


}
