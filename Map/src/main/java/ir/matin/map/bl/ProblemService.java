package ir.matin.map.bl;

import ir.matin.database.dao.*;
import ir.matin.database.entity.*;
import ir.matin.map.data.TypeBookmark;
import ir.matin.map.data.dto.BookmarkDTO;
import ir.matin.map.data.dto.ProblemDTO;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ProblemService {

    @Autowired
    ProblemDao problemDao;

    @Autowired
    TypeProblemDao typeProblemDao;

    @Autowired
    TypeProblemStatusDao typeProblemStatusDao;

    private static final String CUSTOMER_ID = "customerID";
    private static final String PROBLEM_ID = "problemID";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String TYPE_PROBLEM_ID = "typeProblemID";
    private static final String TYPE_PROBLEM_STATUS_ID = "typeProblemStatusID";

    public void add(int customerID, Map<String, Object> map) {
        ProblemEntity problemEntity = new ProblemEntity();

        if (!map.containsKey(LATITUDE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LATITUDE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        } else if (!map.containsKey(LONGITUDE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LONGITUDE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        }

        problemEntity.setCustomerID(customerID);
        problemEntity.setDate(new Timestamp(System.currentTimeMillis()));

        setLatitudeAndLongitude(problemEntity, map);
        setTypeProblemID(problemEntity, map);
        setTypeProblemStatusID(problemEntity, map);
    }

    private void setTypeProblemID(ProblemEntity problemEntity, Map<String, Object> map) {
        if (map.containsKey(TYPE_PROBLEM_ID)) {
            int typeProblemID = Integer.parseInt(map.get(TYPE_PROBLEM_ID).toString());
            if (typeProblemDao.get(typeProblemID) == null)
                throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE_PROBLEM_ID, SystemError.DATA_NOT_FOUND.getValue());
            problemEntity.setTypeProblemID(typeProblemID);
        }
    }

    private void setTypeProblemStatusID(ProblemEntity problemEntity, Map<String, Object> map) {
        if (map.containsKey(TYPE_PROBLEM_STATUS_ID)) {
            int typeProblemStatusID = Integer.parseInt(map.get(TYPE_PROBLEM_STATUS_ID).toString());
            if (typeProblemStatusDao.get(typeProblemStatusID) == null)
                throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE_PROBLEM_STATUS_ID, SystemError.DATA_NOT_FOUND.getValue());
            problemEntity.setTypeProblemStatusID(typeProblemStatusID);
        }
    }

    private void setLatitudeAndLongitude(ProblemEntity problemEntity, Map<String, Object> map) {
        if (map.containsKey(LATITUDE)) {
            double lat = Double.parseDouble(map.get(LATITUDE).toString());
            problemEntity.setLatitude(lat);
        }

        if (map.containsKey(LONGITUDE)) {
            double lon = Double.parseDouble(map.get(LONGITUDE).toString());
            problemEntity.setLongitude(lon);
        }
    }

    public void update(int customerID, int problemID, Map<String, Object> map) {
        ProblemEntity problemEntity = problemDao.get(problemID);

        if (problemEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, PROBLEM_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (problemEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PROBLEM_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }

        setLatitudeAndLongitude(problemEntity, map);
        setTypeProblemID(problemEntity, map);
        setTypeProblemStatusID(problemEntity, map);
    }

    private ProblemEntity getProblemEntity(int customerID, int problemID) {
        ProblemEntity problemEntity = problemDao.get(problemID);
        if (problemEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, PROBLEM_ID, SystemError.DATA_NOT_FOUND.getValue());
        } else if (problemEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, PROBLEM_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }

        return problemEntity;
    }

    public ProblemDTO get(int customerID, int problemID) {
        return new ProblemDTO(getProblemEntity(customerID, problemID));
    }

//    public List<ProblemEntity> getCustomerProblemsByType(int customerID, int typeProblemID) {
//        List<ProblemEntity> problemEntities = problemDao.getProblemByCustomerIDAndTypeID(customerID, typeProblemID);
//        return problemEntities;
//    }

    public void delete(int customerID, int problemID) {
        ProblemEntity problemEntity = problemDao.get(problemID);

        if (problemEntity == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, PROBLEM_ID, SystemError.ILLEGAL_ARGUMENT.getValue());
        }

        if (problemEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, CUSTOMER_ID, SystemError.LOGICAL_UN_AUTHORIZED.getValue());
        }

        problemDao.delete(problemID);
    }


    public List<ProblemDTO> getAll(int customerID) {

        List<ProblemEntity> problemEntities = problemDao.getProblemByCustomerID(customerID);
        List<ProblemDTO> problemDTOList = new ArrayList<>();

        for (ProblemEntity problemEntity : problemEntities) {
            problemDTOList.add(new ProblemDTO(problemEntity));
        }

        return problemDTOList;
    }


}
