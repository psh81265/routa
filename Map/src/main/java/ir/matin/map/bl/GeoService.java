package ir.matin.map.bl;

import com.sun.jndi.toolkit.url.Uri;
import com.sun.org.apache.xml.internal.utils.URI;
import ir.matin.database.dao.FeedbackDao;
import ir.matin.database.dao.GeoPlaceDao;
import ir.matin.database.dao.TypeFeedbackStatusDao;
import ir.matin.database.entity.FeedbackEntity;
import ir.matin.map.data.dto.FeedbackDTO;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import springfox.documentation.spring.web.json.Json;

import java.sql.Timestamp;
import java.util.*;

@Service
public class GeoService {

    @Autowired
    GeoPlaceDao geoPlaceDao;

    @Autowired
    FeedbackDao feedbackDao;

    @Autowired
    TypeFeedbackStatusDao typeFeedbackStatusDao;

    @Autowired
    ValidationService validationService;

    RestTemplate restTemplate = new RestTemplate();

    public Object getPointToAddress(Double latitude , Double longitude) {
        final String url = "http://192.168.7.129:5001/gateway/api/free/macro/point/to/address";
        HttpEntity<String> entity = setHeader();
        //
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("lat",latitude.toString())
                .queryParam("lon", longitude.toString());
        //
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,String.class);
        return response.getBody();
    }

    public Object getAddressToPoint(String address ,String viewbox) {
        final String url = "http://192.168.7.129:5001/gateway/api/free/macro/address/to/point";
        HttpEntity<String> entity =setHeader();
        //
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("address",address)
                .queryParam("viewbox", "");
        //
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,String.class);
        return response.getBody();
    }

    public Object searchSimpleAddress(Double latitude , Double longitude) {
        final String url = "http://192.168.7.129:5001/gateway/api/free/macro/search/place";
        HttpEntity<String> entity =setHeader();
        //
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("lat",latitude.toString())
                .queryParam("lon", longitude.toString());
        //
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,String.class);
        return response.getBody();
    }

    public Object getDirection(String origin , String destination , boolean oddeven , boolean trafficzone) {
        final String url = "http://192.168.7.129:5001/gateway/api/free/macro/direction";
        HttpEntity<String> entity =setHeader();
        //
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("orgin",origin)
                .queryParam("destination", destination)
                .queryParam("oddeven", oddeven)
                .queryParam("trafficzone", trafficzone);
        //
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,String.class);
        return response.getBody();
    }

    public Object getDistance(String points) {
        final String url = "http://192.168.7.129:5001/gateway/api/free/macro/distance/matrice";
        HttpEntity<String> entity =setHeader();
        //
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("points", points);
        //
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,String.class);
        return response.getBody();
    }

    public Object getMapMaching(String points) {
        final String url = "http://192.168.7.129:5001/gateway/api/free/macro/map/matching";
        HttpEntity<String> entity =setHeader();
        //
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("points", points);
        //
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity,String.class);
        return response.getBody();
    }

    private HttpEntity setHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("apikey","pk.iman1");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", httpHeaders);
        return entity;
    }
}
