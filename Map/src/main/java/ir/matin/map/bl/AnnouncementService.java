package ir.matin.map.bl;

import ir.matin.database.dao.AnnouncementDao;
import ir.matin.database.dao.TypeAnnouncementDao;
import ir.matin.database.dao.TypeAnnouncementStatusDao;
import ir.matin.database.entity.AnnouncementEntity;
import ir.matin.map.data.dto.AnnouncementDTO;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AnnouncementService {

    @Autowired
    AnnouncementDao announcementDao;

    @Autowired
    TypeAnnouncementDao typeAnnouncementDao;

    @Autowired
    TypeAnnouncementStatusDao typeAnnouncementStatusDao;

    private static final String ANNOUNCEMENT_ID = "announcementID";
    private static final String CUSTOMER_ID = "customerID";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String TYPE_ANNOUNCEMENT_ID = "typeAnnouncementID";
    private static final String TYPE_ANNOUNCEMENT_STATUS_ID = "typeAnnouncementStatusID";

    public void add(int customerID, Map<String, Object> map) {

        if (!map.containsKey(LATITUDE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LATITUDE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        } else if (!map.containsKey(LONGITUDE)) {
            throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LONGITUDE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
        }

        AnnouncementEntity announcementEntity = new AnnouncementEntity();
        announcementEntity.setCustomerID(customerID);
        announcementEntity.setDate(new Timestamp(System.currentTimeMillis()));

        setAnnouncementLatAndLong(announcementEntity, map);
        setTypeAnnouncementID(announcementEntity, map);
        setTypeAnnouncementStatusID(announcementEntity, map);

        announcementDao.add(announcementEntity);
    }

    private void setTypeAnnouncementID(AnnouncementEntity announcementEntity, Map<String, Object> map) {
        if (map.containsKey(TYPE_ANNOUNCEMENT_ID)) {
            int typeAnnouncementID = Integer.parseInt(map.get(TYPE_ANNOUNCEMENT_ID).toString());
            if (typeAnnouncementDao.get(typeAnnouncementID) == null) {
                throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE_ANNOUNCEMENT_ID, SystemError.DATA_NOT_FOUND.getValue());
            }
            announcementEntity.setTypeAnnouncementID(typeAnnouncementID);
        }
    }

    private void setTypeAnnouncementStatusID(AnnouncementEntity announcementEntity, Map<String, Object> map) {
        if (map.containsKey(TYPE_ANNOUNCEMENT_STATUS_ID)) {
            int typeAnnouncementStatusID = Integer.parseInt(map.get(TYPE_ANNOUNCEMENT_STATUS_ID).toString());
            if (typeAnnouncementStatusDao.get(typeAnnouncementStatusID) == null) {
                throw new SystemException(SystemError.DATA_NOT_FOUND, TYPE_ANNOUNCEMENT_STATUS_ID, SystemError.DATA_NOT_FOUND.getValue());
            }
            announcementEntity.setTypeAnnouncementStatusID(typeAnnouncementStatusID);
        }
    }

    public AnnouncementDTO get(int customerID, int announcementID) {
        return new AnnouncementDTO(getAnnouncementEntity(customerID, announcementID));
    }

    public List<AnnouncementDTO> getAll(int customerID) {
        List<AnnouncementEntity> announcementEntityList = announcementDao.getByCustomerID(customerID);
        List<AnnouncementDTO> announcementDTOList = new ArrayList<>();
        for (AnnouncementEntity announcementEntity: announcementEntityList) {
            announcementDTOList.add(new AnnouncementDTO(announcementEntity));
        }
        return announcementDTOList;
    }

    private AnnouncementEntity getAnnouncementEntity(int customerID, int announcementID) {
        AnnouncementEntity announcementEntity = announcementDao.get(announcementID);
        if (announcementEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, ANNOUNCEMENT_ID, SystemError.DATA_NOT_FOUND.getValue());
        }
        else if (announcementEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, CUSTOMER_ID, SystemError.DATA_NOT_FOUND.getValue());
        }

        return announcementEntity;
    }

    public void update(int customerID, int announcementID, Map<String, Object> map) {
        AnnouncementEntity announcementEntity = announcementDao.get(announcementID);

        if (announcementEntity == null) {
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, ANNOUNCEMENT_ID, SystemError.ILLEGAL_ARGUMENT.getValue());
        }

        if (announcementEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, CUSTOMER_ID, SystemError.DATA_NOT_FOUND.getValue());
        }

        setAnnouncementLatAndLong(announcementEntity, map);
        setTypeAnnouncementID(announcementEntity, map);
        setTypeAnnouncementStatusID(announcementEntity, map);
    }

    private void setAnnouncementLatAndLong(AnnouncementEntity announcementEntity, Map<String, Object> map) {
        if (map.containsKey(LATITUDE)) {
            double lat = Double.parseDouble(map.get(LATITUDE).toString());
            announcementEntity.setLatitude(lat);
        }

        if (map.containsKey(LONGITUDE)) {
            double lon = Double.parseDouble(map.get(LONGITUDE).toString());
            announcementEntity.setLongitude(lon);
        }
    }

    public void delete(int customerID, int announcementID) {
        AnnouncementEntity announcementEntity = announcementDao.get(announcementID);

        if (announcementEntity == null) {
            throw new SystemException(SystemError.DATA_NOT_FOUND, ANNOUNCEMENT_ID, SystemError.DATA_NOT_FOUND.getValue());
        }

        if (announcementEntity.getCustomerID() != customerID) {
            throw new SystemException(SystemError.LOGICAL_UN_AUTHORIZED, CUSTOMER_ID, SystemError.DATA_NOT_FOUND.getValue());
        }

        announcementDao.delete(announcementID);
    }

}
