package ir.matin.map.api;

import ir.matin.database.dao.PersonDao;
import ir.matin.database.entity.PersonEntity;
import ir.matin.user.api.PublicUserController;
import ir.matin.utility.bl.StringService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.Filter;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ContextConfiguration(locations = {"classpath:test-dispatcher-servlet.xml"})
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
class AnnouncementControllerTest {

    private static final String ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAFWNOw7CMBBE77J1jNbYxp-OA9DlArZjSwYRoqyRiBB3Z1NQMNXoaZ7mDfRMEOCccyEaH7cywwCNiFlbD_fY2w5y3xhcuC1tgiCUHKC8FgheSeuMlydkK3YI0mj5A9fe2LIR86SVErZoJzTvhc_HJBBV9a5WV5PZL2hmG__y-QK3jXI7nwAAAA.otBjuyLLLmAHNni3HDdzk_eyWkiVypHRhVSBCSSaMEO7M561LpxFtOTaJYniZZC6CKE_R455G17JIaPA93XQCw";
    private static final String CTY = "m";
    private static final String CSN = "100000000000000";

    @Autowired
    PublicUserController publicUserController;

    @Autowired
    PersonDao personDao;

    @Autowired
    private AnnouncementController announcementController;

    @Qualifier("springSecurityFilterChain")
    @Autowired
    Filter filter;

    private MockMvc mockMvc;

    @BeforeEach
    void init() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(announcementController)
                .addFilter(filter)
                .build();
    }

    @Test
    void getAll() {
        try {
            MvcResult mvcResult = mockMvc.perform(get("/rest/map/announcement")
                    .header("csn", CSN)
                    .header("cty", CTY)
                    .header("Authorization", ACCESS_TOKEN)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
            System.out.println(mvcResult.getResponse().getContentAsString());
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    @Test
    void update() {
    }

    @Test
    void delete() {
    }

    @Test
    void add() {
    }

    /**
     * Scenario: username and password are correct
     * login with kamangir1
     * @throws Exception
     */
    @Transactional
    @Test
    public void login_kamangir1_loginWhenEveryThingIsOK() throws Exception {
        String restAddress = "/rest/public/user/login";
        String username = "elyasi1";
        String password = "h123456";
        String cty = "m";
        String csn = "100000000000000";
        String role =  "DRIVER_COMPANY_L1";
        String roleCategory = "DRV_CMP";
        String picture = null;
        String firstname = "اردشیر";
        String lastname = "کمانگیر";
        String totalCredit = "0";
        String mobile = "09361766223";
        String email = "test4@gmail.com";

        PersonEntity personEntity = personDao.getByUsername("elyasi1");
        personEntity.setLogin(false);
        personDao.update(personEntity);

        Map<String, Object> postParameters = new HashMap<>();
        postParameters.put("username", username);
        postParameters.put("password", password);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("cty", cty);
        request.addHeader("csn", csn);

        MvcResult mvcResult = mockMvc.perform(post(restAddress)
                .header("cty", cty)
                .header("csn", csn)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(StringService.toJsonString(postParameters)))
                .andReturn();

        System.out.println(mvcResult.getResponse().getContentAsString());

    }
}