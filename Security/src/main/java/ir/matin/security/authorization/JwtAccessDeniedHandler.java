package ir.matin.security.authorization;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.matin.database.entity.PersonEntity;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.type.DefaultResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ir.matin.security.log.AuditLogService.RestAuthorizationError;
import static ir.matin.security.log.AuditLogService.createAuthorizationFailedLogPattern;
import static ir.matin.utility.bl.DevelopmentLogService.createAuthLogPattern;

/**
 * Access Denied Handler Class,
 * Handling Requests after Authorization FAILURE
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {

    private static final Logger auditLogger = LogManager.getLogger("audit");
    private static final Logger devLogger = LogManager.getLogger(JwtAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException exp) throws IOException {
        PersonEntity personEntity = (PersonEntity) request.getAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE);

        auditLogger.log(RestAuthorizationError, createAuthorizationFailedLogPattern(personEntity, request));
        devLogger.error(createAuthLogPattern("handle", "RestAuthorization") + SystemError.ACCESS_DENIED);

        RestResult<DefaultResult> result = new RestResult<>();
        result.setError(SystemError.ACCESS_DENIED);
        result.setDescription(SystemError.ACCESS_DENIED.getValue());

        SecurityContextHolder.clearContext();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream().write(new ObjectMapper().writeValueAsBytes(result));
    }
}
