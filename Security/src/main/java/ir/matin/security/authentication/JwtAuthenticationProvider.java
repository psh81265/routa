package ir.matin.security.authentication;

import ir.matin.database.entity.PersonEntity;
import ir.matin.security.bl.JwtService;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.object.JwtInputAuthentication;
import ir.matin.security.data.object.JwtOutputAuthentication;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * Authentication Provider Class,
 * Process Authentication in Spring Security
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private JwtService jwtService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            PersonEntity possibleUser = this.jwtService.extractPersonEntityFromToken((AuthRequestHeader) authentication.getPrincipal());
            return new JwtOutputAuthentication(possibleUser);
        } catch (SystemException e) {
            throw new JwtAuthenticationException(e);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtInputAuthentication.class.equals(authentication);
    }
}
