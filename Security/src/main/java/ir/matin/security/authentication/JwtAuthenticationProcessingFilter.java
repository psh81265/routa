package ir.matin.security.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.matin.security.bl.HeaderService;
import ir.matin.security.bl.SkipPathRequestMatcherService;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.object.JwtInputAuthentication;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static ir.matin.security.log.AuditLogService.RestAuthenticationError;
import static ir.matin.security.log.AuditLogService.createAuthenticationFailedLogPattern;

/**
 * Authentication and Authorization Processing Filter Class,
 * Main Class for Authentication and Authorization
 * which Handles Successful or Unsuccessful Situations
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Component
public class JwtAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger auditLogger = LogManager.getLogger("audit");

    @Autowired
    private HeaderService headerService;

    public JwtAuthenticationProcessingFilter(SkipPathRequestMatcherService skipPathRequestMatcherService) {
        super(skipPathRequestMatcherService);
    }

    @Autowired
    public void init(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            AuthRequestHeader authRequestHeader = this.headerService.extractAuthTokenClient(request);
            JwtInputAuthentication possibleToken = new JwtInputAuthentication(authRequestHeader);
            return getAuthenticationManager().authenticate(possibleToken);
        } catch (SystemException e) {
            throw new JwtAuthenticationException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication possibleUser) throws IOException, ServletException {
        request.setAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE, possibleUser.getPrincipal());
        SecurityContextHolder.getContext().setAuthentication(possibleUser);
        chain.doFilter(request, response);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException exception) throws IOException {
        JwtAuthenticationException exp = (JwtAuthenticationException) exception;

        auditLogger.log(RestAuthenticationError, createAuthenticationFailedLogPattern(request));

        RestResult<DefaultResult> result = new RestResult<>();
        result.setError(exp.getSystemException().getUserError());
        result.setDescription(exp.getSystemException().getDescription());

        SecurityContextHolder.clearContext();
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getOutputStream().write(new ObjectMapper().writeValueAsBytes(result));
    }
}
