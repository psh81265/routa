package ir.matin.security.data.type;

/**
 * Authentication Patterns Enum,
 * Default Patterns for Spring Security
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public enum AuthenticationPatternType {
    SKIP_PATTERN("/rest/public/**"),
    AUTH_PATTERN("/rest/**");

    private final String pattern;

    AuthenticationPatternType(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }
}
