package ir.matin.security.data.type;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

/**
 * Security Constants,
 * All Security Constants are Declared in SecurityConstant Class
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class SecurityConstant {
    public static final String HEADER_TOKEN_KEY = "Authorization";
    public static final String HEADER_CLIENT_SERIAL_NUMBER_KEY = "CSN";
    public static final String HEADER_CLIENT_TYPE_KEY = "CTY";

    public static final String TOKEN_ISSUER = "ir.matin";
    public static final String TOKEN_SECRET_KEY = "HS512SecretKey351756051523999";
    public static final int TOKEN_TIME_ZONE_OFFSET = 35 * 360; //3.5 * 3600

    public static final String ACCESS_TOKEN_SUBJECT = "AccessToken";
    public static final String REFRESH_TOKEN_SUBJECT = "RefreshToken";

    public static final int ACCESS_TOKEN_MOBILE_EXPIRATION_HOURS = 3 * 30 * 24 * 1000;
    public static final int REFRESH_TOKEN_MOBILE_EXPIRATION_HOURS = 12 * 30 * 24 * 1000;

    public static final int ACCESS_TOKEN_BROWSER_EXPIRATION_HOURS = 24 * 1000;
    public static final int REFRESH_TOKEN_BROWSER_EXPIRATION_HOURS = 30 * 24 * 1000;

    public static final String REQUEST_EXTENDED_ATTRIBUTE = "personEntity";

    /* **************************************************** Defaults **************************************************** */

    public static final Integer OWNER_COMPANY_ID = -1;
    public static final String ROLE_ADMIN_OWNER = "ADMIN_OWNER";
    public static final String ROLE_ADMIN_COMPANY = "ADMIN_COMPANY";
    public static final String ROLE_CUSTOMER = "CUSTOMER";

    private SecurityConstant() {
        throw new SystemException(SystemError.ILLEGAL_CONSTRUCTOR, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
    }
}
