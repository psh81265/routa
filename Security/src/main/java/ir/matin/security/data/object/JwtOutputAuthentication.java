package ir.matin.security.data.object;

import ir.matin.database.entity.PersonEntity;
import ir.matin.database.entity.RoleEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * All Request turns into JwtOutputAuthentication Object
 * after Successful Authentication
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class JwtOutputAuthentication implements Authentication {

    private final PersonEntity personEntity;

    public JwtOutputAuthentication(PersonEntity personEntity) {
        this.personEntity = personEntity;
    }

    @Override
    public Object getPrincipal() {
        return this.personEntity;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<RoleEntity> roles = this.personEntity.getRoles();
        return roles.stream().map(roleEntity -> new SimpleGrantedAuthority("ROLE_" + roleEntity.getName()))
                .collect(Collectors.toSet());
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return null;
    }
}
