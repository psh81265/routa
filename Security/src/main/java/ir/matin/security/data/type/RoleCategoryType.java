package ir.matin.security.data.type;

public enum RoleCategoryType {
    ALL("ALL"),
    ADMIN_OWNER("ADM_OWN"),
    ADMIN_COMPANY("ADM_CMP"),
    CUSTOMER("CUS"),
    OPERATOR_OWNER("OPR_OWN"),
    OPERATOR_COMPANY("OPR_CMP"),
    GISMAN("GMAN");

    private final String value;

    RoleCategoryType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
