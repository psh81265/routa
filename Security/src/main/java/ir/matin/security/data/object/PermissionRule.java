package ir.matin.security.data.object;

import ir.matin.database.entity.PermissionEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.util.List;
import java.util.stream.Collectors;

public class PermissionRule {
    private String permission;
    private List<String> roles;

    public PermissionRule(PermissionEntity permissionEntity) {
        if (permissionEntity == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.permission = permissionEntity.getUrl();
        this.roles = permissionEntity.getRoles().stream().map(RoleEntity::getName)
                .collect(Collectors.toList());
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
