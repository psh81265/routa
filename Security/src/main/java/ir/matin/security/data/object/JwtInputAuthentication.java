package ir.matin.security.data.object;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * All Request turns into JwtInputAuthentication Object
 * and imported into Authentication Process
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class JwtInputAuthentication implements Authentication {
    private final AuthRequestHeader authRequestHeader;

    public JwtInputAuthentication(AuthRequestHeader authRequestHeader) {
        this.authRequestHeader = authRequestHeader;
    }

    @Override
    public Object getPrincipal() {
        return authRequestHeader;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return null;
    }
}
