package ir.matin.security.data.dto;

import ir.matin.database.entity.RoleEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;

public class RoleDto implements Serializable {
    private int roleId;
    private String category;
    private String name;
    private String title;
    private Boolean manageView;

    public RoleDto(RoleEntity roleEntity) {
        if (roleEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.roleId = roleEntity.getRoleID();
        this.category = roleEntity.getCategory();
        this.name = roleEntity.getName();
        this.title = roleEntity.getTitle();
        this.manageView = roleEntity.getManageview();
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getManageView() {
        return manageView;
    }

    public void setManageView(Boolean manageView) {
        this.manageView = manageView;
    }
}
