package ir.matin.security.data.object;

import ir.matin.security.data.type.ClientType;

public class AuthRequestHeader {
    private String token;
    private String clientSerialNumber;
    private ClientType clientType;

    public AuthRequestHeader(String token, String clientSerialNumber, ClientType clientType) {
        this.token = token;
        this.clientSerialNumber = clientSerialNumber;
        this.clientType = clientType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientSerialNumber() {
        return clientSerialNumber;
    }

    public void setClientSerialNumber(String clientSerialNumber) {
        this.clientSerialNumber = clientSerialNumber;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }
}
