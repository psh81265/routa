package ir.matin.security.data.type;

public enum ClientType {
    MOBILE("M"),
    DESKTOP("D"),
    BROWSER("B");

    private final String value;

    ClientType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
