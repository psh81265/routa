package ir.matin.security.log;

import ir.matin.database.entity.PersonEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.apache.logging.log4j.Level;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;

import static ir.matin.utility.bl.StringService.toJsonString;

public class AuditLogService {

    //Log Levels
    public static final Level NOTICE = Level.forName("NOTICE", 250);
    public static final Level MethodAuthorizationError = Level.forName("AuthorizationError", 130);
    public static final Level RestAuthorizationError = Level.forName("AuthorizationError", 120);
    public static final Level RestAuthenticationError = Level.forName("AuthenticationError", 110);

    /* ****************************************************************************************************************** */

    private AuditLogService() {
        throw new SystemException(SystemError.ILLEGAL_CONSTRUCTOR, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
    }

    public static String createPublicLogPattern(HttpServletRequest request) {
        ClientInfo clientInfo = new ClientInfo(request);

        return clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]";
    }

    public static String createPublicLogPattern(HttpServletRequest request, Object parameter) {
        ClientInfo clientInfo = new ClientInfo(request);

        return clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]" + " +" + toJsonString(parameter) + "+";
    }

    public static String createPublicLogPattern(HttpServletRequest request, Map parameters) {
        ClientInfo clientInfo = new ClientInfo(request);

        return clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]" + " +" + toJsonString(parameters) + "+";
    }

    /* ****************************************************************************************************************** */

    public static String createAuthenticatedLogPattern(PersonEntity personEntity, HttpServletRequest request) {
        String roles = personEntity.getRoles().stream().map(RoleEntity::getName)
                .collect(Collectors.joining(","));
        ClientInfo clientInfo = new ClientInfo(request);
        return roles + " " + personEntity.getPersonid() + " " + personEntity.getFirstname() + " " + personEntity.getLastname() + " " + clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]";
    }

    public static String createAuthenticatedLogPattern(PersonEntity personEntity, HttpServletRequest request, Map parameters) {
        String roles = personEntity.getRoles().stream().map(RoleEntity::getName)
                .collect(Collectors.joining(","));
        ClientInfo clientInfo = new ClientInfo(request);
        return roles + " " + personEntity.getPersonid() + " " + personEntity.getFirstname() + " " + personEntity.getLastname() + " " + clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]" + " +" + toJsonString(parameters) + "+";
    }

    public static String createAuthenticatedLogPattern(PersonEntity personEntity, HttpServletRequest request, Object parameter) {
        String roles = personEntity.getRoles().stream().map(RoleEntity::getName)
                .collect(Collectors.joining(","));
        ClientInfo clientInfo = new ClientInfo(request);
        return roles + " " + personEntity.getPersonid() + " " + personEntity.getFirstname() + " " + personEntity.getLastname() + " " + clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]" + " +" + toJsonString(parameter) + "+";
    }

    public static String createAuthenticatedLogPattern(PersonEntity personEntity, HttpServletRequest request, Object[] parameter) {
        String roles = personEntity.getRoles().stream().map(RoleEntity::getName)
                .collect(Collectors.joining(","));
        ClientInfo clientInfo = new ClientInfo(request);
        String parametersJson = "";
        if (parameter != null && parameter.length > 1)
            parametersJson = toJsonString(parameter[0]);
        return roles + " " + personEntity.getPersonid() + " " + personEntity.getFirstname() + " " + personEntity.getLastname() + " " + clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]" + " +" + parametersJson + "+";
    }

    /* ****************************************************************************************************************** */

    public static String createAuthenticationFailedLogPattern(HttpServletRequest request) {
        ClientInfo clientInfo = new ClientInfo(request);

        return clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]";

    }

    public static String createAuthorizationFailedLogPattern(PersonEntity personEntity, HttpServletRequest request) {
        String roles = personEntity.getRoles().stream().map(RoleEntity::getName)
                .collect(Collectors.joining(","));
        ClientInfo clientInfo = new ClientInfo(request);
        return roles + " " + personEntity.getPersonid() + " " + personEntity.getFirstname() + " " + personEntity.getLastname() + " " + clientInfo.getRequestURI()
                + " " + clientInfo.getMainIP() + " [" + clientInfo.getAgent() + "]";
    }

}
