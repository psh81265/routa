package ir.matin.security.log;

import javax.servlet.http.HttpServletRequest;

public class ClientInfo {

    private String requestURI;
    private String mainIP;
    private String proxyIP;
    private String host;
    private String agent;

    public ClientInfo(HttpServletRequest request) {
        this.requestURI = request.getRequestURI();
        this.mainIP = request.getHeader("X-FORWARDED-FOR");
        this.proxyIP = request.getRemoteAddr();
        if (this.mainIP == null || this.mainIP.equals("")) {
            this.mainIP = request.getRemoteAddr();
            this.proxyIP = null;
        }
        this.host = request.getRemoteHost();
        this.agent = request.getHeader("User-Agent");
    }

    public String getRequestURI() {
        return requestURI;
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    public String getMainIP() {
        return mainIP;
    }

    public void setMainIP(String mainIP) {
        this.mainIP = mainIP;
    }

    public String getProxyIP() {
        return proxyIP;
    }

    public void setProxyIP(String proxyIP) {
        this.proxyIP = proxyIP;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return "ClientInfo {" +
                "requestURI='" + requestURI + '\'' +
                ", mainIP='" + mainIP + '\'' +
                ", proxyIP='" + proxyIP + '\'' +
                ", host='" + host + '\'' +
                ", agent='" + agent + '\'' +
                '}';
    }
}
