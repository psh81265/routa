package ir.matin.security.config;

import ir.matin.security.authentication.JwtAuthenticationEntryPoint;
import ir.matin.security.authentication.JwtAuthenticationProcessingFilter;
import ir.matin.security.authentication.JwtAuthenticationProvider;
import ir.matin.security.authorization.JwtAccessDeniedHandler;
import ir.matin.security.bl.RolePermissionService;
import ir.matin.security.data.object.PermissionRule;
import ir.matin.security.data.type.AuthenticationPatternType;
import ir.matin.utility.data.object.SystemException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

import static ir.matin.utility.bl.DevelopmentLogService.createConfigLogPattern;

/**
 * Spring Security Configuration Class,
 * Config and Manage all Security Issues
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger devLogger = LogManager.getLogger(SecurityConfig.class);

    @Autowired
    private JwtAuthenticationProcessingFilter jwtAuthenticationProcessingFilter;
    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    @Autowired
    private JwtAccessDeniedHandler jwtAccessDeniedHandler;
    @Autowired
    private RolePermissionService rolePermissionService;
//    @Autowired
//    private LoggingFilter loggingFilter;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

//    @Bean
//    public CommonsRequestLoggingFilter requestLoggingFilter() {
//        return loggingFilter;
//    }

//    @Bean
//    public FilterRegistrationBean loggingFilterRegistration() {
//        FilterRegistrationBean registration = new FilterRegistrationBean(this.loggingFilter);
//        registration.addUrlPatterns(AuthenticationPatternType.SKIP_PATTERN.getPattern());
//        return registration;
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        devLogger.info(createConfigLogPattern("configure", "Configure Authentication Provider"));
        auth.authenticationProvider(this.jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors();

        devLogger.info(createConfigLogPattern("configure", "Configure Authorization Patterns"));
        http.authorizeRequests().antMatchers(AuthenticationPatternType.SKIP_PATTERN.getPattern()).permitAll();
        try {
            devLogger.info(createConfigLogPattern("configure", "Configure Authorization from Database"));
            List<PermissionRule> permissionRules = this.rolePermissionService.permissionRules();
            for (PermissionRule p : permissionRules)
                http.authorizeRequests().antMatchers(p.getPermission()).hasAnyRole(p.getRoles().toArray(new String[0]));
        } catch (SystemException ignored) {
        }
        http.authorizeRequests().antMatchers(AuthenticationPatternType.AUTH_PATTERN.getPattern()).authenticated();

        devLogger.info(createConfigLogPattern("configure", "Configure Processing Filter"));
        this.jwtAuthenticationProcessingFilter.init(authenticationManagerBean());
        http.addFilterBefore(this.jwtAuthenticationProcessingFilter, UsernamePasswordAuthenticationFilter.class);

        devLogger.info(createConfigLogPattern("configure", "Configure Exception Handler"));
        http.exceptionHandling()
                .authenticationEntryPoint(this.jwtAuthenticationEntryPoint)
                .accessDeniedHandler(this.jwtAccessDeniedHandler);

        devLogger.info(createConfigLogPattern("configure", "Configure Session Management"));
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        devLogger.info(createConfigLogPattern("configure", "Configure CSRF"));
        http.csrf().disable();
    }
}
