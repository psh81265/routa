package ir.matin.security.config;//package ir.matin.security.config;
//
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.CommonsRequestLoggingFilter;
//
//import javax.servlet.http.HttpServletRequest;
//
//@Component
//public class LoggingFilter extends CommonsRequestLoggingFilter {
//    public LoggingFilter() {
//        super.setIncludeClientInfo(true);
//        super.setIncludeQueryString(true);
//        super.setIncludePayload(true);
//    }
//
//    @Override
//    protected boolean shouldLog(HttpServletRequest request) {
//        return true;
//    }
//
//    @Override
//    protected void beforeRequest(HttpServletRequest request, String message) {
//        System.out.println("beforeRequest " + request.getRequestURI());
//        System.out.println(message);
//    }
//
//    @Override
//    protected void afterRequest(HttpServletRequest request, String message) {
//        System.out.println("afterRequest " + request.getRequestURI());
//        System.out.println(message);
//    }
//}
