package ir.matin.security.bl;

import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.type.ClientType;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class HeaderService {
    @Autowired
    private SecurityValidationService securityValidationService;

    public AuthRequestHeader extractAuthTokenClient(HttpServletRequest request) {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

        String token = this.securityValidationService.validateAuthHeaderToken(request.getHeader(SecurityConstant.HEADER_TOKEN_KEY));
        String clientSerialNumber = this.securityValidationService.validateClientSerialNumber(request.getHeader(SecurityConstant.HEADER_CLIENT_SERIAL_NUMBER_KEY));
        ClientType clientType = this.securityValidationService.validateClientType(request.getHeader(SecurityConstant.HEADER_CLIENT_TYPE_KEY));

        return new AuthRequestHeader(token, clientSerialNumber, clientType);
    }

    public AuthRequestHeader extractPublicTokenClient(HttpServletRequest request) {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

        String token = this.securityValidationService.validatePublicHeaderToken(request.getHeader(SecurityConstant.HEADER_TOKEN_KEY));
        String clientSerialNumber = this.securityValidationService.validateClientSerialNumber(request.getHeader(SecurityConstant.HEADER_CLIENT_SERIAL_NUMBER_KEY));
        ClientType clientType = this.securityValidationService.validateClientType(request.getHeader(SecurityConstant.HEADER_CLIENT_TYPE_KEY));

        return new AuthRequestHeader(token, clientSerialNumber, clientType);
    }

    public AuthRequestHeader extractClient(HttpServletRequest request) {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

        String clientSerialNumber = this.securityValidationService.validateClientSerialNumber(request.getHeader(SecurityConstant.HEADER_CLIENT_SERIAL_NUMBER_KEY));
        ClientType clientType = this.securityValidationService.validateClientType(request.getHeader(SecurityConstant.HEADER_CLIENT_TYPE_KEY));

        return new AuthRequestHeader(null, clientSerialNumber, clientType);
    }
}
