package ir.matin.security.bl;

import io.jsonwebtoken.*;
import ir.matin.database.entity.PersonEntity;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.type.ClientType;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * The Json Web Token (JWT) Service Class,
 * Containing Methods about JWt Management
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class JwtService {
    private final String CLAIM_ISSUER = "iss";
    private final String CLAIM_SUBJECT = "sub";
    private final String CLAIM_AUDIENCE = "aud";
    private final String CLAIM_EXPIRATION = "exp";
    private final String CLAIM_NOT_BEFORE = "nbf";
    private final String CLAIM_ISSUED_AT = "iat";
    private final String CLAIM_JWT_ID = "jti";
    private final String CLAIM_PERSON_ID = "pid";
    //    private final String CLAIM_PERSON_TYPE = "pty";
//    private final String CLAIM_PERSON_ROLES = "prl";
    private final String CLAIM_CLIENT_SERIAL_NUMBER = "csn";
    private final String CLAIM_CLIENT_TYPE = "cty";

    @Autowired
    private ValidationService validationService;

    @Autowired
    private SecurityValidationService securityValidationService;

    @Autowired
    private AvailableService availableService;

    /**
     * Create Access Refresh Tokens with User Context Data
     * <br />Possible {@link SystemException} in Function Call of {@link JwtService#buildToken(Map, String, int)}
     *
     * @param personId A {@link Integer} Instance Representing Serial Number of Client's Application
     * @return A {@link Pair} Instance Representing Access and Refresh Tokens in Json Wen Token Format
     */
    public Pair<String, String> create(Integer personId, AuthRequestHeader authRequestHeader) {
        String clientSerialNumber = authRequestHeader.getClientSerialNumber();
        ClientType clientType = authRequestHeader.getClientType();

        Map<String, Object> claims = new HashMap<>();
        claims.put(this.CLAIM_PERSON_ID, personId);
        claims.put(this.CLAIM_CLIENT_SERIAL_NUMBER, clientSerialNumber);
        claims.put(this.CLAIM_CLIENT_TYPE, clientType.getValue());

        int accessTokenExpirationTime = SecurityConstant.ACCESS_TOKEN_MOBILE_EXPIRATION_HOURS;
        int refreshTokenExpirationTime = SecurityConstant.REFRESH_TOKEN_MOBILE_EXPIRATION_HOURS;
        if (clientType == ClientType.BROWSER) {
            accessTokenExpirationTime = SecurityConstant.ACCESS_TOKEN_BROWSER_EXPIRATION_HOURS;
            refreshTokenExpirationTime = SecurityConstant.REFRESH_TOKEN_BROWSER_EXPIRATION_HOURS;
        }

        String access = this.buildToken(claims, SecurityConstant.ACCESS_TOKEN_SUBJECT, accessTokenExpirationTime);
        String refresh = this.buildToken(claims, SecurityConstant.REFRESH_TOKEN_SUBJECT, refreshTokenExpirationTime);
        return new Pair<>(access, refresh);
    }

    /**
     * Refresh Access Token with Refresh Token
     * <br />Possible {@link SystemException} in Function Call of {@link JwtService#buildToken(Map, String, int)}
     *
     * @param authRequestHeader A {@link String} Instance Representing Refresh token
     * @return A {@link String} Instance Representing New and Refreshed Access Token
     */
    public String refresh(AuthRequestHeader authRequestHeader) {
        Map<String, Object> claims = this.extractClaims(authRequestHeader.getToken(), SecurityConstant.REFRESH_TOKEN_SUBJECT);
        int accessTokenExpirationTime = SecurityConstant.ACCESS_TOKEN_MOBILE_EXPIRATION_HOURS;
        if (authRequestHeader.getClientType() == ClientType.BROWSER)
            accessTokenExpirationTime = SecurityConstant.ACCESS_TOKEN_BROWSER_EXPIRATION_HOURS;

        return this.buildToken(claims, SecurityConstant.ACCESS_TOKEN_SUBJECT, accessTokenExpirationTime);
    }

    /* ****************************************************************************************************************** */

    /**
     * Extract Username from Access Token
     * <br />Possible {@link SystemException} in Function Call of {@link JwtService#extractClaims(String, String)}
     *
     * @param authRequestHeader A {@link String} Instance Representing Access token
     * @return A {@link String} Instance Representing Username of Verified Token
     */
    public PersonEntity extractPersonEntityFromToken(AuthRequestHeader authRequestHeader) {
        Map<String, Object> claims = this.extractClaims(authRequestHeader.getToken(), SecurityConstant.ACCESS_TOKEN_SUBJECT);
        Integer personId = this.validateClaims(claims, authRequestHeader);
        return this.availableService.getUserByTokenClaim(personId, authRequestHeader.getClientSerialNumber(),
                authRequestHeader.getClientType());
    }

    /* ****************************************************************************************************************** */

    /**
     * Build a Token Based on Requested Claims
     *
     * @param claims    A {@link Map} Instance Representing Claims of Json Web Token
     * @param subject   A {@link String} Instance Representing Subject of Json Web Token
     * @param plusHours An {@link int} Instance Representing Expiration Hours of Json Web Token
     * @return A {@link String} Instance Representing Requested Token
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#TOKEN_CREATION_FAILED} when Building Token Failed
     */
    private String buildToken(Map<String, Object> claims, String subject, int plusHours) {
        try {
            Date now = new Date();
            Date expire = Date.from(LocalDateTime.now().plusHours(plusHours).toInstant(ZoneOffset.ofTotalSeconds(SecurityConstant.TOKEN_TIME_ZONE_OFFSET)));
            return Jwts.builder()
                    .setHeaderParam("typ", "JWT")
                    .setClaims(claims)
                    .setIssuer(SecurityConstant.TOKEN_ISSUER)
                    .setSubject(subject)
//                    .setAudience("11")
                    .setExpiration(expire)
//                    .setNotBefore(now)
                    .setIssuedAt(now)
                    .setId(UUID.randomUUID().toString())
                    .compressWith(CompressionCodecs.GZIP)
                    .signWith(SignatureAlgorithm.HS512, SecurityConstant.TOKEN_SECRET_KEY)
                    .compact();
        } catch (RuntimeException e) {
            throw new SystemException(SystemError.TOKEN_CREATION_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    /**
     * Extract Claims of Input Token
     *
     * @param token A {@link String} Instance Representing Json Web Token
     * @return A {@link Map} Instance Representing Claims of Input Token
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#} when Extracting Token Failed
     */
    private Map<String, Object> extractClaims(String token, String subject) {
        try {
            return Jwts.parser()
                    .requireSubject(subject)
                    .setSigningKey(SecurityConstant.TOKEN_SECRET_KEY).parseClaimsJws(token).getBody();
        } catch (MissingClaimException | IncorrectClaimException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_INVALID_TYPE, SystemError.TOKEN_VERIFICATION_INVALID_TYPE.getValue());
        } catch (ExpiredJwtException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_EXPIRED, SystemError.TOKEN_VERIFICATION_EXPIRED.getValue());
        } catch (RuntimeException e) {
            //  JwtException
            //      ClaimJwtException                     thrown after a validation of a JTW claim failed
            //		    ExpiredJwtException               indicating that a JWT was accepted after it expired and must be rejected
            //		    InvalidClaimException             indicating a parsed claim is invalid in some way.  Subclasses reflect the specific reason the claim is invalid
            //			    IncorrectClaimException       thrown when discovering that a required claim does not equal the required value, indicating the JWT is invalid and may not be used
            //			    MissingClaimException         thrown when discovering that a required claim is not present, indicating the JWT is invalid and may not be used
            //		    PrematureJwtException             indicates that a JWT was accepted before it is allowed to be accessed and must be rejected
            //	    CompressionException                  indicating that either compressing or decompressing an JWT body failed
            //	    MalformedJwtException                 thrown when a JWT was not correctly constructed and should be rejected
            //	    RequiredTypeException
            //	    SignatureException                    indicates that either calculating a signature or verifying an existing signature of a JWT failed
            //	    UnsupportedJwtException               thrown when receiving a JWT in a particular format/configuration that does not match the format expected by the application. For example, this exception would be thrown if parsing an unsigned plaintext JWT when the application requires a cryptographically signed Claims JWS instead

            throw new SystemException(SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        }
    }

    private Integer validateClaims(Map<String, Object> claims, AuthRequestHeader authRequestHeader) {
        Integer claimPersonId;
        String claimClientSerialNumber;
        ClientType claimClientType;
        try {
            claimPersonId = this.validationService.validateInteger(claims.get(this.CLAIM_PERSON_ID).toString());
            claimClientSerialNumber = claims.get(this.CLAIM_CLIENT_SERIAL_NUMBER).toString();
            claimClientType = this.securityValidationService.validateClientType(claims.get(this.CLAIM_CLIENT_TYPE).toString());
        } catch (SystemException e) {
            throw new SystemException(SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        }
        String clientSerialNumber = authRequestHeader.getClientSerialNumber();
        ClientType clientType = authRequestHeader.getClientType();

        if (!clientSerialNumber.equals(claimClientSerialNumber) || !clientType.equals(claimClientType))
            throw new SystemException(SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());

        return claimPersonId;
    }
}
