package ir.matin.security.bl;

//import ir.matin.database.dao.PersonLicenseDao;

import ir.matin.database.entity.PersonEntity;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

//import ir.matin.database.entity.PersonLicenseEntity;

@Service
public class AccessService {
//    @Autowired
//    private PersonLicenseDao personLicenseDao;

    @Autowired
    private HeaderService headerService;

    /**
     * The {@link JwtService} Instance Representing Json Web Token Service
     */
    @Autowired
    private JwtService jwtService;

    /* ****************************************************************************************************************** */

    public PersonEntity getLoggedInUser(HttpServletRequest request) {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

        PersonEntity personEntity = (PersonEntity) request.getAttribute(SecurityConstant.REQUEST_EXTENDED_ATTRIBUTE);
        return personEntity;
    }

    public PersonEntity getPublicUser(HttpServletRequest request) {
        if (request == null)
            throw new SystemException(SystemError.ILLEGAL_ARGUMENT, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());

        PersonEntity personEntity = null;
        try {
            AuthRequestHeader authRequestHeader = this.headerService.extractPublicTokenClient(request);
            if (authRequestHeader.getToken() != null)
                personEntity = this.jwtService.extractPersonEntityFromToken(authRequestHeader);
        } catch (SystemException ignored) {
        }
        return personEntity;
    }

    /* ****************************************************************************************************************** */

//    @PostAuthorize("returnObject")
//    public boolean hasLicense(Integer personId, String license) {
//        PersonLicenseEntity result = this.personLicenseDao.hasLicense(personId, license, new Timestamp(System.currentTimeMillis()));
//        return result != null;
//    }
}
