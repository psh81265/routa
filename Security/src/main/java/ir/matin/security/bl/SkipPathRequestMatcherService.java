package ir.matin.security.bl;

import ir.matin.security.data.type.AuthenticationPatternType;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Path Request Matcher Class,
 * Matching Default Pattern and Processing Them in Spring Security
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Component
public class SkipPathRequestMatcherService implements RequestMatcher {
    private RequestMatcher skipMatcher;
    private RequestMatcher authMatcher;

    public SkipPathRequestMatcherService() {
        this.skipMatcher = new AntPathRequestMatcher(AuthenticationPatternType.SKIP_PATTERN.getPattern());
        this.authMatcher = new AntPathRequestMatcher(AuthenticationPatternType.AUTH_PATTERN.getPattern());
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        return !skipMatcher.matches(request) && authMatcher.matches(request);
    }
}
