package ir.matin.security.bl;

import ir.matin.security.data.type.ClientType;
import ir.matin.security.data.type.RoleCategoryType;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.stereotype.Service;

/**
 * The Validation Service Class,
 * Containing Methods about String Patterns and Input Validation
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class SecurityValidationService {
    public String validateAuthHeaderToken(String value) {
        // 7 characters for "Bearer " and 2 characters for two dots
        if (value == null || value.length() < 10)
            throw new SystemException(SystemError.INVALID_TOKEN_HEADER, SystemError.INVALID_TOKEN_HEADER.getValue());
        return value.replaceAll("Bearer ", "");
    }

    public String validatePublicHeaderToken(String value) {
        // 7 characters for "Bearer " and 2 characters for two dots
        if (value == null || value.length() < 10)
            return null;
        return value.replaceAll("Bearer ", "");
    }

    /**
     * Validate Client Type
     *
     * @param value A {@link String} Instance Representing Client Serial Number
     * @return A {@link ClientType} Instance Representing Validated Client Serial Number
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_CLIENT_SERIAL_NUMBER} when clientSerialNumber is not valid
     */
    public String validateClientSerialNumber(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_CLIENT_SERIAL_NUMBER, SystemError.INVALID_CLIENT_SERIAL_NUMBER.getValue());
        try {
            return (value);
        } catch (NumberFormatException e) {
            throw new SystemException(SystemError.INVALID_CLIENT_SERIAL_NUMBER, SystemError.INVALID_CLIENT_SERIAL_NUMBER.getValue());
        }
    }

    /**
     * Validate Client Type
     *
     * @param value A {@link String} Instance Representing Client Type
     * @return A {@link ClientType} Instance Representing Validated Client Type
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_CLIENT_TYPE} when clientType is not valid
     */
    public ClientType validateClientType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_CLIENT_TYPE, SystemError.INVALID_CLIENT_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "m":
            case "mobile":
                return ClientType.MOBILE;
            case "d":
            case "desktop":
                return ClientType.DESKTOP;
            case "b":
            case "browser":
                return ClientType.BROWSER;
            default:
                throw new SystemException(SystemError.INVALID_CLIENT_TYPE, SystemError.INVALID_CLIENT_TYPE.getValue());
        }
    }

    public RoleCategoryType validateRoleCategoryType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "all":
                return RoleCategoryType.ALL;
            case "adm_own":
            case "admin_owner":
                return RoleCategoryType.ADMIN_OWNER;
            case "adm_cmp":
            case "admin_company":
                return RoleCategoryType.ADMIN_COMPANY;
            case "cus":
            case "customer":
                return RoleCategoryType.CUSTOMER;
            case "opr_own":
            case "operator_owner":
                return RoleCategoryType.OPERATOR_OWNER;
            case "opr_cmp":
            case "operator_company":
                return RoleCategoryType.OPERATOR_COMPANY;
            case "gman":
            case "gisman":
                return RoleCategoryType.GISMAN;
            default:
                throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }
}
