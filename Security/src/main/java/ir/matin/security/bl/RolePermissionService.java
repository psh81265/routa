package ir.matin.security.bl;

import ir.matin.database.dao.PermissionDao;
import ir.matin.database.dao.RoleDao;
import ir.matin.database.entity.PermissionEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.security.data.object.PermissionRule;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Role and Permission Service Class,
 * Containing Methods User Authorization based on Database
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class RolePermissionService {
    @Autowired
    private RoleDao roleDao;

    /**
     * The {@link PermissionDao} Instance Representing Permission Dao Object
     */
    @Autowired
    private PermissionDao permissionDao;

    public List<RoleEntity> listRole() {
        List<RoleEntity> roles = this.roleDao.list();
        List<String> forbiddenRoles = Collections.singletonList(SecurityConstant.ROLE_ADMIN_OWNER);
        roles = roles.stream().filter(roleEntity -> !forbiddenRoles.contains(roleEntity.getName())).collect(Collectors.toList());
        if (roles.isEmpty())
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());
        return roles;
    }

    /**
     * Load Permissions from Database and Build Permission Rules Based on That
     *
     * @return A {@link List<PermissionRule>} Instance Representing list of Permission Rules
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#EMPTY_RESULTS} when no Permission Found
     */
    public List<PermissionRule> permissionRules() {
        List<PermissionEntity> permissionEntities = this.permissionDao.list();
        if (permissionEntities.isEmpty())
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());
        return permissionEntities.stream().map(PermissionRule::new)
                .collect(Collectors.toList());
    }
}
