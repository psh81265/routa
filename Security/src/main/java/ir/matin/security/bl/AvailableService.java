package ir.matin.security.bl;

import ir.matin.database.dao.DeviceDao;
import ir.matin.database.dao.PersonDao;
import ir.matin.database.entity.DeviceEntity;
import ir.matin.database.entity.PersonEntity;
import ir.matin.security.data.type.ClientType;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class AvailableService {

    @Autowired
    private PersonDao personDao;
    @Autowired
    private DeviceDao deviceDao;

    public PersonEntity getUserByTokenClaim(Integer personId, String clientSerialNumber, ClientType clientType) {
        PersonEntity personEntity = this.personDao.get(personId);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        if (personEntity.getBlock())
            throw new SystemException(SystemError.USER_BLOCKED, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        if (!personEntity.getConfirm())
            throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        if (!personEntity.getActive())
            throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());

        if (clientType.equals(ClientType.MOBILE)) {
            if (!personEntity.getLogin() || personEntity.getClientserialnumber() == null)
                throw new SystemException(SystemError.USER_NOT_LOGIN, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
            if (!clientSerialNumber.equals(personEntity.getClientserialnumber()))
                throw new SystemException(SystemError.INVALID_CLIENT_SERIAL_NUMBER, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        }
        return personEntity;
    }

    public PersonEntity getUserByLogin(String username, String clientSerialNumber, ClientType clientType) {
        PersonEntity personEntity = this.personDao.getByUsername(username);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.USER_NOT_FOUND.getValue());
        if (personEntity.getBlock())
            throw new SystemException(SystemError.USER_BLOCKED, SystemError.USER_BLOCKED.getValue());
        if (!personEntity.getConfirm())
            throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.USER_NOT_CONFIRM.getValue());
        if (!personEntity.getActive())
            throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.USER_NOT_ACTIVE.getValue());

        if (clientType == ClientType.MOBILE) {
            if (personEntity.getLogin() && personEntity.getClientserialnumber() != null) {
                if (!clientSerialNumber.equals(personEntity.getClientserialnumber())) {
                    throw new SystemException(SystemError.USER_ALREADY_LOGGED_IN, SystemError.USER_ALREADY_LOGGED_IN.getValue());
                }
            }
        }
        return personEntity;
    }

    public PersonEntity getUserByUsername(String username, Boolean confirmCheck, Boolean activateCheck) {
        PersonEntity personEntity = this.personDao.getByUsername(username);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.USER_NOT_FOUND.getValue());
        if (personEntity.getBlock())
            throw new SystemException(SystemError.USER_BLOCKED, SystemError.USER_BLOCKED.getValue());
        if (confirmCheck && !personEntity.getConfirm())
            throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.USER_NOT_CONFIRM.getValue());
        if (activateCheck && !personEntity.getActive())
            throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.USER_NOT_ACTIVE.getValue());

        return personEntity;
    }

    public PersonEntity getUserByUsername(String username, Boolean confirmCheck, Boolean activateCheck, Integer validationCode) {
        PersonEntity personEntity = this.personDao.getByUsername(username);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.USER_NOT_FOUND.getValue());
        if (personEntity.getBlock())
            throw new SystemException(SystemError.USER_BLOCKED, SystemError.USER_BLOCKED.getValue());
        if (confirmCheck && !personEntity.getConfirm())
            throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.USER_NOT_CONFIRM.getValue());
        if (activateCheck && !personEntity.getActive())
            throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.USER_NOT_ACTIVE.getValue());

        if (!validationCode.equals(personEntity.getValidationcode()))
            throw new SystemException(SystemError.INVALID_VALIDATION_CODE, SystemError.INVALID_VALIDATION_CODE.getValue());

        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        if (personEntity.getValidationexpiretime().compareTo(now) < 0)
            throw new SystemException(SystemError.VALIDATION_CODE_EXPIRED, SystemError.VALIDATION_CODE_EXPIRED.getValue());

        return personEntity;
    }

    public DeviceEntity getDeviceByTokenClaim(Integer deviceId, Long clientSerialNumber, ClientType clientType) {
        DeviceEntity deviceEntity = this.deviceDao.get(deviceId);
        if (deviceEntity == null)
            return null;

        //if (clientType.equals(ClientType.MOBILE)) {
        //   if (!deviceEntity.getLogin() || deviceEntity.getDeviceserialnumber() == null)
        //        throw new SystemException(SystemError.USER_NOT_LOGIN, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        //    if (!clientSerialNumber.equals(deviceEntity.getDeviceserialnumber()))
        //        throw new SystemException(SystemError.INVALID_CLIENT_SERIAL_NUMBER, SystemError.TOKEN_VERIFICATION_FAILED, SystemError.TOKEN_VERIFICATION_FAILED.getValue());
        //}
        return deviceEntity;
    }

    public PersonEntity getUserbySerialNumber(String clientSerialNumber, ClientType clientType) {
        PersonEntity personEntity = this.personDao.getBySerialNumber(clientSerialNumber);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.USER_NOT_FOUND.getValue());
        //if (personEntity.getBlock())
        //    throw new SystemException(SystemError.USER_BLOCKED, SystemError.USER_BLOCKED.getValue());
        //if (!personEntity.getConfirm())
        //    throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.USER_NOT_CONFIRM.getValue());
        //if (!personEntity.getActive())
        //    throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.USER_NOT_ACTIVE.getValue());

        if (clientType == ClientType.MOBILE) {
            if (personEntity.getLogin() && personEntity.getClientserialnumber() != null) {
                if (!clientSerialNumber.equals(personEntity.getClientserialnumber())) {
                    throw new SystemException(SystemError.USER_ALREADY_LOGGED_IN, SystemError.USER_ALREADY_LOGGED_IN.getValue());
                }
            }
        }
        return personEntity;
    }

    public PersonEntity getCustomerbySerialNumber(String clientSerialNumber, ClientType clientType) {
        PersonEntity personEntity = this.personDao.getCustomerBySerialNumber(clientSerialNumber);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.USER_NOT_FOUND.getValue());
        //if (personEntity.getBlock())
        //    throw new SystemException(SystemError.USER_BLOCKED, SystemError.USER_BLOCKED.getValue());
        //if (!personEntity.getConfirm())
        //    throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.USER_NOT_CONFIRM.getValue());
        //if (!personEntity.getActive())
        //    throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.USER_NOT_ACTIVE.getValue());

        if (clientType == ClientType.MOBILE) {
            if (personEntity.getLogin() && personEntity.getClientserialnumber() != null) {
                if (!clientSerialNumber.equals(personEntity.getClientserialnumber())) {
                    throw new SystemException(SystemError.USER_ALREADY_LOGGED_IN, SystemError.USER_ALREADY_LOGGED_IN.getValue());
                }
            }
        }
        return personEntity;
    }

    public PersonEntity getGuestbySerialNumber(String clientSerialNumber, ClientType clientType) {
        PersonEntity personEntity = this.personDao.getGuestBySerialNumber(clientSerialNumber);
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.USER_NOT_FOUND.getValue());
        //if (personEntity.getBlock())
        //    throw new SystemException(SystemError.USER_BLOCKED, SystemError.USER_BLOCKED.getValue());
        //if (!personEntity.getConfirm())
        //    throw new SystemException(SystemError.USER_NOT_CONFIRM, SystemError.USER_NOT_CONFIRM.getValue());
        //if (!personEntity.getActive())
        //    throw new SystemException(SystemError.USER_NOT_ACTIVE, SystemError.USER_NOT_ACTIVE.getValue());

        if (clientType == ClientType.MOBILE) {
            if (personEntity.getLogin() && personEntity.getClientserialnumber() != null) {
                if (!clientSerialNumber.equals(personEntity.getClientserialnumber())) {
                    throw new SystemException(SystemError.USER_ALREADY_LOGGED_IN, SystemError.USER_ALREADY_LOGGED_IN.getValue());
                }
            }
        }
        return personEntity;
    }
}
