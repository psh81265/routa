package ir.matin.database.bl;

import ir.matin.database.sp.ResetSP;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Database Service Class,
 * Containing Methods about Database
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class DatabaseResetService {

    /**
     * The {@link ResetSP} Instance Representing Reset Stored Procedures
     */
    @Autowired
    private ResetSP resetSP;

    public void databaseCleanse() {
        this.resetSP.databaseCleanse();
    }

    public void databaseCleansePermission() {
        this.resetSP.databaseCleansePermission();
    }

    /**
     * Reset Elastic and Oracle Database to Default Values
     * <br />Possible {@link SystemException} in Function Call of {@link ResetSP#databaseCleanse()}
     * <br />Possible {@link SystemException} in Function Call of {@link ResetSP#databaseDefault()}
     */
    public void databaseDefault() {
        this.resetSP.databaseCleanse();
        this.resetSP.databaseDefault();
    }

    /**
     * Reset Elastic and Oracle Database to Default Values
     * <br />Possible {@link SystemException} in Function Call of {@link ResetSP#databaseCleanse()}
     * <br />Possible {@link SystemException} in Function Call of {@link ResetSP#databaseDefault()}
     * <br />Possible {@link SystemException} in Function Call of {@link ResetSP#databaseSample()}
     */
    public void databaseSample() {
        this.resetSP.databaseCleanse();
        this.resetSP.databaseDefault();
        this.resetSP.databaseSample();
    }
}
