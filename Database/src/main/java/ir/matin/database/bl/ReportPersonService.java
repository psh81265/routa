package ir.matin.database.bl;

import ir.matin.database.dao.GISManDao;
import ir.matin.database.dao.OperatorDao;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.GISManEntity;
import ir.matin.database.entity.OperatorEntity;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SortOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportPersonService {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private OperatorDao operatorDao;

    @Autowired
    private GISManDao gisManDao;

    public ReportResult<OperatorEntity> reportOperatorSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                             Boolean confirm, Boolean active, Boolean block, Boolean selfRegister,
                                                             String username, String firstname, String lastname, String nationalId,
                                                             String mobile, String phone, String email, String father, String address,
                                                             String zipCode, Timestamp birthdayMin, Timestamp birthdayMax,
                                                             Integer sex, Integer creditMin, Integer creditMax, String speciality,
                                                             Integer salaryMin, Integer salaryMax) {
        List<String> columns = this.validationService.fieldNames(OperatorEntity.class);
        List<SortOption> sortOptions = sortBy.stream().filter(item -> columns.contains(item.getColumn())).collect(Collectors.toList());

        ReportCondition reportCondition = this.generatePersonReportCondition(pageSize, pageNumber, sortBy, export, query,
                confirm, active, block, selfRegister, username, firstname, lastname, nationalId, mobile, phone,
                email, father, address, zipCode, birthdayMin, birthdayMax, sex, creditMin, creditMax);
        reportCondition.setSortOptions(sortOptions);

        if (query != null) {
            if (query.getFirst() == Integer.class) {
                reportCondition.getEqualCondition().put("salary", query.getSecond());
            }
            if (query.getFirst() == String.class) {
                reportCondition.getLikeCondition().put("speciality", query.getSecond().toString());
            }
        } else {
            if (speciality != null)
                reportCondition.getLikeCondition().put("speciality", speciality);
            if (salaryMin != null)
                reportCondition.getMinNumberCondition().put("salary", salaryMin);
            if (salaryMax != null)
                reportCondition.getMaxNumberCondition().put("salary", salaryMax);
        }

        if (query != null)
            return this.operatorDao.reportOperatorSimple(reportCondition);
        else
            return this.operatorDao.reportOperatorAdvance(reportCondition);
    }

    public ReportResult<GISManEntity> reportGisManSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                           Boolean confirm, Boolean active, Boolean block, Boolean selfRegister,
                                                           String username, String firstname, String lastname, String nationalId,
                                                           String mobile, String phone, String email, String father, String address,
                                                           String zipCode, Timestamp birthdayMin, Timestamp birthdayMax,
                                                           Integer sex, Integer creditMin, Integer creditMax, String speciality,
                                                           Integer salaryMin, Integer salaryMax) {
        List<String> columns = this.validationService.fieldNames(OperatorEntity.class);
        List<SortOption> sortOptions = sortBy.stream().filter(item -> columns.contains(item.getColumn())).collect(Collectors.toList());

        ReportCondition reportCondition = this.generatePersonReportCondition(pageSize, pageNumber, sortBy, export, query,
                confirm, active, block, selfRegister, username, firstname, lastname, nationalId, mobile, phone,
                email, father, address, zipCode, birthdayMin, birthdayMax, sex, creditMin, creditMax);
        reportCondition.setSortOptions(sortOptions);

        if (query != null) {
            if (query.getFirst() == Integer.class) {
                reportCondition.getEqualCondition().put("salary", query.getSecond());
            }
            if (query.getFirst() == String.class) {
                reportCondition.getLikeCondition().put("speciality", query.getSecond().toString());
            }
        } else {
            if (speciality != null)
                reportCondition.getLikeCondition().put("speciality", speciality);
            if (salaryMin != null)
                reportCondition.getMinNumberCondition().put("salary", salaryMin);
            if (salaryMax != null)
                reportCondition.getMaxNumberCondition().put("salary", salaryMax);
        }

        if (query != null)
            return this.gisManDao.reportGISManSimple(reportCondition);
        else
            return this.gisManDao.reportGISManAdvance(reportCondition);
    }

    /* *************************************************************************************** */

    private ReportCondition generatePersonReportCondition(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                          Boolean confirm, Boolean active, Boolean block, Boolean selfRegister,
                                                          String username, String firstname, String lastname, String nationalId,
                                                          String mobile, String phone, String email, String father, String address,
                                                          String zipCode, Timestamp birthdayMin, Timestamp birthdayMax,
                                                          Integer sex, Integer creditMin, Integer creditMax) {
        Map<String, Object> equalCondition = new HashMap<>();
        Map<String, String> likeCondition = new HashMap<>();
        Map<String, Number> minNumberCondition = new HashMap<>();
        Map<String, Number> maxNumberCondition = new HashMap<>();
        Map<String, Timestamp> minTimestampCondition = new HashMap<>();
        Map<String, Timestamp> maxTimestampCondition = new HashMap<>();

        equalCondition.put("deleted", false);

        if (query != null) {
            if (query.getFirst() == Integer.class) {
                equalCondition.put("credit", query.getSecond());
            }
            if (query.getFirst() == String.class) {
                likeCondition.put("username", query.getSecond().toString());
                likeCondition.put("firstname", query.getSecond().toString());
                likeCondition.put("lastname", query.getSecond().toString());
                likeCondition.put("nationalid", query.getSecond().toString());
                likeCondition.put("mobile", query.getSecond().toString());
                likeCondition.put("phone", query.getSecond().toString());
                likeCondition.put("email", query.getSecond().toString());
                likeCondition.put("father", query.getSecond().toString());
                likeCondition.put("address", query.getSecond().toString());
                likeCondition.put("zipcode", query.getSecond().toString());
            }
        } else {
            if (confirm != null)
                equalCondition.put("confirm", confirm);
            if (active != null)
                equalCondition.put("active", active);
            if (block != null)
                equalCondition.put("block", block);
            if (selfRegister != null)
                equalCondition.put("selfregister", selfRegister);
            if (username != null)
                likeCondition.put("username", username);
            if (firstname != null)
                likeCondition.put("firstname", firstname);
            if (lastname != null)
                likeCondition.put("lastname", lastname);
            if (nationalId != null)
                likeCondition.put("nationalid", nationalId);
            if (mobile != null)
                likeCondition.put("mobile", mobile);
            if (phone != null)
                likeCondition.put("phone", phone);
            if (email != null)
                likeCondition.put("email", email);
            if (father != null)
                likeCondition.put("father", father);
            if (address != null)
                likeCondition.put("address", address);
            if (zipCode != null)
                likeCondition.put("zipcode", zipCode);
            if (birthdayMin != null)
                minTimestampCondition.put("birthday", birthdayMin);
            if (birthdayMax != null)
                maxTimestampCondition.put("birthday", birthdayMax);
            if (sex != null)
                equalCondition.put("sex", sex);
            if (creditMin != null)
                minNumberCondition.put("credit", creditMin);
            if (creditMax != null)
                maxNumberCondition.put("credit", creditMax);
        }

        ReportCondition reportCondition = new ReportCondition(pageSize, pageNumber, null, export);
        reportCondition.setEqualCondition(equalCondition);
        reportCondition.setLikeCondition(likeCondition);
        reportCondition.setMinNumberCondition(minNumberCondition);
        reportCondition.setMaxNumberCondition(maxNumberCondition);
        reportCondition.setMinTimestampCondition(minTimestampCondition);
        reportCondition.setMaxTimestampCondition(maxTimestampCondition);

        return reportCondition;
    }
}
