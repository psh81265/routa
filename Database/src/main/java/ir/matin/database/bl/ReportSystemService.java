package ir.matin.database.bl;

import ir.matin.database.dao.AppVersionDao;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.AppVersionEntity;
import ir.matin.database.entity.TypePersonReasonEntity;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SortOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportSystemService {

    @Autowired
    private AppVersionDao appVersionDao;

    @Autowired
    private ValidationService validationService;

    public ReportResult<AppVersionEntity> reportAppVersion(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                           Integer appType, String version, Timestamp releaseMin, Timestamp releaseMax, Boolean stable, Boolean force) {
        List<String> columns = this.validationService.fieldNames(TypePersonReasonEntity.class);
        List<SortOption> sortOptions = sortBy.stream().filter(item -> columns.contains(item.getColumn())).collect(Collectors.toList());

        ReportCondition reportCondition = new ReportCondition(pageSize, pageNumber, sortOptions, export);
        Map<String, Object> equalCondition = new HashMap<>();
        Map<String, String> likeCondition = new HashMap<>();
        Map<String, Timestamp> minTimestampCondition = new HashMap<>();
        Map<String, Timestamp> maxTimestampCondition = new HashMap<>();

        if (query != null) {
            if (query.getFirst() == String.class) {
                likeCondition.put("VERSION", query.getSecond().toString());
            }
        } else {
            if (appType != null)
                equalCondition.put("APPTYPE", appType);
            if (version != null)
                likeCondition.put("VERSION", version);
            if (releaseMin != null)
                minTimestampCondition.put("RELEASE", releaseMin);
            if (releaseMax != null)
                maxTimestampCondition.put("RELEASE", releaseMax);
            if (stable != null)
                equalCondition.put("STABLE", stable);
            if (force != null)
                equalCondition.put("FORCE", force);
        }

        reportCondition.setEqualCondition(equalCondition);
        reportCondition.setLikeCondition(likeCondition);
        reportCondition.setMinTimestampCondition(minTimestampCondition);
        reportCondition.setMaxTimestampCondition(maxTimestampCondition);

        if (query != null)
            return this.appVersionDao.reportAppVersionSimple(reportCondition);
        else
            return this.appVersionDao.reportAppVersionAdvance(reportCondition);
    }
}
