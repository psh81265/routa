package ir.matin.database.bl;

import ir.matin.database.dao.*;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.*;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SortOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportTypeService {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private TypeProvinceDao typeProvinceDao;

    @Autowired
    private TypeCityDao typeCityDao;

    @Autowired
    private TypePersonReasonDao typePersonReasonDao;

    public ReportResult<TypeProvinceEntity> reportProvinceSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                                 String name, Double latitudeMin, Double latitudeMax,
                                                                 Double longitudeMin, Double longitudeMax) {
        List<String> columns = this.validationService.fieldNames(TypeProvinceEntity.class);
        List<SortOption> sortOptions = sortBy.stream().filter(item -> columns.contains(item.getColumn())).collect(Collectors.toList());

        ReportCondition reportCondition = new ReportCondition(pageSize, pageNumber, sortOptions, export);
        Map<String, Object> equalCondition = new HashMap<>();
        Map<String, String> likeCondition = new HashMap<>();
        Map<String, Number> minNumberCondition = new HashMap<>();
        Map<String, Number> maxNumberCondition = new HashMap<>();

        equalCondition.put("deleted", false);

        if (query != null) {
            if (query.getFirst() == String.class) {
                likeCondition.put("name", query.getSecond().toString());
            }
        } else {
            if (name != null)
                likeCondition.put("name", name);
            if (latitudeMin != null)
                minNumberCondition.put("latitude", latitudeMin);
            if (latitudeMax != null)
                maxNumberCondition.put("latitude", latitudeMax);
            if (longitudeMin != null)
                minNumberCondition.put("longitude", longitudeMin);
            if (longitudeMax != null)
                maxNumberCondition.put("longitude", longitudeMax);
        }

        reportCondition.setEqualCondition(equalCondition);
        reportCondition.setLikeCondition(likeCondition);
        reportCondition.setMinNumberCondition(minNumberCondition);
        reportCondition.setMaxNumberCondition(maxNumberCondition);

        if (query != null)
            return this.typeProvinceDao.reportTypeProvinceSimple(reportCondition);
        else
            return this.typeProvinceDao.reportTypeProvinceAdvance(reportCondition);
    }

    public ReportResult<TypeCityEntity> reportCitySimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                         String name, Double latitudeMin, Double latitudeMax,
                                                         Double longitudeMin, Double longitudeMax) {
        List<String> columns = this.validationService.fieldNames(TypeCityEntity.class);
        List<SortOption> sortOptions = sortBy.stream().filter(item -> columns.contains(item.getColumn())).collect(Collectors.toList());

        ReportCondition reportCondition = new ReportCondition(pageSize, pageNumber, sortOptions, export);
        Map<String, Object> equalCondition = new HashMap<>();
        Map<String, String> likeCondition = new HashMap<>();
        Map<String, Number> minNumberCondition = new HashMap<>();
        Map<String, Number> maxNumberCondition = new HashMap<>();

        equalCondition.put("deleted", false);

        if (query != null) {
            if (query.getFirst() == String.class) {
                likeCondition.put("name", query.getSecond().toString());
            }
        } else {
            if (name != null)
                likeCondition.put("name", name);
            if (latitudeMin != null)
                minNumberCondition.put("latitude", latitudeMin);
            if (latitudeMax != null)
                maxNumberCondition.put("latitude", latitudeMax);
            if (longitudeMin != null)
                minNumberCondition.put("longitude", longitudeMin);
            if (longitudeMax != null)
                maxNumberCondition.put("longitude", longitudeMax);
        }

        reportCondition.setEqualCondition(equalCondition);
        reportCondition.setLikeCondition(likeCondition);
        reportCondition.setMinNumberCondition(minNumberCondition);
        reportCondition.setMaxNumberCondition(maxNumberCondition);

        if (query != null)
            return this.typeCityDao.reportTypeCitySimple(reportCondition);
        else
            return this.typeCityDao.reportTypeCityAdvance(reportCondition);
    }

    public ReportResult<TypePersonReasonEntity> reportPersonReasonSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                                         String name, Integer type) {
        List<String> columns = this.validationService.fieldNames(TypePersonReasonEntity.class);
        List<SortOption> sortOptions = sortBy.stream().filter(item -> columns.contains(item.getColumn())).collect(Collectors.toList());

        ReportCondition reportCondition = new ReportCondition(pageSize, pageNumber, sortOptions, export);
        Map<String, Object> equalCondition = new HashMap<>();
        Map<String, String> likeCondition = new HashMap<>();

        equalCondition.put("deleted", false);

        if (query != null) {
            if (query.getFirst() == String.class) {
                likeCondition.put("name", query.getSecond().toString());
            }
        } else {
            if (name != null)
                likeCondition.put("name", name);
            if (type != null)
                equalCondition.put("type", type);
        }

        reportCondition.setEqualCondition(equalCondition);
        reportCondition.setLikeCondition(likeCondition);

        if (query != null)
            return this.typePersonReasonDao.reportTypePersonReasonSimple(reportCondition);
        else
            return this.typePersonReasonDao.reportTypePersonReasonAdvance(reportCondition);
    }
}
