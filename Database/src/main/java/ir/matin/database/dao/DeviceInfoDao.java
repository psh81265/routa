package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.DeviceInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class DeviceInfoDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public DeviceInfoEntity get(Integer id) {
        return this.baseDaoInterface.getById(DeviceInfoEntity.class, id);
    }

    public List<DeviceInfoEntity> list() {
        return this.baseDaoInterface.list(DeviceInfoEntity.class);
    }

    public Integer add(DeviceInfoEntity deviceInfoEntity) {
        return this.baseDaoInterface.save(deviceInfoEntity);
    }

    public void update(DeviceInfoEntity deviceInfoEntity) {
        baseDaoInterface.update(deviceInfoEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(DeviceInfoEntity.class, id);
    }


    /* ****************************************************************************************************************** */

}
