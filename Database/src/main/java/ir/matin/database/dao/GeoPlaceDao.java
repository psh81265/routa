package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.BookmarkEntity;
import ir.matin.database.entity.CustomerEntity;
import ir.matin.database.entity.GeoPlaceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class GeoPlaceDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public GeoPlaceEntity get(Integer id) {
        return this.baseDaoInterface.getById(GeoPlaceEntity.class, id);
    }

    public List<GeoPlaceEntity> list() {
        return this.baseDaoInterface.list(GeoPlaceEntity.class);
    }

    public Integer add(GeoPlaceEntity geoPlaceEntity) {
        return this.baseDaoInterface.save(geoPlaceEntity);
    }

    public void update(GeoPlaceEntity geoPlaceEntity) {
        baseDaoInterface.update(geoPlaceEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(CustomerEntity.class, id);
    }
    /* ****************************************************************************************************************** */

}
