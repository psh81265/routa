package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeScoreEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypePointDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeScoreEntity get(Integer id) {
        return this.baseDaoInterface.getById(TypeScoreEntity.class, id);
    }

    public List<TypeScoreEntity> list() {
        return this.baseDaoInterface.list(TypeScoreEntity.class);
    }

    public Integer add(TypeScoreEntity typeScoreEntity) {
        return this.baseDaoInterface.save(typeScoreEntity);
    }

    public void update(TypeScoreEntity typeScoreEntity) {
        baseDaoInterface.update(typeScoreEntity);
    }

    /* ****************************************************************************************************************** */

    public TypeScoreEntity getTypeScoreByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return baseDaoInterface.getByAndConditions(TypeScoreEntity.class, map);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typepointid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypeScoreEntity.class, conditions, parameters);
    }
}
