package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.PermissionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@Transactional
public class PermissionDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public PermissionEntity get(Integer id) {
        return this.baseDaoInterface.getById(PermissionEntity.class, id);
    }

    public List<PermissionEntity> list() {
        return this.baseDaoInterface.list(PermissionEntity.class);
    }

    /* ****************************************************************************************************************** */

    public List<String> listAllNames() {
        List<PermissionEntity> result = this.baseDaoInterface.list(PermissionEntity.class);
        return (result != null) ? result.stream().map(PermissionEntity::getName).collect(Collectors.toList()) : null;
    }

}
