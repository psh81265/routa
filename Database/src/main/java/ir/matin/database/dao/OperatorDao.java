package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.OperatorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class OperatorDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public OperatorEntity get(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("personid", id);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(OperatorEntity.class, map);
    }

    public List<OperatorEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(OperatorEntity.class, map);
    }

    public Integer add(OperatorEntity operatorEntity) {
        return this.baseDaoInterface.save(operatorEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(OperatorEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public ReportResult<OperatorEntity> reportOperatorSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(OperatorEntity.class, reportCondition);
        List<OperatorEntity> result = this.baseDaoInterface.reportByOrConditions(OperatorEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<OperatorEntity> reportOperatorAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(OperatorEntity.class, reportCondition);
        List<OperatorEntity> result = this.baseDaoInterface.reportByAndConditions(OperatorEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

}
