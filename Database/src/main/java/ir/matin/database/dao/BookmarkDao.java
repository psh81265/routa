package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.AppVersionEntity;
import ir.matin.database.entity.BookmarkEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Book;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class BookmarkDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public BookmarkEntity get(Integer id) {
        return this.baseDaoInterface.getById(BookmarkEntity.class, id);
    }

    public List<BookmarkEntity> list() {
        return this.baseDaoInterface.list(BookmarkEntity.class);
    }

    public Integer add(BookmarkEntity bookmarkEntity) {
        return this.baseDaoInterface.save(bookmarkEntity);
    }

    public void update(BookmarkEntity bookmarkEntity) {
        baseDaoInterface.update(bookmarkEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(BookmarkEntity.class, id);
    }

    /* ****************************************************************************************************************** */

    public List<BookmarkEntity> getBookmarkByCustomerID(Integer customerID) {
        Map<String, Object> map = new HashMap<>();
        map.put("customerid", customerID);
        return baseDaoInterface.listByAndConditions(BookmarkEntity.class, map);
    }

    public List<BookmarkEntity> getBookmarkByLatAndLon(Double lat, Double lon) {
        Map<String, Object> map = new HashMap<>();
        map.put("latitude", lat);
        map.put("longitude", lon);
        return baseDaoInterface.listByAndConditions(BookmarkEntity.class, map);
    }

    public List<BookmarkEntity> getBookmarkByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return baseDaoInterface.listByAndConditions(BookmarkEntity.class, map);
    }

    public List<BookmarkEntity> getBookmarkByCustomerIDAndName(Integer customerID, String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("customerid", customerID);
        map.put("name", name);
        return baseDaoInterface.listByAndConditions(BookmarkEntity.class, map);
    }

    public List<BookmarkEntity> getBookmarkByType(int typeBookmarkID) {
        Map<String, Object> map = new HashMap<>();
        map.put("typebookmarkid", typeBookmarkID);
        return baseDaoInterface.listByAndConditions(BookmarkEntity.class, map);
    }

    public List<BookmarkEntity> getByCustomerIDAndType(int customerID, int typeBookmarkID) {
        Map<String, Object> map = new HashMap<>();
        map.put("typebookmarkid", typeBookmarkID);
        map.put("customerid", customerID);
        return baseDaoInterface.listByAndConditions(BookmarkEntity.class, map);
    }



}
