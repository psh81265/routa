package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.data.type.PersonStateType;
import ir.matin.database.entity.TypePersonReasonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypePersonReasonDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypePersonReasonEntity get(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("typepersonreasonid", id);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public Integer add(TypePersonReasonEntity typePersonReasonEntity) {
        return this.baseDaoInterface.save(typePersonReasonEntity);
    }

    public void update(TypePersonReasonEntity typePersonReasonEntity) {
        this.baseDaoInterface.update(typePersonReasonEntity);
    }

    /* ****************************************************************************************************************** */

    public List<TypePersonReasonEntity> listEnabledLogin() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.LOGIN.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledLogout() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.LOGOUT.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledConfirm() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.CONFIRM.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledUnConfirm() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.UN_CONFIRM.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledActivate() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.ACTIVATE.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledDeActivate() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.DE_ACTIVATE.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledBlock() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.BLOCK.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    public List<TypePersonReasonEntity> listEnabledUnBlock() {
        Map<String, Object> map = new HashMap<>();
        map.put("type", PersonStateType.UN_BLOCK.getValue());
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypePersonReasonEntity.class, map);
    }

    /* ****************************************************************************************************************** */

    public ReportResult<TypePersonReasonEntity> reportTypePersonReasonSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(TypePersonReasonEntity.class, reportCondition);
        List<TypePersonReasonEntity> result = this.baseDaoInterface.reportByOrConditions(TypePersonReasonEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<TypePersonReasonEntity> reportTypePersonReasonAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(TypePersonReasonEntity.class, reportCondition);
        List<TypePersonReasonEntity> result = this.baseDaoInterface.reportByAndConditions(TypePersonReasonEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    /* ****************************************************************************************************************** */

    public Boolean enable(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typepersonreasonid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("enabled", true);
        return this.baseDaoInterface.updateByAndConditions(TypePersonReasonEntity.class, conditions, parameters);
    }

    public Boolean disable(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typepersonreasonid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("enabled", false);
        return this.baseDaoInterface.updateByAndConditions(TypePersonReasonEntity.class, conditions, parameters);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typepersonreasonid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypePersonReasonEntity.class, conditions, parameters);
    }

}
