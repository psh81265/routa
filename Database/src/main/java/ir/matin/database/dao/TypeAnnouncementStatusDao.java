package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeAnnouncementEntity;
import ir.matin.database.entity.TypeAnnouncementStatusEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeAnnouncementStatusDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeAnnouncementStatusEntity get(Integer id) {
        return this.baseDaoInterface.getById(TypeAnnouncementStatusEntity.class, id);
    }

    public List<TypeAnnouncementStatusEntity> list() {
        return this.baseDaoInterface.list(TypeAnnouncementStatusEntity.class);
    }

    public Integer add(TypeAnnouncementStatusEntity typeAnnouncementStatusEntity) {
        return this.baseDaoInterface.save(typeAnnouncementStatusEntity);
    }

    public void update(TypeAnnouncementStatusEntity typeAnnouncementStatusEntity) {
        baseDaoInterface.update(typeAnnouncementStatusEntity);
    }

    /* ****************************************************************************************************************** */

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typeannouncementstatusid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypeAnnouncementStatusEntity.class, conditions, parameters);
    }
}
