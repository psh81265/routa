package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeFeedbackEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeFeedbackDao {
    private static final String NAME = "name";
    private static final String TYPE_FEEDBACK_ID = "typeFeedbackId";
    private static final String DELETED = "deleted";
    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeFeedbackEntity get(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put(TYPE_FEEDBACK_ID, id);
        conditions.put(DELETED, false);
        return this.baseDaoInterface.getByAndConditions(TypeFeedbackEntity.class, conditions);
    }

    public List<TypeFeedbackEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put(DELETED, false);
        return this.baseDaoInterface.listByAndConditions(TypeFeedbackEntity.class, map);
    }

    public Integer add(TypeFeedbackEntity typeFeedbackEntity) {
        return this.baseDaoInterface.save(typeFeedbackEntity);
    }

    public void update(TypeFeedbackEntity typeFeedbackEntity) {
        baseDaoInterface.update(typeFeedbackEntity);
    }

    /* ****************************************************************************************************************** */

    public TypeFeedbackEntity getTypeFeedbackByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put(NAME, name);
        return baseDaoInterface.getByAndConditions(TypeFeedbackEntity.class, map);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put(TYPE_FEEDBACK_ID, id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(DELETED, true);
        return this.baseDaoInterface.updateByAndConditions(TypeFeedbackEntity.class, conditions, parameters);
    }
}
