package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.BookmarkEntity;
import ir.matin.database.entity.SpecialBookmarkEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class SpecialBookmarkDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public SpecialBookmarkEntity get(Integer id) {
        return this.baseDaoInterface.getById(SpecialBookmarkEntity.class, id);
    }

    public List<SpecialBookmarkEntity> list() {
        return this.baseDaoInterface.list(SpecialBookmarkEntity.class);
    }

    public Integer add(SpecialBookmarkEntity specialBookmarkEntity) {
        return this.baseDaoInterface.save(specialBookmarkEntity);
    }

    public void update(SpecialBookmarkEntity specialBookmarkEntity) {
        baseDaoInterface.update(specialBookmarkEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(SpecialBookmarkEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public List<SpecialBookmarkEntity> getSpecialBookmarkByCustomerID(Integer customerID) {
        Map<String, Object> map = new HashMap<>();
        map.put("customerid", customerID);
        return baseDaoInterface.listByAndConditions(SpecialBookmarkEntity.class, map);
    }

    public List<SpecialBookmarkEntity> getSpecialBookmarkByLatAndLon(Double lat, Double lon) {
        Map<String, Object> map = new HashMap<>();
        map.put("latitude", lat);
        map.put("longitude", lon);
        return baseDaoInterface.listByAndConditions(SpecialBookmarkEntity.class, map);
    }

    public List<SpecialBookmarkEntity> getSpecialBookmarkByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return baseDaoInterface.listByAndConditions(SpecialBookmarkEntity.class, map);
    }

    public List<SpecialBookmarkEntity> getSpecialBookmarkByCustomerIDAndName(Integer customerID, String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("customerid", customerID);
        map.put("name", name);
        return baseDaoInterface.listByAndConditions(SpecialBookmarkEntity.class, map);
    }

    public List<SpecialBookmarkEntity> getByCustomerIDAndType(int customerID, int typeBookmarkID) {
        Map<String, Object> map = new HashMap<>();
        map.put("typebookmarkid", typeBookmarkID);
        map.put("customerid", customerID);
        return baseDaoInterface.listByAndConditions(SpecialBookmarkEntity.class, map);
    }



}
