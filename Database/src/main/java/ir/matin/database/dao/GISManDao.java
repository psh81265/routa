package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.CustomerEntity;
import ir.matin.database.entity.GISManEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class GISManDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public GISManEntity get(Integer id) {
        return this.baseDaoInterface.getById(GISManEntity.class, id);
    }

    public List<GISManEntity> list() {
        return this.baseDaoInterface.list(GISManEntity.class);
    }

    public Integer add(GISManEntity gisManEntity) {
        return this.baseDaoInterface.save(gisManEntity);
    }

    public void update(GISManEntity gisManEntity) {
        baseDaoInterface.update(gisManEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(GISManEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public ReportResult<GISManEntity> reportGISManSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(GISManEntity.class, reportCondition);
        List<GISManEntity> result = this.baseDaoInterface.reportByOrConditions(GISManEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<GISManEntity> reportGISManAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(GISManEntity.class, reportCondition);
        List<GISManEntity> result = this.baseDaoInterface.reportByAndConditions(GISManEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

}
