package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.DeviceFeedbackEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class DeviceFeedbackDao {

    private static final String DEVICE_ID = "deviceID";
    private static final String FEEDBACK_DATE = "feedbackDate";
    private static final String TYPE_FEEDBACK_STATUS_ID = "typeFeedbackStatusId";
    private static final String TYPE_FEEDBACK_ID = "typeFeedbackId";

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public DeviceFeedbackEntity get(Integer id) {
        return this.baseDaoInterface.getById(DeviceFeedbackEntity.class, id);
    }

    public List<DeviceFeedbackEntity> list() {
        return this.baseDaoInterface.list(DeviceFeedbackEntity.class);
    }

    public Integer add(DeviceFeedbackEntity feedbackEntity) {
        return this.baseDaoInterface.save(feedbackEntity);
    }

    public void update(DeviceFeedbackEntity feedbackEntity) {
        baseDaoInterface.update(feedbackEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(DeviceFeedbackEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public List<DeviceFeedbackEntity> getFeedbackByDeviceID(Integer deviceID) {
        Map<String, Object> map = new HashMap<>();
        map.put(DEVICE_ID, deviceID);
        return baseDaoInterface.listByAndConditions(DeviceFeedbackEntity.class, map);
    }

    public List<DeviceFeedbackEntity> getFeedbackByFeedbackTypeID(Integer typeFeedbackId) {
        Map<String, Object> map = new HashMap<>();
        map.put(TYPE_FEEDBACK_ID, typeFeedbackId);
        return baseDaoInterface.listByAndConditions(DeviceFeedbackEntity.class, map);
    }

    public List<DeviceFeedbackEntity> getFeedbackByDate(Timestamp date) {
        Map<String, Object> map = new HashMap<>();
        map.put(FEEDBACK_DATE, date);
        return baseDaoInterface.listByAndConditions(DeviceFeedbackEntity.class, map);
    }

    public List<DeviceFeedbackEntity> getDeviceFeedbackByType(Integer deviceID, Integer typeFeedbackStatusID) {
        Map<String, Object> map = new HashMap<>();
        map.put(TYPE_FEEDBACK_STATUS_ID, typeFeedbackStatusID);
        map.put(DEVICE_ID, deviceID);
        return baseDaoInterface.listByAndConditions(DeviceFeedbackEntity.class, map);
    }



}
