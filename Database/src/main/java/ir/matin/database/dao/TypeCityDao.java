package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.TypeCityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeCityDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeCityEntity get(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("typecityid", id);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(TypeCityEntity.class, map);
    }

    public List<TypeCityEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypeCityEntity.class, map);
    }

    public Integer add(TypeCityEntity typeCityEntity) {
        return this.baseDaoInterface.save(typeCityEntity);
    }

    public void update(TypeCityEntity typeCityEntity) {
        this.baseDaoInterface.update(typeCityEntity);
    }

    /* ****************************************************************************************************************** */

    public Boolean enable(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typecityid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("enabled", true);
        return this.baseDaoInterface.updateByAndConditions(TypeCityEntity.class, conditions, parameters);
    }

    public Boolean disable(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typecityid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("enabled", false);
        return this.baseDaoInterface.updateByAndConditions(TypeCityEntity.class, conditions, parameters);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typecityid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypeCityEntity.class, conditions, parameters);
    }

    /* ****************************************************************************************************************** */

    public List<TypeCityEntity> listEnabled() {
        Map<String, Object> map = new HashMap<>();
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypeCityEntity.class, map);
    }

    public List<TypeCityEntity> listEnabled(Integer provinceId) {
        Map<String, Object> map = new HashMap<>();
        map.put("typeprovinceid", provinceId);
        return this.baseDaoInterface.listByAndConditions(TypeCityEntity.class, map);
    }

    public List<TypeCityEntity> listLikeName(String name) {
        String hql = "select city from TypeCityEntity city where city.name like :name and city.enabled = 1 and city.deleted = 0 order by city.name asc";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", "%" + name + "%");
        return baseDaoInterface.queryHql(hql, parameters);
    }

    public TypeCityEntity getCityByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(TypeCityEntity.class, map);
    }

    /* ****************************************************************************************************************** */

    public ReportResult<TypeCityEntity> reportTypeCitySimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(TypeCityEntity.class, reportCondition);
        List<TypeCityEntity> result = this.baseDaoInterface.reportByOrConditions(TypeCityEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<TypeCityEntity> reportTypeCityAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(TypeCityEntity.class, reportCondition);
        List<TypeCityEntity> result = this.baseDaoInterface.reportByAndConditions(TypeCityEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }
}
