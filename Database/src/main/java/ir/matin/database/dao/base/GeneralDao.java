package ir.matin.database.dao.base;

import com.google.common.collect.Iterables;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.utility.data.object.SortOption;
import ir.matin.utility.data.type.SortType;
import org.hibernate.IdentifierLoadAccess;
import org.hibernate.MultiIdentifierLoadAccess;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.*;

@Repository
@Transactional
public class GeneralDao implements BaseDaoInterface {

    /**
     * The Batch Size for Flush a Batch of Inserts and Release Memory
     * 20, Same as the JDBC Batch Size
     */
    private static final int BATCH_SIZE = 20;

    /**
     * The {@link SessionFactory} Instance Representing Database Session Factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    /* ****************************************************************************************************************** */

    @Override
    public <T> Integer count(Class<T> cls) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> rootQuery = criteriaQuery.from(cls);

        criteriaQuery.select(criteriaBuilder.count(rootQuery));
        return session.createQuery(criteriaQuery).getSingleResult().intValue();
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> T getById(Class<T> cls, Integer id) {
        Session session = this.sessionFactory.getCurrentSession();
        IdentifierLoadAccess<T> identifierLoadAccess = session.byId(cls);
        return identifierLoadAccess.load(id);
    }

    @Override
    public <T> T getByAndConditions(Class<T> cls, Map<String, Object> conditions) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        List<T> result = session.createQuery(criteriaQuery).list();
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public <T> T getByOrConditions(Class<T> cls, Map<String, Object> conditions) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[]{})));
        List<T> result = session.createQuery(criteriaQuery).list();
        return !result.isEmpty() ? result.get(0) : null;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> List<T> list(Class<T> cls) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        criteriaQuery.select(rootQuery);
        List<T> result = session.createQuery(criteriaQuery).list();
        return !result.isEmpty() ? result : null;
    }

    @Override
    public <T> List<T> listByIds(Class<T> cls, Set<Integer> ids) {
        Session session = this.sessionFactory.getCurrentSession();
        MultiIdentifierLoadAccess<T> multiIdentifierLoadAccess = session.byMultipleIds(cls);
        List<T> result = multiIdentifierLoadAccess.multiLoad(Iterables.toArray(ids, Integer.class));
        return !result.isEmpty() ? result : null;
    }

    @Override
    public <T> List<T> listByAndConditions(Class<T> cls, Map<String, Object> conditions) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        List<T> result = session.createQuery(criteriaQuery).list();
        return !result.isEmpty() ? result : null;
    }

    @Override
    public <T> List<T> listByInConditions(Class<T> cls, Map<String, List<Object>> conditions) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, List<Object>> pair : conditions.entrySet()) {
            Expression<String> parentExpression = rootQuery.get(pair.getKey());
            predicates.add(parentExpression.in(pair.getValue()));
        }

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        List<T> result = session.createQuery(criteriaQuery).list();
        return !result.isEmpty() ? result : null;
    }

    @Override
    public <T> List<T> queryHql(String hql, Map<String, Object> parameters) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            query.setParameter(pair.getKey(), pair.getValue());
        List<T> result = query.list();
        return !result.isEmpty() ? result : null;
    }

    @Override
    public <T> List<T> querySql(String sql) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery(sql);

        List<T> result = query.list();
        return !result.isEmpty() ? result : null;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> Integer countByAndConditions(Class<T> cls, ReportCondition reportCondition) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : reportCondition.getEqualCondition().entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, String> pair : reportCondition.getLikeCondition().entrySet())
            predicates.add(criteriaBuilder.like(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Number> pair : reportCondition.getMinNumberCondition().entrySet())
            predicates.add(criteriaBuilder.ge(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Number> pair : reportCondition.getMaxNumberCondition().entrySet())
            predicates.add(criteriaBuilder.le(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Timestamp> pair : reportCondition.getMinTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Timestamp> pair : reportCondition.getMaxTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(criteriaBuilder.count(rootQuery));
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        return session.createQuery(criteriaQuery).getSingleResult().intValue();
    }

    @Override
    public <T> Integer countByOrConditions(Class<T> cls, ReportCondition reportCondition) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : reportCondition.getEqualCondition().entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, String> pair : reportCondition.getLikeCondition().entrySet())
            predicates.add(criteriaBuilder.like(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Number> pair : reportCondition.getMinNumberCondition().entrySet())
            predicates.add(criteriaBuilder.ge(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Number> pair : reportCondition.getMaxNumberCondition().entrySet())
            predicates.add(criteriaBuilder.le(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Timestamp> pair : reportCondition.getMinTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Timestamp> pair : reportCondition.getMaxTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));

        criteriaQuery.select(criteriaBuilder.count(rootQuery));
        criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[]{})));
        return session.createQuery(criteriaQuery).getSingleResult().intValue();
    }

    @Override
    public <T> List<T> reportByAndConditions(Class<T> cls, ReportCondition reportCondition) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : reportCondition.getEqualCondition().entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, String> pair : reportCondition.getLikeCondition().entrySet())
            predicates.add(criteriaBuilder.like(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Number> pair : reportCondition.getMinNumberCondition().entrySet())
            predicates.add(criteriaBuilder.ge(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Number> pair : reportCondition.getMaxNumberCondition().entrySet())
            predicates.add(criteriaBuilder.le(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Timestamp> pair : reportCondition.getMinTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Timestamp> pair : reportCondition.getMaxTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));

        List<Order> sorts = new ArrayList<>();
        for (SortOption item : reportCondition.getSortOptions()) {
            if (item.getType() == SortType.ASCENDING)
                sorts.add(criteriaBuilder.asc(rootQuery.get(item.getColumn())));
            else
                sorts.add(criteriaBuilder.desc(rootQuery.get(item.getColumn())));
        }

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        criteriaQuery.orderBy(sorts);
        List<T> result;
        if (reportCondition.getExport())
            result = session.createQuery(criteriaQuery).list();
        else
            result = session.createQuery(criteriaQuery)
                    .setFirstResult((reportCondition.getPageNumber() - 1) * reportCondition.getPageSize())
                    .setMaxResults(reportCondition.getPageSize())
                    .list();
        return !result.isEmpty() ? result : null;
    }

    @Override
    public <T> List<T> reportByOrConditions(Class<T> cls, ReportCondition reportCondition) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(cls);
        Root<T> rootQuery = criteriaQuery.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : reportCondition.getEqualCondition().entrySet())
            predicates.add(criteriaBuilder.equal(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, String> pair : reportCondition.getLikeCondition().entrySet())
            predicates.add(criteriaBuilder.like(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Number> pair : reportCondition.getMinNumberCondition().entrySet())
            predicates.add(criteriaBuilder.ge(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Number> pair : reportCondition.getMaxNumberCondition().entrySet())
            predicates.add(criteriaBuilder.le(rootQuery.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Timestamp> pair : reportCondition.getMinTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));
        for (Map.Entry<String, Timestamp> pair : reportCondition.getMaxTimestampCondition().entrySet())
            predicates.add(criteriaBuilder.lessThanOrEqualTo(rootQuery.get(pair.getKey()), pair.getValue()));

        List<Order> sorts = new ArrayList<>();
        for (SortOption item : reportCondition.getSortOptions()) {
            if (item.getType() == SortType.ASCENDING)
                sorts.add(criteriaBuilder.asc(rootQuery.get(item.getColumn())));
            else
                sorts.add(criteriaBuilder.desc(rootQuery.get(item.getColumn())));
        }

        criteriaQuery.select(rootQuery);
        criteriaQuery.where(criteriaBuilder.or(predicates.toArray(new Predicate[]{})));
        criteriaQuery.orderBy(sorts);
        List<T> result;
        if (reportCondition.getExport())
            result = session.createQuery(criteriaQuery).list();
        else
            result = session.createQuery(criteriaQuery)
                    .setFirstResult((reportCondition.getPageNumber() - 1) * reportCondition.getPageSize())
                    .setMaxResults(reportCondition.getPageSize())
                    .list();
        return !result.isEmpty() ? result : null;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> void deleteById(Class<T> cls, Integer id) {
        Session session = this.sessionFactory.getCurrentSession();
        IdentifierLoadAccess<T> identifierLoadAccess = session.byId(cls);
        T entity = identifierLoadAccess.load(id);
        session.delete(entity);
    }

    @Override
    public <T> void deleteByIdList(Class<T> cls, List<Integer> ids, String idName) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaDelete<T> criteriaDelete = criteriaBuilder.createCriteriaDelete(cls);
        Root<T> rootDelete = criteriaDelete.from(cls);

        criteriaDelete.where(rootDelete.get(idName).in(ids));
        session.createQuery(criteriaDelete).executeUpdate();
    }

    @Override
    public <T> Boolean deleteByAndConditions(Class<T> cls, Map<String, Object> conditions) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaDelete<T> criteriaDelete = criteriaBuilder.createCriteriaDelete(cls);
        Root<T> rootDelete = criteriaDelete.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootDelete.get(pair.getKey()), pair.getValue()));

        criteriaDelete.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        int count = session.createQuery(criteriaDelete).executeUpdate();
        return count > 0;
    }

    @Override
    public <T> Boolean updateByAndConditions(Class<T> cls, Map<String, Object> conditions, Map<String, Object> parameters) {
        Session session = this.sessionFactory.getCurrentSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaUpdate<T> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(cls);
        Root<T> rootUpdate = criteriaUpdate.from(cls);

        List<Predicate> predicates = new ArrayList<>();
        for (Map.Entry<String, Object> pair : conditions.entrySet())
            predicates.add(criteriaBuilder.equal(rootUpdate.get(pair.getKey()), pair.getValue()));

        for (Map.Entry<String, Object> pair : parameters.entrySet())
            criteriaUpdate.set(pair.getKey(), pair.getValue());
        criteriaUpdate.where(criteriaBuilder.and(predicates.toArray(new Predicate[]{})));
        int count = session.createQuery(criteriaUpdate).executeUpdate();
        return count > 0;
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> Integer save(T entity) {
        Session session = this.sessionFactory.getCurrentSession();
        return (Integer) session.save(entity);
    }

    @Override
    public <T> void update(T entity) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(entity);
    }

    @Override
    public <T> void delete(T entity) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(entity);
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> T getByLongId(Class<T> cls, Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        IdentifierLoadAccess<T> identifierLoadAccess = session.byId(cls);
        return identifierLoadAccess.load(id);
    }

    @Override
    public <T> Long saveByLongId(T entity) {
        Session session = this.sessionFactory.getCurrentSession();
        return (Long) session.save(entity);
    }

    /* ****************************************************************************************************************** */

    @Override
    public <T> void saveCollection(Collection<T> entities) {
        int i = 0;
        Session session = this.sessionFactory.getCurrentSession();
        for (T entity : entities) {
            session.save(entity);
            if (i % BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            i++;
        }
    }

    @Override
    public <T> void updateCollection(Collection<T> entities) {
        int i = 0;
        Session session = this.sessionFactory.getCurrentSession();
        for (T entity : entities) {
            session.update(entity);
            if (i % BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            i++;
        }
    }

    @Override
    public <T> void saveOrUpdateCollection(Collection<T> entities) {
        int i = 0;
        Session session = this.sessionFactory.getCurrentSession();
        for (T entity : entities) {
            session.saveOrUpdate(entity);
            if (i % BATCH_SIZE == 0) {
                session.flush();
                session.clear();
            }
            i++;
        }
    }

    @Override
    public void flush() {
        Session session = this.sessionFactory.getCurrentSession();
        session.flush();
        session.clear();

    }
}
