package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.BookmarkEntity;
import ir.matin.database.entity.ProblemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class ProblemDao {

    private static final String CUSTOMER_ID = "customerID";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String DATE = "date";
    private static final String TYPE_PROBLEM_ID = "typeProblemID";
    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public ProblemEntity get(Integer id) {
        return this.baseDaoInterface.getById(ProblemEntity.class, id);
    }

    public List<ProblemEntity> list() {
        return this.baseDaoInterface.list(ProblemEntity.class);
    }

    public Integer add(ProblemEntity problemEntity) {
        return this.baseDaoInterface.save(problemEntity);
    }

    public void update(ProblemEntity problemEntity) {
        baseDaoInterface.update(problemEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(ProblemEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public List<ProblemEntity> getProblemByCustomerID(Integer customerID) {
        Map<String, Object> map = new HashMap<>();
        map.put(CUSTOMER_ID, customerID);
        return baseDaoInterface.listByAndConditions(ProblemEntity.class, map);
    }

    public List<ProblemEntity> getProblemByLatAndLon(Double lat, Double lon) {
        Map<String, Object> map = new HashMap<>();
        map.put(LATITUDE, lat);
        map.put(LONGITUDE, lon);
        return baseDaoInterface.listByAndConditions(ProblemEntity.class, map);
    }

    public List<ProblemEntity> getProblemByDate(Timestamp date) {
        Map<String, Object> map = new HashMap<>();
        map.put(DATE, date);
        return baseDaoInterface.listByAndConditions(ProblemEntity.class, map);
    }

    public List<ProblemEntity> getProblemByCustomerIDAndTypeID(Integer customerID, Integer typeID) {
        Map<String, Object> map = new HashMap<>();
        map.put(CUSTOMER_ID, customerID);
        map.put(TYPE_PROBLEM_ID, typeID);
        return baseDaoInterface.listByAndConditions(ProblemEntity.class, map);
    }



}
