package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.HisPersonInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class HisPersonInfoDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public HisPersonInfoEntity get(Integer id) {
        return this.baseDaoInterface.getById(HisPersonInfoEntity.class, id);
    }

    public List<HisPersonInfoEntity> list() {
        return this.baseDaoInterface.list(HisPersonInfoEntity.class);
    }

    public Integer add(HisPersonInfoEntity hisPersonInfoEntity) {
        return this.baseDaoInterface.save(hisPersonInfoEntity);
    }

    public void update(HisPersonInfoEntity hisPersonInfoEntity) {
        baseDaoInterface.update(hisPersonInfoEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(HisPersonInfoEntity.class, id);
    }


    /* ****************************************************************************************************************** */

}
