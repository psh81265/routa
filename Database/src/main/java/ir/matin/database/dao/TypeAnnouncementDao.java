package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeAnnouncementEntity;
import ir.matin.database.entity.TypeCityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeAnnouncementDao {
    private static final String TYPE_ANNOUNCEMENT_ID = "typeannouncementid";
    private static final String DELETED = "DELETED";
    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeAnnouncementEntity get(Integer id) {
        return this.baseDaoInterface.getById(TypeAnnouncementEntity.class, id);
    }

    public List<TypeAnnouncementEntity> list() {
        return this.baseDaoInterface.list(TypeAnnouncementEntity.class);
    }

    public Integer add(TypeAnnouncementEntity typeAnnouncementEntity) {
        return this.baseDaoInterface.save(typeAnnouncementEntity);
    }

    public void update(TypeAnnouncementEntity typeAnnouncementEntity) {
        baseDaoInterface.update(typeAnnouncementEntity);
    }

    /* ****************************************************************************************************************** */

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put(TYPE_ANNOUNCEMENT_ID, id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(DELETED, true);
        return this.baseDaoInterface.updateByAndConditions(TypeAnnouncementEntity.class, conditions, parameters);
    }
}
