package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeBookmarkEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeBookmarkDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeBookmarkEntity get(Integer id) {
        return this.baseDaoInterface.getById(TypeBookmarkEntity.class, id);
    }

    public List<TypeBookmarkEntity> list() {
        return this.baseDaoInterface.list(TypeBookmarkEntity.class);
    }

    public Integer add(TypeBookmarkEntity typeBookmarkEntity) {
        return this.baseDaoInterface.save(typeBookmarkEntity);
    }

    public void update(TypeBookmarkEntity typeBookmarkEntity) {
        baseDaoInterface.update(typeBookmarkEntity);
    }

    /* ****************************************************************************************************************** */

    public TypeBookmarkEntity getTypeBookmarkByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return baseDaoInterface.getByAndConditions(TypeBookmarkEntity.class, map);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typebookmarkid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypeBookmarkEntity.class, conditions, parameters);
    }
}
