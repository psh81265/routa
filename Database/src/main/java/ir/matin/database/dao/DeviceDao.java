package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.DeviceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class DeviceDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public DeviceEntity get(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("deviceid", id);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(DeviceEntity.class, map);
    }

    public List<DeviceEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(DeviceEntity.class, map);
    }

    public void update(DeviceEntity deviceEntity) {
        this.baseDaoInterface.update(deviceEntity);
    }

    public DeviceEntity getBySerialNumber(String deviceserialnumber) {
        Map<String, Object> map = new HashMap<>();
        map.put("deviceserialnumber", deviceserialnumber);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(DeviceEntity.class, map);
    }

    public Boolean login(Integer id, String deviceSerialNumber) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("deviceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", true);
        parameters.put("deviceserialnumber", deviceSerialNumber);
        return this.baseDaoInterface.updateByAndConditions(DeviceEntity.class, conditions, parameters);
    }

    public Boolean login(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("deviceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", true);
        return this.baseDaoInterface.updateByAndConditions(DeviceEntity.class, conditions, parameters);
    }
    public Integer add(DeviceEntity deviceEntity) {
        return this.baseDaoInterface.save(deviceEntity);
    }

    public Boolean logout(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("deviceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", false);
        return this.baseDaoInterface.updateByAndConditions(DeviceEntity.class, conditions, parameters);
    }

    public Boolean logicalDelete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("deviceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(DeviceEntity.class, conditions, parameters);
    }
}
