package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeBookmarkEntity;
import ir.matin.database.entity.TypeProblemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeProblemDao {
    private static final String NAME = "name";
    private static final String TYPE_PROBLEM_ID = "typeProblemID";
    private static final String DELETED = "deleted";
    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeProblemEntity get(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put(TYPE_PROBLEM_ID, id);
        conditions.put(DELETED, false);
        return this.baseDaoInterface.getByAndConditions(TypeProblemEntity.class, conditions);
    }

    public List<TypeProblemEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put(DELETED, false);
        return this.baseDaoInterface.listByAndConditions(TypeProblemEntity.class, map);
    }

    public Integer add(TypeProblemEntity typeProblemEntity) {
        return this.baseDaoInterface.save(typeProblemEntity);
    }

    public void update(TypeProblemEntity typeProblemEntity) {
        baseDaoInterface.update(typeProblemEntity);
    }

    /* ****************************************************************************************************************** */

    public TypeProblemEntity getTypeProblemByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put(NAME, name);
        return baseDaoInterface.getByAndConditions(TypeProblemEntity.class, map);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put(TYPE_PROBLEM_ID, id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(DELETED, true);
        return this.baseDaoInterface.updateByAndConditions(TypeProblemEntity.class, conditions, parameters);
    }
}
