package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeProblemStatusEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeProblemStatusDao {

    private static final String NAME = "name";
    private static final String TYPE_PROBLEM_STATUS_ID = "typeProblemStatusID";
    private static final String DELETED = "deleted";

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeProblemStatusEntity get(Integer id) {
        return this.baseDaoInterface.getById(TypeProblemStatusEntity.class, id);
    }

    public List<TypeProblemStatusEntity> list() {
        return this.baseDaoInterface.list(TypeProblemStatusEntity.class);
    }

    public Integer add(TypeProblemStatusEntity typeProblemStatusEntity) {
        return this.baseDaoInterface.save(typeProblemStatusEntity);
    }

    public void update(TypeProblemStatusEntity typeProblemStatusEntity) {
        baseDaoInterface.update(typeProblemStatusEntity);
    }

    /* ****************************************************************************************************************** */

    public TypeProblemStatusEntity getTypeProblemStatusByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put(NAME, name);
        return baseDaoInterface.getByAndConditions(TypeProblemStatusEntity.class, map);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put(TYPE_PROBLEM_STATUS_ID, id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(DELETED, true);
        return this.baseDaoInterface.updateByAndConditions(TypeProblemStatusEntity.class, conditions, parameters);
    }

}
