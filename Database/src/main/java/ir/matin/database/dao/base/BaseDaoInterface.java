package ir.matin.database.dao.base;

import ir.matin.database.data.object.ReportCondition;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public interface BaseDaoInterface {

    <T> Integer count(Class<T> cls);

    /* ****************************************************************************************************************** */

    <T> T getById(Class<T> cls, Integer id);

    <T> T getByAndConditions(Class<T> cls, Map<String, Object> conditions);

    <T> T getByOrConditions(Class<T> cls, Map<String, Object> conditions);

    /* ****************************************************************************************************************** */

    <T> List<T> list(Class<T> cls);

    <T> List<T> listByIds(Class<T> cls, Set<Integer> ids);

    <T> List<T> listByAndConditions(Class<T> cls, Map<String, Object> conditions);

    <T> List<T> listByInConditions(Class<T> cls, Map<String, List<Object>> conditions);

    <T> List<T> queryHql(String hql, Map<String, Object> parameters);

    <T> List<T> querySql(String sql);

    /* ****************************************************************************************************************** */

    <T> Integer countByAndConditions(Class<T> cls, ReportCondition reportCondition);

    <T> Integer countByOrConditions(Class<T> cls, ReportCondition reportCondition);

    <T> List<T> reportByAndConditions(Class<T> cls, ReportCondition reportCondition);

    <T> List<T> reportByOrConditions(Class<T> cls, ReportCondition reportCondition);

    /* ****************************************************************************************************************** */

    <T> void deleteById(Class<T> cls, Integer id);

    <T> void deleteByIdList(Class<T> cls, List<Integer> ids, String idName);

    <T> Boolean deleteByAndConditions(Class<T> cls, Map<String, Object> conditions);

    <T> Boolean updateByAndConditions(Class<T> cls, Map<String, Object> conditions, Map<String, Object> parameters);

    /* ****************************************************************************************************************** */

    <T> Integer save(T entity);

    <T> void update(T entity);

    <T> void delete(T entity);

    /* ****************************************************************************************************************** */

    <T> T getByLongId(Class<T> cls, Long id);

    <T> Long saveByLongId(T entity);

    /* ****************************************************************************************************************** */

    <T> void saveCollection(Collection<T> entities);

    <T> void updateCollection(Collection<T> entities);

    <T> void saveOrUpdateCollection(Collection<T> entities);

    void flush();
}
