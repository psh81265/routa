package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.FeedbackEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class FeedbackDao {

    private static final String PERSON_ID = "personID";
    private static final String GEO_PLACE_ID = "geoPlaceID";
    private static final String FEEDBACK_DATE = "feedbackDate";
    private static final String TYPE_FEEDBACK_STATUS_ID = "typefeedbackstatusid";

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public FeedbackEntity get(Integer id) {
        return this.baseDaoInterface.getById(FeedbackEntity.class, id);
    }

    public List<FeedbackEntity> list() {
        return this.baseDaoInterface.list(FeedbackEntity.class);
    }

    public Integer add(FeedbackEntity feedbackEntity) {
        return this.baseDaoInterface.save(feedbackEntity);
    }

    public void update(FeedbackEntity feedbackEntity) {
        baseDaoInterface.update(feedbackEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(FeedbackEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public List<FeedbackEntity> getFeedbackByPersonID(Integer personID) {
        Map<String, Object> map = new HashMap<>();
        map.put(PERSON_ID, personID);
        return baseDaoInterface.listByAndConditions(FeedbackEntity.class, map);
    }

    public List<FeedbackEntity> getFeedbackByGeoPlaceID(Integer geoPlaceID) {
        Map<String, Object> map = new HashMap<>();
        map.put(GEO_PLACE_ID, geoPlaceID);
        return baseDaoInterface.listByAndConditions(FeedbackEntity.class, map);
    }

    public List<FeedbackEntity> getFeedbackByDate(Timestamp date) {
        Map<String, Object> map = new HashMap<>();
        map.put(FEEDBACK_DATE, date);
        return baseDaoInterface.listByAndConditions(FeedbackEntity.class, map);
    }

    public List<FeedbackEntity> getPersonFeedbackByType(Integer personID, Integer typeFeedbackStatusID) {
        Map<String, Object> map = new HashMap<>();
        map.put(TYPE_FEEDBACK_STATUS_ID, typeFeedbackStatusID);
        map.put(PERSON_ID, personID);
        return baseDaoInterface.listByAndConditions(FeedbackEntity.class, map);
    }



}
