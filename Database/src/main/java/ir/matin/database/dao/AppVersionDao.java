package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.AppVersionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class AppVersionDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public AppVersionEntity get(Integer id) {
        return this.baseDaoInterface.getById(AppVersionEntity.class, id);
    }

    public List<AppVersionEntity> list() {
        return this.baseDaoInterface.list(AppVersionEntity.class);
    }

    public Integer add(AppVersionEntity appVersionEntity) {
        return this.baseDaoInterface.save(appVersionEntity);
    }

    public void update(AppVersionEntity appVersionEntity) {
        this.baseDaoInterface.update(appVersionEntity);
    }

    /* ****************************************************************************************************************** */

    public ReportResult<AppVersionEntity> reportAppVersionSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(AppVersionEntity.class, reportCondition);
        List<AppVersionEntity> result = this.baseDaoInterface.reportByOrConditions(AppVersionEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<AppVersionEntity> reportAppVersionAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(AppVersionEntity.class, reportCondition);
        List<AppVersionEntity> result = this.baseDaoInterface.reportByAndConditions(AppVersionEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    /* ****************************************************************************************************************** */

    public AppVersionEntity getLatestVersion(Integer appType) {
        String hqlQuery = "select app from AppVersionEntity app where app.release in " +
                "(select max(allApps.release) from AppVersionEntity allApps where allApps.stable = true and allApps.appType = :type)";
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("type", appType);
        List<AppVersionEntity> result = this.baseDaoInterface.queryHql(hqlQuery, parameters);
        return (result != null) ? result.get(0) : null;
    }
}
