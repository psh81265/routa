package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.GuestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class GuestDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public GuestEntity get(Integer id) {
        return this.baseDaoInterface.getById(GuestEntity.class, id);
    }

    public List<GuestEntity> list() {
        return this.baseDaoInterface.list(GuestEntity.class);
    }

    public Integer add(GuestEntity guestEntity) {
        return this.baseDaoInterface.save(guestEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(GuestEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public ReportResult<GuestEntity> reportGuestSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(GuestEntity.class, reportCondition);
        List<GuestEntity> result = this.baseDaoInterface.reportByOrConditions(GuestEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<GuestEntity> reportOperatorAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(GuestEntity.class, reportCondition);
        List<GuestEntity> result = this.baseDaoInterface.reportByAndConditions(GuestEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public GuestEntity getBySerialNumber(Long clientserialnumber) {
        Map<String, Object> map = new HashMap<>();
        map.put("clientserialnumber", clientserialnumber);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(GuestEntity.class, map);
    }

    public Boolean login(Integer id, Long guestSerialNumber) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", true);
        parameters.put("clientserialnumber", guestSerialNumber);
        return this.baseDaoInterface.updateByAndConditions(GuestEntity.class, conditions, parameters);
    }

    public Boolean login(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("guestid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", true);
        return this.baseDaoInterface.updateByAndConditions(GuestEntity.class, conditions, parameters);
    }

}
