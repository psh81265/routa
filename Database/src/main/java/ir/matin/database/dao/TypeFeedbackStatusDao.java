package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.TypeFeedbackStatusEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeFeedbackStatusDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeFeedbackStatusEntity get(Integer id) {
        return this.baseDaoInterface.getById(TypeFeedbackStatusEntity.class, id);
    }

    public List<TypeFeedbackStatusEntity> list() {
        return this.baseDaoInterface.list(TypeFeedbackStatusEntity.class);
    }

    public Integer add(TypeFeedbackStatusEntity typeFeedbackStatusEntity) {
        return this.baseDaoInterface.save(typeFeedbackStatusEntity);
    }

    public void update(TypeFeedbackStatusEntity typeFeedbackStatusEntity) {
        baseDaoInterface.update(typeFeedbackStatusEntity);
    }

    /* ****************************************************************************************************************** */

    public TypeFeedbackStatusEntity getTypeFeedbackStatusByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return baseDaoInterface.getByAndConditions(TypeFeedbackStatusEntity.class, map);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typereviewstatusid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypeFeedbackStatusEntity.class, conditions, parameters);
    }

}
