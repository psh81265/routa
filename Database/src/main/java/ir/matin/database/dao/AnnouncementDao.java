package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.AnnouncementEntity;
import ir.matin.database.entity.BookmarkEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class AnnouncementDao {
    private static final String ANNOUNCEMENT_ID = "announcementID";
    private static final String CUSTOMER_ID = "customerID";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String DATE = "date";
    private static final String TYPE_ANNOUNCEMENT_ID = "typeAnnouncementID";
    private static final String TYPE_ANNOUNCEMENT_STATUS_ID = "typeAnnouncementStatusID";

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public AnnouncementEntity get(Integer id) {
        return this.baseDaoInterface.getById(AnnouncementEntity.class, id);
    }

    public List<AnnouncementEntity> list() {
        return this.baseDaoInterface.list(AnnouncementEntity.class);
    }

    public Integer add(AnnouncementEntity announcementEntity) {
        return this.baseDaoInterface.save(announcementEntity);
    }

    public void update(AnnouncementEntity announcementEntity) {
        baseDaoInterface.update(announcementEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(AnnouncementEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public List<AnnouncementEntity> getAnnouncementByCustomerID(Integer customerID) {
        Map<String, Object> map = new HashMap<>();
        map.put(CUSTOMER_ID, customerID);
        return baseDaoInterface.listByAndConditions(AnnouncementEntity.class, map);
    }

    public List<AnnouncementEntity> getAnnouncementByLatAndLon(Double lat, Double lon) {
        Map<String, Object> map = new HashMap<>();
        map.put(LATITUDE, lat);
        map.put(LONGITUDE, lon);
        return baseDaoInterface.listByAndConditions(AnnouncementEntity.class, map);
    }

    public List<AnnouncementEntity> getAnnouncementByDate(Timestamp date) {
        Map<String, Object> map = new HashMap<>();
        map.put(DATE, date);
        return baseDaoInterface.listByAndConditions(AnnouncementEntity.class, map);
    }

    public List<AnnouncementEntity> getByCustomerID(int customerID) {
        Map<String, Object> map = new HashMap<>();
        map.put(CUSTOMER_ID, customerID);
        return baseDaoInterface.listByAndConditions(AnnouncementEntity.class, map);
    }



}
