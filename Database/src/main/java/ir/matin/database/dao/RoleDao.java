package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class RoleDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public RoleEntity get(Integer id) {
        return this.baseDaoInterface.getById(RoleEntity.class, id);
    }

    public List<RoleEntity> list() {
        return this.baseDaoInterface.list(RoleEntity.class);
    }

    /* ****************************************************************************************************************** */

    public List<RoleEntity> listByCategory(String category) {
        Map<String, Object> map = new HashMap<>();
        map.put("category", category);
        return this.baseDaoInterface.listByAndConditions(RoleEntity.class, map);
    }

    public RoleEntity getByName(String name) {
        Map<String, Object> map = new HashMap<>();
        map.put("name", name);
        return this.baseDaoInterface.getByAndConditions(RoleEntity.class, map);
    }
}
