package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.CustomerEntity;
import ir.matin.database.entity.OperatorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CustomerDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public CustomerEntity get(Integer id) {
        return this.baseDaoInterface.getById(CustomerEntity.class, id);
    }

    public List<CustomerEntity> list() {
        return this.baseDaoInterface.list(CustomerEntity.class);
    }

    public Integer add(CustomerEntity customerEntity) {
        return this.baseDaoInterface.save(customerEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(CustomerEntity.class, id);
    }
    /* ****************************************************************************************************************** */

    public ReportResult<CustomerEntity> reportCustomerSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(CustomerEntity.class, reportCondition);
        List<CustomerEntity> result = this.baseDaoInterface.reportByOrConditions(CustomerEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<CustomerEntity> reportOperatorAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(CustomerEntity.class, reportCondition);
        List<CustomerEntity> result = this.baseDaoInterface.reportByAndConditions(CustomerEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

}
