package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.data.object.ReportCondition;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.TypeProvinceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class TypeProvinceDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public TypeProvinceEntity get(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("typeprovinceid", id);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(TypeProvinceEntity.class, map);
    }

    public List<TypeProvinceEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypeProvinceEntity.class, map);
    }

    public Integer add(TypeProvinceEntity typeProvinceEntity) {
        return this.baseDaoInterface.save(typeProvinceEntity);
    }

    public void update(TypeProvinceEntity typeProvinceEntity) {
        this.baseDaoInterface.update(typeProvinceEntity);
    }

    /* ****************************************************************************************************************** */

    public Boolean enable(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typeprovinceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("enabled", true);
        return this.baseDaoInterface.updateByAndConditions(TypeProvinceEntity.class, conditions, parameters);
    }

    public Boolean disable(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typeprovinceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("enabled", false);
        return this.baseDaoInterface.updateByAndConditions(TypeProvinceEntity.class, conditions, parameters);
    }

    public Boolean delete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("typeprovinceid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(TypeProvinceEntity.class, conditions, parameters);
    }

    /* ****************************************************************************************************************** */

    public List<TypeProvinceEntity> listEnabled() {
        Map<String, Object> map = new HashMap<>();
        map.put("enabled", true);
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(TypeProvinceEntity.class, map);
    }

    /* ****************************************************************************************************************** */

    public ReportResult<TypeProvinceEntity> reportTypeProvinceSimple(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByOrConditions(TypeProvinceEntity.class, reportCondition);
        List<TypeProvinceEntity> result = this.baseDaoInterface.reportByOrConditions(TypeProvinceEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }

    public ReportResult<TypeProvinceEntity> reportTypeProvinceAdvance(ReportCondition reportCondition) {
        Integer countAll = this.baseDaoInterface.countByAndConditions(TypeProvinceEntity.class, reportCondition);
        List<TypeProvinceEntity> result = this.baseDaoInterface.reportByAndConditions(TypeProvinceEntity.class, reportCondition);
        return new ReportResult<>(countAll, result);
    }
}
