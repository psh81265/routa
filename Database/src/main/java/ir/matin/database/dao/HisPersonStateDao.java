package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.HisPersonStateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class HisPersonStateDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public HisPersonStateEntity get(Integer id) {
        return this.baseDaoInterface.getById(HisPersonStateEntity.class, id);
    }

    public List<HisPersonStateEntity> list() {
        return this.baseDaoInterface.list(HisPersonStateEntity.class);
    }

    public Integer add(HisPersonStateEntity hisPersonStateEntity) {
        return this.baseDaoInterface.save(hisPersonStateEntity);
    }

    public void update(HisPersonStateEntity hisPersonStateEntity) {
        baseDaoInterface.update(hisPersonStateEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(HisPersonStateEntity.class, id);
    }


    /* ****************************************************************************************************************** */
}
