package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class PersonDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    public PersonEntity get(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("personid", id);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(PersonEntity.class, map);
    }

    public List<PersonEntity> list() {
        Map<String, Object> map = new HashMap<>();
        map.put("deleted", false);
        return this.baseDaoInterface.listByAndConditions(PersonEntity.class, map);
    }

//    public void delete(Integer id) {
//        this.baseDaoInterface.deleteById(PersonEntity.class, id);
//    }

    public void update(PersonEntity personEntity) {
        this.baseDaoInterface.update(personEntity);
    }

//    public void delete(Integer id) {
//        baseDaoInterface.deleteById(PersonEntity.class, id);
//    }
    /* ****************************************************************************************************************** */

//    public List<PersonEntity> list(PersonType personType) {
//        Session session = this.sessionFactory.getCurrentSession();
//        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
//        CriteriaQuery<PersonEntity> criteriaQuery = criteriaBuilder.createQuery(PersonEntity.class);
//        Root<PersonEntity> rootQuery = criteriaQuery.from(PersonEntity.class);
//
//        criteriaQuery.select(rootQuery)
//                .where(criteriaBuilder.equal(rootQuery.get("persontype"), personType.getValue()));
//        return session.createQuery(criteriaQuery).list();
//    }

    public PersonEntity getByUsername(String username) {
        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(PersonEntity.class, map);
    }

    public PersonEntity getBySerialNumber(String clientserialnumber) {
        Map<String, Object> map = new HashMap<>();
        map.put("clientserialnumber", clientserialnumber);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(PersonEntity.class, map);
    }

    public PersonEntity getCustomerBySerialNumber(String clientserialnumber) {
        Map<String, Object> map = new HashMap<>();
        map.put("clientserialnumber", clientserialnumber);
        map.put("guest" , false);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(PersonEntity.class, map);
    }

    public PersonEntity getGuestBySerialNumber(String clientserialnumber) {
        Map<String, Object> map = new HashMap<>();
        map.put("clientserialnumber", clientserialnumber);
        map.put("guest" , true);
        map.put("deleted", false);
        return this.baseDaoInterface.getByAndConditions(PersonEntity.class, map);
    }

//    public PersonEntity getByTypeMobile(PersonType personType, String mobile) {
//        Session session = this.sessionFactory.getCurrentSession();
//        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
//        CriteriaQuery<PersonEntity> criteriaQuery = criteriaBuilder.createQuery(PersonEntity.class);
//        Root<PersonEntity> rootQuery = criteriaQuery.from(PersonEntity.class);
//
//        criteriaQuery.select(rootQuery)
//                .where(criteriaBuilder.equal(rootQuery.get("persontype"), personType.getValue()),
//                        criteriaBuilder.equal(rootQuery.get("mobile"), mobile));
//        List<PersonEntity> result = session.createQuery(criteriaQuery).list();
//        return (result.size() > 0) ? result.get(0) : null;
//    }

    /**
     * LOGIN User
     *
     * @param id                 An {@link Integer} Instance Representing Person Id
     * @param clientSerialNumber A {@link Long} Instance Representing Serial Number of Client's Application
     * @return A {@link Boolean} Instance Representing Successful LOGIN
     */
    public Boolean login(Integer id, String clientSerialNumber) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", true);
        parameters.put("clientserialnumber", clientSerialNumber);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * LOGIN User
     *
     * @param id                 An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful LOGIN
     */
    public Boolean login(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", true);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * LOGOUT User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful LOGOUT
     */
    public Boolean logout(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("login", false);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    public Boolean logicalDelete(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("deleted", true);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * CONFIRM User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful Confirmation
     */
    public Boolean confirm(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("confirm", true);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * UnConfirm User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful UnConfirmation
     */
    public Boolean unConfirm(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("confirm", false);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * Activate User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful Activation
     */
    public Boolean activate(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("active", true);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * Deactivate User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful Deactivation
     */
    public Boolean deActivate(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("active", false);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * BLOCK User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful Blocking
     */
    public Boolean block(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("block", true);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * Unblock User
     *
     * @param id An {@link Integer} Instance Representing Person Id
     * @return A {@link Boolean} Instance Representing Successful Unblocking
     */
    public Boolean unBlock(Integer id) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("block", false);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * Set Validation Code and Expiration Time of Validation Code
     *
     * @param id                   An {@link Integer} Instance Representing Person Id
     * @param validationCode       An {@link Integer} Instance Representing Validation Code
     * @param validationExpireTime A {@link Timestamp} Instance Representing Expiration Time of Validation Code
     * @return A {@link Boolean} Instance Representing Successful Validation UPDATE
     */
    public Boolean updateValidation(Integer id, Integer validationCode, Timestamp validationExpireTime) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("validationcode", validationCode);
        parameters.put("validationexpiretime", validationExpireTime);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * UPDATE the User's Password
     *
     * @param id       An {@link Integer} Instance Representing Person Id
     * @param password A {@link Long} Instance Representing Hashed Password of Person
     * @return A {@link Boolean} Instance Representing Successful Password UPDATE
     */
    public Boolean updatePassword(Integer id, String password) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("password", password);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    /**
     * UPDATE the User's Password
     *
     * @param id        An {@link Integer} Instance Representing Person Id
     * @param password  A {@link Long} Instance Representing Hashed Password of Person
     * @param stayLogin A {@link String} Instance Representing Status of LOGIN after Password Changing
     * @return A {@link Boolean} Instance Representing Successful Password UPDATE
     */
    public Boolean updatePassword(Integer id, String password, Boolean stayLogin) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("password", password);
        parameters.put("login", stayLogin);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }

    public Boolean updatePicture(Integer id, String pictureUrl) {
        Map<String, Object> conditions = new HashMap<>();
        conditions.put("personid", id);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("picture", pictureUrl);
        return this.baseDaoInterface.updateByAndConditions(PersonEntity.class, conditions, parameters);
    }
}
