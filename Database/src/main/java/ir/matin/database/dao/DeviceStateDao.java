package ir.matin.database.dao;

import ir.matin.database.dao.base.BaseDaoInterface;
import ir.matin.database.entity.DeviceStateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class DeviceStateDao {

    /**
     * The {@link BaseDaoInterface} Instance Representing Interface of Generic Dao Implementation
     */
    @Autowired
    private BaseDaoInterface baseDaoInterface;

    /* ****************************************************************************************************************** */

    public DeviceStateEntity get(Integer id) {
        return this.baseDaoInterface.getById(DeviceStateEntity.class, id);
    }

    public List<DeviceStateEntity> list() {
        return this.baseDaoInterface.list(DeviceStateEntity.class);
    }

    public Integer add(DeviceStateEntity deviceStateEntity) {
        return this.baseDaoInterface.save(deviceStateEntity);
    }

    public void update(DeviceStateEntity deviceStateEntity) {
        baseDaoInterface.update(deviceStateEntity);
    }

    public void delete(Integer id) {
        baseDaoInterface.deleteById(DeviceStateEntity.class, id);
    }


    /* ****************************************************************************************************************** */
}
