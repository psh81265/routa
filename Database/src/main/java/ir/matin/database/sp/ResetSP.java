package ir.matin.database.sp;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import javax.persistence.StoredProcedureQuery;

/**
 * The General Stored Procedure Class,
 * Containing Methods Global CRUD Operation on Oracle Database
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Repository
@Transactional
public class ResetSP {

    /**
     * The {@link SessionFactory} Instance Representing Database Session Factory
     */
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Cleanse Oracle Database
     */
    public void databaseCleanse() {
        Session session = this.sessionFactory.getCurrentSession();
        StoredProcedureQuery storedProcedure = session.createStoredProcedureQuery("DATABASE_CLEANSE");
        try {
            storedProcedure.execute();
        } catch (PersistenceException e) {
            throw new SystemException(SystemError.STORED_PROCEDURE_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    /**
     * Cleanse Oracle Database
     */
    public void databaseCleansePermission() {
        Session session = this.sessionFactory.getCurrentSession();
        StoredProcedureQuery storedProcedure = session.createStoredProcedureQuery("DATABASE_CLEANSE_PERMISSION");
        try {
            storedProcedure.execute();
        } catch (PersistenceException e) {
            throw new SystemException(SystemError.STORED_PROCEDURE_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    /**
     * Insert Default Type Values into Oracle Database
     * This Method Must Call After {@link ResetSP#databaseCleanse()}
     */
    public void databaseDefault() {
        Session session = this.sessionFactory.getCurrentSession();
        StoredProcedureQuery storedProcedure = session.createStoredProcedureQuery("DATABASE_DEFAULT");
        try {
            storedProcedure.execute();
        } catch (PersistenceException e) {
            throw new SystemException(SystemError.STORED_PROCEDURE_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

    /**
     * Insert Sample Values into Oracle Database
     * This Method Must Call After {@link ResetSP#databaseDefault()}
     */
    public void databaseSample() {
        Session session = this.sessionFactory.getCurrentSession();
        StoredProcedureQuery storedProcedure = session.createStoredProcedureQuery("DATABASE_SAMPLE");
        try {
            storedProcedure.execute();
        } catch (PersistenceException e) {
            throw new SystemException(SystemError.STORED_PROCEDURE_FAILED, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
    }

}
