package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "ROLE", schema = "ROUTAUSER")
public class RoleEntity {
    private int roleID;
    private String category;
    private String name;
    private String title;
    private Boolean manageview;

    @Id
    @Column(name = "ROLE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Role_Sequence")
    @SequenceGenerator(name = "Role_Sequence", sequenceName = "ROLE_SEQ", allocationSize = 1)
    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    @Basic
    @Column(name = "CATEGORY")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "MANAGE_VIEW")
    public Boolean getManageview() {
        return manageview;
    }

    public void setManageview(Boolean manageview) {
        this.manageview = manageview;
    }
}
