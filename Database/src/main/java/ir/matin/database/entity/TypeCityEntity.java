package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_CITY", schema = "ROUTAUSER")
public class TypeCityEntity {
    private int typeCityID;
    private Integer typeProvinceID;
    private String name;
    private Double latitude;
    private Double longitude;
    private Boolean enabled;
    private Boolean deleted;

    private TypeProvinceEntity typeProvinceEntity;

    @Id
    @Column(name = "TYPE_CITY_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeCity_Sequence")
    @SequenceGenerator(name = "TypeCity_Sequence", sequenceName = "TYPE_CITY_SEQ", allocationSize = 1)
    public int getTypeCityID() {
        return typeCityID;
    }

    public void setTypeCityID(int typeCityID) {
        this.typeCityID = typeCityID;
    }

    @Basic
    @Column(name = "TYPE_PROVINCE_ID")
    public Integer getTypeProvinceID() {
        return typeProvinceID;
    }

    public void setTypeProvinceID(Integer typeProvinceID) {
        this.typeProvinceID = typeProvinceID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_PROVINCE_ID", referencedColumnName = "TYPE_PROVINCE_ID", insertable = false, updatable = false)
    public TypeProvinceEntity getTypeProvinceEntity() {
        return typeProvinceEntity;
    }

    public void setTypeProvinceEntity(TypeProvinceEntity typeProvinceEntity) {
        this.typeProvinceEntity = typeProvinceEntity;
    }
}
