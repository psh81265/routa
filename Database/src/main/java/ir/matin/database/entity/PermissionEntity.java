package ir.matin.database.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "PERMISSION", schema = "ROUTAUSER")
public class PermissionEntity {
    private int permissionID;
    private String name;
    private String url;

    private Set<RoleEntity> roles = new HashSet<>();

    @Id
    @Column(name = "PERMISSION_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Permission_Sequence")
    @SequenceGenerator(name = "Permission_Sequence", sequenceName = "PERMISSION_SEQ", allocationSize = 1)
    public int getPermissionID() {
        return permissionID;
    }

    public void setPermissionID(int permissionID) {
        this.permissionID = permissionID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "URL")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "ROLE_PERMISSION", schema = "ROUTAUSER", joinColumns = @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "PERMISSION_ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID", nullable = false))
    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }
}
