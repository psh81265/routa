package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "HIS_PERSON_INFO", schema = "ROUTAUSER")
public class HisPersonInfoEntity {
    private int hisPersonInfoID;
    private Integer operatorID;
    private Timestamp time;
    private Integer personID;
    private Integer state;
    private String username;
    private String password;
    private Boolean confirm;
    private Boolean active;
    private Boolean block;
    private Boolean login;
    private Boolean selfRegister;
    private Integer validationCode;
    private Timestamp validationExpireTime;
    private String clientSerialNumber;
    private String clientType;
    private String nationalID;
    private String firstName;
    private String lastName;
    private String mobile;
    private String phone;
    private String email;
    private String father;
    private Integer typeCityID;
    private String address;
    private String zipcode;
    private Timestamp birthday;
    private Integer sex;
    private String picture;
    private Integer totalCredit;
    private Boolean deleted;

    @Id
    @Column(name = "HIS_PERSON_INFO_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HisPersonInfo_Sequence")
    @SequenceGenerator(name = "HisPersonInfo_Sequence", sequenceName = "HIS_PERSON_INFO_SEQ", allocationSize = 1)
    public int getHisPersonInfoID() {
        return hisPersonInfoID;
    }

    public void setHisPersonInfoID(int hisPersonInfoID) {
        this.hisPersonInfoID = hisPersonInfoID;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    @Basic
    @Column(name = "TIME")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    @Basic
    @Column(name = "USERNAME")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "CONFIRM")
    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    @Basic
    @Column(name = "ACTIVE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "BLOCK")
    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    @Basic
    @Column(name = "LOGIN")
    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    @Basic
    @Column(name = "SELF_REGISTER")
    public Boolean getSelfRegister() {
        return selfRegister;
    }

    public void setSelfRegister(Boolean selfRegister) {
        this.selfRegister = selfRegister;
    }

    @Basic
    @Column(name = "VALIDATION_CODE")
    public Integer getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(Integer validationCode) {
        this.validationCode = validationCode;
    }

    @Basic
    @Column(name = "VALIDATION_EXPIRE_TIME")
    public Timestamp getValidationExpireTime() {
        return validationExpireTime;
    }

    public void setValidationExpireTime(Timestamp validationExpireTime) {
        this.validationExpireTime = validationExpireTime;
    }

    @Basic
    @Column(name = "CLIENT_SERIAL_NUMBER")
    public String getClientSerialNumber() {
        return clientSerialNumber;
    }

    public void setClientSerialNumber(String clientSerialNumber) {
        this.clientSerialNumber = clientSerialNumber;
    }

    @Basic
    @Column(name = "CLIENT_TYPE")
    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    @Basic
    @Column(name = "NATIONAL_ID")
    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    @Basic
    @Column(name = "FIRST_NAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "LAST_NAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "MOBILE")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "PHONE")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "FATHER")
    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    @Basic
    @Column(name = "TYPE_CITY_ID")
    public Integer getTypeCityID() {
        return typeCityID;
    }

    public void setTypeCityID(Integer typeCityID) {
        this.typeCityID = typeCityID;
    }

    @Basic
    @Column(name = "ADDRESS")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "ZIPCODE")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "BIRTHDAY")
    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "SEX")
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "PICTURE")
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Basic
    @Column(name = "TOTAL_CREDIT")
    public Integer getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Integer totalCredit) {
        this.totalCredit = totalCredit;
    }

    @Basic
    @Column(name = "STATE")
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
