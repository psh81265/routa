package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "DEVICE", schema = "ROUTAUSER")
public class DeviceEntity {
    private int deviceid;
    private Boolean deleted;
    private String deviceserialnumber;
    private String devicetype;
    private Boolean login;
    private Set<RoleEntity> roles = new HashSet<>();

    @Id
    @Column(name = "DEVICE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Device_Sequence")
    @SequenceGenerator(name = "Device_Sequence", sequenceName = "DEVICE_SEQ", allocationSize = 1)
    public int getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(int deviceid) {
        this.deviceid = deviceid;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "DEVICE_SERIAL_NUMBER")
    public String getDeviceserialnumber() {
        return deviceserialnumber;
    }

    public void setDeviceserialnumber(String deviceserialnumber) {
        this.deviceserialnumber = deviceserialnumber;
    }

    @Basic
    @Column(name = "DEVICE_TYPE")
    public String getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    @Basic
    @Column(name = "LOGIN")
    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "DEVICE_ROLE", schema = "ROUTAUSER", joinColumns = @JoinColumn(name = "DEVICE_ID", referencedColumnName = "DEVICE_ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID", nullable = false))
    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

}
