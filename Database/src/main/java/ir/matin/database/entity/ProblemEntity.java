package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "PROBLEM", schema = "ROUTAUSER")
public class ProblemEntity {
    private int problemID;
    private Integer typeProblemID;
    private Integer customerID;
    private Integer typeProblemStatusID;

    private Double latitude;
    private Double longitude;
    private Timestamp date;

    private TypeProblemEntity typeProblemEntity;
    private CustomerEntity customerEntity;
    private TypeProblemStatusEntity typeProblemStatusEntity;

    @Id
    @Column(name = "PROBLEM_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Problem_Sequence")
    @SequenceGenerator(name = "Problem_Sequence", sequenceName = "PROBLEM_SEQ", allocationSize = 1)
    public int getProblemID() {
        return problemID;
    }

    public void setProblemID(int problemID) {
        this.problemID = problemID;
    }

    @Basic
    @Column(name = "TYPE_PROBLEM_ID")
    public Integer getTypeProblemID() {
        return typeProblemID;
    }

    public void setTypeProblemID(Integer typeProblemID) {
        this.typeProblemID = typeProblemID;
    }

    @Basic
    @Column(name = "TYPE_PROBLEM_STATUS_ID")
    public Integer getTypeProblemStatusID() {
        return typeProblemStatusID;
    }

    public void setTypeProblemStatusID(Integer typeProblemStatusID) {
        this.typeProblemStatusID = typeProblemStatusID;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "CUSTOMER_ID")
    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    @Basic
    @Column(name = "PROBLEM_DATE")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID", insertable = false, updatable = false)
    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_PROBLEM_ID", referencedColumnName = "TYPE_PROBLEM_ID", insertable = false, updatable = false)
    public TypeProblemEntity getTypeProblemEntity() {
        return typeProblemEntity;
    }

    public void setTypeProblemEntity(TypeProblemEntity typeProblemEntity) {
        this.typeProblemEntity = typeProblemEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_PROBLEM_STATUS_ID", referencedColumnName = "TYPE_PROBLEM_STATUS_ID", insertable = false, updatable = false)
    public TypeProblemStatusEntity getTypeProblemStatusEntity() {
        return typeProblemStatusEntity;
    }

    public void setTypeProblemStatusEntity(TypeProblemStatusEntity typeProblemStatusEntity) {
        this.typeProblemStatusEntity = typeProblemStatusEntity;
    }


}
