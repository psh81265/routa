package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "DEVICE_STATE", schema = "ROUTAUSER")
public class DeviceStateEntity {
    private int deviceStateID;
    private Timestamp time;
    private Integer deviceID;
    private Integer state;

    private TypePersonReasonEntity typePersonReasonEntity;

    @Id
    @Column(name = "DEVICE_STATE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DeviceState_Sequence")
    @SequenceGenerator(name = "DeviceState_Sequence", sequenceName = "DEVICE_STATE_SEQ", allocationSize = 1)
    public int getDeviceStateID() {
        return deviceStateID;
    }

    public void setDeviceStateID(int deviceStateID) {
        this.deviceStateID = deviceStateID;
    }

    @Basic
    @Column(name = "TIME")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "DEVICE_ID")
    public Integer getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Integer deviceid) {
        this.deviceID = deviceid;
    }

    @Basic
    @Column(name = "STATE")
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
