package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_SCORE", schema = "ROUTAUSER")
public class TypeScoreEntity {
    private int typeScoreID;
    private String name;
    private Integer score;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_SCORE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeScore_Sequence")
    @SequenceGenerator(name = "TypeScore_Sequence", sequenceName = "TYPE_SCORE_SEQ", allocationSize = 1)
    public int getTypeScoreID() {
        return typeScoreID;
    }

    public void setTypeScoreID(int typeScoreID) {
        this.typeScoreID = typeScoreID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "SCORE")
    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
}
