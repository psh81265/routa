package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_PROVINCE", schema = "ROUTAUSER")
public class TypeProvinceEntity {
    private int typeProvinceID;
    private String name;
    private Double latitude;
    private Double longitude;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_PROVINCE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeProvince_Sequence")
    @SequenceGenerator(name = "TypeProvince_Sequence", sequenceName = "TYPE_PROVINCE_SEQ", allocationSize = 1)
    public int getTypeProvinceID() {
        return typeProvinceID;
    }

    public void setTypeProvinceID(int typeProvinceID) {
        this.typeProvinceID = typeProvinceID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
