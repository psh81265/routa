package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "ANNOUNCEMENT", schema = "ROUTAUSER")
public class AnnouncementEntity {
    private int announcementID;
    private Integer typeAnnouncementID;
    private Integer customerID;
    private Integer typeAnnouncementStatusID;

    private Double latitude;
    private Double longitude;

    private Timestamp date;

    private TypeAnnouncementEntity typeAnnouncementEntity;
    private CustomerEntity customerEntity;
    private TypeAnnouncementStatusEntity typeAnnouncementStatusEntity;

    @Id
    @Column(name = "ANNOUNCEMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Announcement_Sequence")
    @SequenceGenerator(name = "Announcement_Sequence", sequenceName = "ANNOUNCEMENT_SEQ", allocationSize = 1)
    public int getAnnouncementID() {
        return announcementID;
    }

    public void setAnnouncementID(int announcementID) {
        this.announcementID = announcementID;
    }

    @Basic
    @Column(name = "TYPE_ANNOUNCEMENT_ID")
    public int getTypeAnnouncementID() {
        return typeAnnouncementID;
    }

    public void setTypeAnnouncementID(int typeAnnouncementID) {
        this.typeAnnouncementID = typeAnnouncementID;
    }

    @Basic
    @Column(name = "TYPE_ANNOUNCEMENT_STATUS_ID")
    public int getTypeAnnouncementStatusID() {
        return typeAnnouncementStatusID;
    }

    public void setTypeAnnouncementStatusID(int typeAnnouncementStatusID) {
        this.typeAnnouncementStatusID = typeAnnouncementStatusID;
    }

    @Basic
    @Column(name = "CUSTOMER_ID")
    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    @Basic
    @Column(name = "ANNOUNCEMENT_DATE")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_ANNOUNCEMENT_ID", referencedColumnName = "TYPE_ANNOUNCEMENT_ID", insertable = false, updatable = false)
    public TypeAnnouncementEntity getTypeAnnouncementEntity() {
        return typeAnnouncementEntity;
    }

    public void setTypeAnnouncementEntity(TypeAnnouncementEntity typeAnnouncementEntity) {
        this.typeAnnouncementEntity = typeAnnouncementEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID", insertable = false, updatable = false)
    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_ANNOUNCEMENT_STATUS_ID", referencedColumnName = "TYPE_ANNOUNCEMENT_STATUS_ID", insertable = false, updatable = false)
    public TypeAnnouncementStatusEntity getTypeAnnouncementStatusEntity() {
        return typeAnnouncementStatusEntity;
    }

    public void setTypeAnnouncementStatusEntity(TypeAnnouncementStatusEntity typeAnnouncementStatusEntity) {
        this.typeAnnouncementStatusEntity = typeAnnouncementStatusEntity;
    }
}
