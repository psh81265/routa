package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_FEEDBACK", schema = "ROUTAUSER")
public class TypeFeedbackEntity {
    private int typeFeedbackId;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_FEEDBACK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeFeedback_Sequence")
    @SequenceGenerator(name = "TypeFeedback_Sequence", sequenceName = "TYPE_FEEDBACK_SEQ", allocationSize = 1)
    public int getTypeFeedbackId() {
        return typeFeedbackId;
    }

    public void setTypeFeedbackId(int typeFeedbackId) {
        this.typeFeedbackId = typeFeedbackId;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
}
