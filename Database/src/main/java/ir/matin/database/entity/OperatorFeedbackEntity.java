package ir.matin.database.entity;

import io.swagger.models.auth.In;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "OPERATOR_FEEDBACK", schema = "ROUTAUSER")
public class OperatorFeedbackEntity {
    private int operatorFeedbackID;
    private Integer feedbackID;
    private Integer operatorID;
    private Integer typeFeedbackStatusID;
    private Timestamp date;

    private TypeFeedbackStatusEntity typeFeedbackStatusEntity;
    private FeedbackEntity feedbackEntity;
    private OperatorEntity operatorEntity;



    @Id
    @Column(name = "OPERATOR_FEEDBACK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OperatorFeedback_Sequence")
    @SequenceGenerator(name = "OperatorFeedback_Sequence", sequenceName = "OPERATOR_FEEDBACK_SEQ", allocationSize = 1)
    public int getOperatorFeedbackID() {
        return operatorFeedbackID;
    }

    public void setOperatorFeedbackID(int operatorFeedbackID) {
        this.operatorFeedbackID = operatorFeedbackID;
    }

    @Basic
    @Column(name = "FEEDBACK_ID")
    public Integer getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(Integer feedbackID) {
        this.feedbackID = feedbackID;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    @Basic
    @Column(name = "TYPE_FEEDBACK_STATUS_ID")
    public Integer getTypeFeedbackStatusID() {
        return typeFeedbackStatusID;
    }

    public void setTypeFeedbackStatusID(Integer typeFeedbackStatusID) {
        this.typeFeedbackStatusID = typeFeedbackStatusID;
    }

    @Basic
    @Column(name = "OPERATOR_FEEDBACK_DATE")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OPERATOR_ID", referencedColumnName = "OPERATOR_ID", insertable = false, updatable = false)
    public OperatorEntity getOperatorEntity() {
        return operatorEntity;
    }

    public void setOperatorEntity(OperatorEntity operatorEntity) {
        this.operatorEntity = operatorEntity;
    }



    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "FEEDBACK_ID", referencedColumnName = "FEEDBACK_ID", insertable = false, updatable = false)
    public FeedbackEntity getFeedbackEntity() {
        return feedbackEntity;
    }

    public void setFeedbackEntity(FeedbackEntity feedbackEntity) {
        this.feedbackEntity = feedbackEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_FEEDBACK_STATUS_ID", referencedColumnName = "TYPE_FEEDBACK_STATUS_ID", insertable = false, updatable = false)
    public TypeFeedbackStatusEntity getTypeFeedbackStatusEntity() {
        return typeFeedbackStatusEntity;
    }

    public void setTypeFeedbackStatusEntity(TypeFeedbackStatusEntity typeFeedbackStatusEntity) {
        this.typeFeedbackStatusEntity = typeFeedbackStatusEntity;
    }
}
