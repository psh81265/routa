package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "OPERATOR", schema = "ROUTAUSER")
@PrimaryKeyJoinColumn(name = "OPERATOR_ID", referencedColumnName = "PERSON_ID")
public class OperatorEntity extends PersonEntity {
    private String speciality;
    private Integer salary;

    @Basic
    @Column(name = "SPECIALITY")
    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    @Basic
    @Column(name = "SALARY")
    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

}
