package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "TRIP_HISTORY", schema = "ROUTAUSER")
public class TripHistoryEntity {
    private Integer tripHistoryID;
    private Integer customerID;
    private Double originLatitude;
    private Double originLongitude;
    private Double destinationLatitude;
    private Double destinationLongitude;
    private String name;
    private Timestamp date;

    private CustomerEntity customerEntity;

    @Id
    @Column(name = "TRIP_HISTORY_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TripHistory_Sequence")
    @SequenceGenerator(name = "TripHistory_Sequence", sequenceName = "TRIP_HISTORY_SEQ", allocationSize = 1)
    public Integer getTripHistoryID() {
        return tripHistoryID;
    }

    public void setTripHistoryID(Integer tripHistoryID) {
        this.tripHistoryID = tripHistoryID;
    }

    @Basic
    @Column(name = "CUSTOMER_ID")
    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    @Basic
    @Column(name = "ORIGIN_LATITUDE")
    public Double getOriginLatitude() {
        return originLatitude;
    }

    public void setOriginLatitude(Double originLatitude) {
        this.originLatitude = originLatitude;
    }

    @Basic
    @Column(name = "ORIGIN_LONGITUDE")
    public Double getOriginLongitude() {
        return originLongitude;
    }

    public void setOriginLongitude(Double originLongitude) {
        this.originLongitude = originLongitude;
    }

    @Basic
    @Column(name = "DESTINATION_LATITUDE")
    public Double getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(Double destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    @Basic
    @Column(name = "DESTINATION_LONGITUDE")
    public Double getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(Double destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "TRIP_HISTORY_DATE")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID", insertable = false, updatable = false)
    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }
}
