package ir.matin.database.entity;

import javax.persistence.*;

@Entity

@Table(name = "TYPE_PERSON_REASON", schema = "ROUTAUSER")
public class TypePersonReasonEntity {
    private int typePersonReasonID;
    private String name;
    private Integer type;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_PERSON_REASON_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypePersonReason_Sequence")
    @SequenceGenerator(name = "TypePersonReason_Sequence", sequenceName = "TYPE_PERSON_REASON_SEQ", allocationSize = 1)
    public int getTypePersonReasonID() {
        return typePersonReasonID;
    }

    public void setTypePersonReasonID(int typePersonReasonID) {
        this.typePersonReasonID = typePersonReasonID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "TYPE")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
