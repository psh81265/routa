package ir.matin.database.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER", schema = "ROUTAUSER")
@PrimaryKeyJoinColumn(name = "CUSTOMER_ID", referencedColumnName = "PERSON_ID")
public class CustomerEntity extends PersonEntity {

}
