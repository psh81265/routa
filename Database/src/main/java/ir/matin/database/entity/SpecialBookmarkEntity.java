package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "SPECIAL_BOOKMARK", schema = "ROUTAUSER")
public class SpecialBookmarkEntity {
    private int specialBookmarkID;
    private Integer customerID;
    private Integer typeBookmarkID;
    private String name;
    private Double latitude;
    private Double longitude;
//    private Boolean deleted;

    private TypeBookmarkEntity typeBookmarkEntity;
    private CustomerEntity customerEntity;

    public SpecialBookmarkEntity(String name, double latitude, double longitude, int typeBookmarkID, int customerID) {
        setName(name);
        setLatitude(latitude);
        setLongitude(longitude);
        setTypeBookmarkID(typeBookmarkID);
        setCustomerID(customerID);
    }

    public SpecialBookmarkEntity() {

    }

    @Id
    @Column(name = "SPECIAL_BOOKMARK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SpecialBookmark_Sequence")
    @SequenceGenerator(name = "SpecialBookmark_Sequence", sequenceName = "SPECIAL_BOOKMARK_SEQ", allocationSize = 1)
    public int getSpecialBookmarkID() {
        return specialBookmarkID;
    }

    public void setSpecialBookmarkID(int specialBookmarkID) {
        this.specialBookmarkID = specialBookmarkID;
    }

    @Basic
    @Column(name = "CUSTOMER_ID")
    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "TYPE_BOOKMARK_ID")
    public Integer getTypeBookmarkID() {
        return typeBookmarkID;
    }

    public void setTypeBookmarkID(Integer typeBookmarkID) {
        this.typeBookmarkID = typeBookmarkID;
    }

//    @Basic
//    @Column(name = "DELETED")
//    public Boolean getDeleted() {
//        return deleted;
//    }
//
//    public void setDeleted(Boolean deleted) {
//        this.deleted = deleted;
//    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "CUSTOMER_ID", insertable = false, updatable = false)
    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_BOOKMARK_ID", referencedColumnName = "TYPE_BOOKMARK_ID", insertable = false, updatable = false)
    public TypeBookmarkEntity getTypeBookmarkEntity() {
        return typeBookmarkEntity;
    }

    public void setTypeBookmarkEntity(TypeBookmarkEntity typeBookmarkEntity) {
        this.typeBookmarkEntity = typeBookmarkEntity;
    }
}
