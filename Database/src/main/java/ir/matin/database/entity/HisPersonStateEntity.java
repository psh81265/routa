package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "HIS_PERSON_STATE", schema = "ROUTAUSER")
public class HisPersonStateEntity {
    private int hisPersonStateID;
    private Integer operatorID;
    private Timestamp time;
    private Integer personID;
    private Integer typePersonReasonID;
    private Integer state;

    private TypePersonReasonEntity typePersonReasonEntity;

    @Id
    @Column(name = "HIS_PERSON_STATE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HisPersonState_Sequence")
    @SequenceGenerator(name = "HisPersonState_Sequence", sequenceName = "HIS_PERSON_STATE_SEQ", allocationSize = 1)
    public int getHisPersonStateID() {
        return hisPersonStateID;
    }

    public void setHisPersonStateID(int hisPersonStateID) {
        this.hisPersonStateID = hisPersonStateID;
    }

    @Basic
    @Column(name = "OPERATOR_ID")
    public Integer getOperatorID() {
        return operatorID;
    }

    public void setOperatorID(Integer operatorID) {
        this.operatorID = operatorID;
    }

    @Basic
    @Column(name = "TIME")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personid) {
        this.personID = personid;
    }

    @Basic
    @Column(name = "TYPE_PERSON_REASON_ID")
    public Integer getTypePersonReasonID() {
        return typePersonReasonID;
    }

    public void setTypePersonReasonID(Integer typepersonreasonid) {
        this.typePersonReasonID = typepersonreasonid;
    }

    @Basic
    @Column(name = "STATE")
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_PERSON_REASON_ID", referencedColumnName = "TYPE_PERSON_REASON_ID", insertable = false, updatable = false)
    public TypePersonReasonEntity getTypePersonReasonEntity() {
        return typePersonReasonEntity;
    }

    public void setTypePersonReasonEntity(TypePersonReasonEntity typePersonReasonEntity) {
        this.typePersonReasonEntity = typePersonReasonEntity;
    }
}
