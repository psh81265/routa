package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_BOOKMARK", schema = "ROUTAUSER")
public class TypeBookmarkEntity {
    private int typeBookmarkID;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_BOOKMARK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeBookmark_Sequence")
    @SequenceGenerator(name = "TypeBookmark_Sequence", sequenceName = "TYPE_BOOKMARK_SEQ", allocationSize = 1)
    public int getTypeBookmarkID() {
        return typeBookmarkID;
    }

    public void setTypeBookmarkID(int typeBookmarkID) {
        this.typeBookmarkID = typeBookmarkID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
}
