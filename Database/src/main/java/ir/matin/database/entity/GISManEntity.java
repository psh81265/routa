package ir.matin.database.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "GISMAN", schema = "ROUTAUSER")
@PrimaryKeyJoinColumn(name = "GISMAN_ID", referencedColumnName = "PERSON_ID")
public class GISManEntity extends PersonEntity {

}
