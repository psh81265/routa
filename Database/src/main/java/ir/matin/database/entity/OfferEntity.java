package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "OFFER", schema = "ROUTAUSER")
public class OfferEntity {
    private int offerID;
    private Integer geoPlaceID;
    private String name;
    private Timestamp expireDate;

    private GeoPlaceEntity geoPlaceEntity;

    @Id
    @Column(name = "OFFER_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Offer_Sequence")
    @SequenceGenerator(name = "Offer_Sequence", sequenceName = "OFFER_SEQ", allocationSize = 1)
    public int getOfferID() {
        return offerID;
    }

    public void setOfferID(int offerID) {
        this.offerID = offerID;
    }

    @Basic
    @Column(name = "GEO_PLACE_ID")
    public Integer getGeoPlaceID() {
        return geoPlaceID;
    }

    public void setGeoPlaceID(Integer geoPlaceID) {
        this.geoPlaceID = geoPlaceID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "EXPIRE_DATE")
    public Timestamp getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Timestamp expireDate) {
        this.expireDate = expireDate;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GEO_PLACE_ID", referencedColumnName = "GEO_PLACE_ID", insertable = false, updatable = false)
    public GeoPlaceEntity getGeoPlaceEntity() {
        return geoPlaceEntity;
    }

    public void setGeoPlaceEntity(GeoPlaceEntity geoPlaceEntity) {
        this.geoPlaceEntity = geoPlaceEntity;
    }
}
