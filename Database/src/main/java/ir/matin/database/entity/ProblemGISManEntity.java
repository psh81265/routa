package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "PROBLEM_GISMAN", schema = "ROUTAUSER")
public class ProblemGISManEntity {
    private int problemGISManID;
    private Integer problemID;
    private Integer gisManID;
    private Integer typeProblemStatusID;
    private Integer date;

    private TypeProblemStatusEntity typeProblemStatusEntity;
    private ProblemEntity problemEntity;
    private GISManEntity gisManEntity;

    @Id
    @Column(name = "PROBLEM_GISMAN_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProblemGisMan_Sequence")
    @SequenceGenerator(name = "ProblemGisMan_Sequence", sequenceName = "PROBLEM_GIS_MAN_SEQ", allocationSize = 1)
    public int getProblemGISManID() {
        return problemGISManID;
    }

    public void setProblemGISManID(int problemGISManID) {
        this.problemGISManID = problemGISManID;
    }

    @Basic
    @Column(name = "PROBLEM_ID")
    public Integer getProblemID() {
        return problemID;
    }

    public void setProblemID(Integer problemID) {
        this.problemID = problemID;
    }

    @Basic
    @Column(name = "GISMAN_ID")
    public Integer getGISManID() {
        return gisManID;
    }

    public void setGISManID(Integer gisManID) {
        this.gisManID = gisManID;
    }

    @Basic
    @Column(name = "TYPE_PROBLEM_STATUS_ID")
    public Integer getTypeProblemStatusID() {
        return typeProblemStatusID;
    }

    public void setTypeProblemStatusID(Integer typeProblemStatusID) {
        this.typeProblemStatusID = typeProblemStatusID;
    }

    @Basic
    @Column(name = "PROBLEM_GISMAN_DATE")
    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PROBLEM_ID", referencedColumnName = "PROBLEM_ID", insertable = false, updatable = false)
    public ProblemEntity getProblemEntity() {
        return problemEntity;
    }

    public void setProblemEntity(ProblemEntity problemEntity) {
        this.problemEntity = problemEntity;
    }



    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GISMAN_ID", referencedColumnName = "GISMAN_ID", insertable = false, updatable = false)
    public GISManEntity getGisManEntity() {
        return gisManEntity;
    }

    public void setGisManEntity(GISManEntity gisManEntity) {
        this.gisManEntity = gisManEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_PROBLEM_STATUS_ID", referencedColumnName = "TYPE_PROBLEM_STATUS_ID", insertable = false, updatable = false)
    public TypeProblemStatusEntity getTypeProblemStatusEntity() {
        return typeProblemStatusEntity;
    }

    public void setTypeProblemStatusEntity(TypeProblemStatusEntity typeProblemStatusEntity) {
        this.typeProblemStatusEntity = typeProblemStatusEntity;
    }
}
