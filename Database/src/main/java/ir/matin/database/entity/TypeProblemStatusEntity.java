package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_PROBLEM_STATUS", schema = "ROUTAUSER")
public class TypeProblemStatusEntity {
    private int typeProblemStatusID;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_PROBLEM_STATUS_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeProblemStatus_Sequence")
    @SequenceGenerator(name = "TypeProblemStatus_Sequence", sequenceName = "TYPE_PROBLEM_STATUS_SEQ", allocationSize = 1)
    public int getTypeProblemStatusID() {
        return typeProblemStatusID;
    }

    public void setTypeProblemStatusID(int typeProblemStatusID) {
        this.typeProblemStatusID = typeProblemStatusID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
}
