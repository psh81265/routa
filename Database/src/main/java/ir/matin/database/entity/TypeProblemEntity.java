package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_PROBLEM", schema = "ROUTAUSER")
public class TypeProblemEntity {
    private int typeProblemID;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_PROBLEM_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Type_Problem_Sequence")
    @SequenceGenerator(name = "TypeProblem_Sequence", sequenceName = "TYPE_PROBLEM_SEQ", allocationSize = 1)
    public int getTypeProblemID() {
        return typeProblemID;
    }

    public void setTypeProblemID(int typeProblemID) {
        this.typeProblemID = typeProblemID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
