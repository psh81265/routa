package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_ANNOUNCEMENT", schema = "ROUTAUSER")
public class TypeAnnouncementEntity {
    private int typeAnnouncementID;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_ANNOUNCEMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeAnnouncement_Sequence")
    @SequenceGenerator(name = "TypeAnnouncement_Sequence", sequenceName = "TYPE_ANNOUNCEMENT_SEQ", allocationSize = 1)
    public int getTypeAnnouncementID() {
        return typeAnnouncementID;
    }

    public void setTypeAnnouncementID(int typeAnnouncementID) {
        this.typeAnnouncementID = typeAnnouncementID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
