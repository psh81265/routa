package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_ANNOUNCEMENT_STATUS", schema = "ROUTAUSER")
public class TypeAnnouncementStatusEntity {
    private int typeAnnouncementStatusID;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_ANNOUNCEMENT_STATUS_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeAnnouncementStatus_Sequence")
    @SequenceGenerator(name = "TypeAnnouncementStatus_Sequence", sequenceName = "TYPE_ANNOUNCEMENT_STATUS_SEQ", allocationSize = 1)
    public int getTypeAnnouncementStatusID() {
        return typeAnnouncementStatusID;
    }

    public void setTypeAnnouncementStatusID(int typeAnnouncementStatusID) {
        this.typeAnnouncementStatusID = typeAnnouncementStatusID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
