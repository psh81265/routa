package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "DEVICE_INFO", schema = "ROUTAUSER")
public class DeviceInfoEntity {
    private int deviceInfoID;
    private Timestamp time;
    private Integer deviceID;
    private Integer state;
    private Boolean login;
    private Integer validationCode;
    private Timestamp validationExpireTime;
    private String deviceSerialNumber;
    private String deviceType;
    private Boolean deleted;

    @Id
    @Column(name = "DEVICE_INFO_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DeviceInfo_Sequence")
    @SequenceGenerator(name = "DeviceInfo_Sequence", sequenceName = "DEVICE_INFO_SEQ", allocationSize = 1)
    public int getDeviceInfoID() {
        return deviceInfoID;
    }

    public void setDeviceInfoID(int deviceInfoID) {
        this.deviceInfoID = deviceInfoID;
    }

    @Basic
    @Column(name = "TIME")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "DEVICE_ID")
    public Integer getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Integer deviceID) {
        this.deviceID = deviceID;
    }

    @Basic
    @Column(name = "LOGIN")
    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    @Basic
    @Column(name = "VALIDATION_CODE")
    public Integer getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(Integer validationCode) {
        this.validationCode = validationCode;
    }

    @Basic
    @Column(name = "VALIDATION_EXPIRE_TIME")
    public Timestamp getValidationExpireTime() {
        return validationExpireTime;
    }

    public void setValidationExpireTime(Timestamp validationExpireTime) {
        this.validationExpireTime = validationExpireTime;
    }

    @Basic
    @Column(name = "DEVICE_SERIAL_NUMBER")
    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    @Basic
    @Column(name = "DEVICE_TYPE")
    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Basic
    @Column(name = "STATE")
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
