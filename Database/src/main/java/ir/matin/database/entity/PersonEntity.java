package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "PERSON", schema = "ROUTAUSER")
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonEntity {
    private int personid;
    private String username;
    private String password;
    private Boolean confirm;
    private Boolean active;
    private Boolean block;
    private Boolean login;
    private Boolean deleted;
    private Boolean selfregister;
    private Integer validationcode;
    private Timestamp validationexpiretime;
    private String clientserialnumber;
    private String clienttype;
    private String nationalid;
    private String firstname;
    private String lastname;
    private String mobile;
    private String phone;
    private String email;
    private String father;
    private Integer typecityid;
    private String address;
    private String zipcode;
    private Timestamp birthday;
    private Integer sex;
    private String picture;
    private Integer totalcredit;
    private Boolean guest;

    private TypeCityEntity typeCityEntity;
    private Set<RoleEntity> roles = new HashSet<>();

    @Id
    @Column(name = "PERSON_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Person_Sequence")
    @SequenceGenerator(name = "Person_Sequence", sequenceName = "PERSON_SEQ", allocationSize = 1)
    public int getPersonid() {
        return personid;
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    @Basic
    @Column(name = "USERNAME")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "CONFIRM")
    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    @Basic
    @Column(name = "ACTIVE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "BLOCK")
    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    @Basic
    @Column(name = "LOGIN")
    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "SELF_REGISTER")
    public Boolean getSelfregister() {
        return selfregister;
    }

    public void setSelfregister(Boolean selfregister) {
        this.selfregister = selfregister;
    }

    @Basic
    @Column(name = "VALIDATION_CODE")
    public Integer getValidationcode() {
        return validationcode;
    }

    public void setValidationcode(Integer validationcode) {
        this.validationcode = validationcode;
    }

    @Basic
    @Column(name = "VALIDATION_EXPIRE_TIME")
    public Timestamp getValidationexpiretime() {
        return validationexpiretime;
    }

    public void setValidationexpiretime(Timestamp validationexpiretime) {
        this.validationexpiretime = validationexpiretime;
    }

    @Basic
    @Column(name = "CLIENT_SERIAL_NUMBER")
    public String getClientserialnumber() {
        return clientserialnumber;
    }

    public void setClientserialnumber(String clientserialnumber) {
        this.clientserialnumber = clientserialnumber;
    }

    @Basic
    @Column(name = "CLIENT_TYPE")
    public String getClienttype() {
        return clienttype;
    }

    public void setClienttype(String clienttype) {
        this.clienttype = clienttype;
    }

    @Basic
    @Column(name = "NATIONAL_ID")
    public String getNationalid() {
        return nationalid;
    }

    public void setNationalid(String nationalid) {
        this.nationalid = nationalid;
    }

    @Basic
    @Column(name = "FIRSTNAME")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "LASTNAME")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "MOBILE")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "PHONE")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "FATHER")
    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    @Basic
    @Column(name = "TYPE_CITY_ID")
    public Integer getTypecityid() {
        return typecityid;
    }

    public void setTypecityid(Integer typecityid) {
        this.typecityid = typecityid;
    }

    @Basic
    @Column(name = "ADDRESS")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "ZIPCODE")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "BIRTHDAY")
    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "SEX")
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "PICTURE")
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Basic
    @Column(name = "TOTAL_CREDIT")
    public Integer getTotalcredit() {
        return totalcredit;
    }

    public void setTotalcredit(Integer totalcredit) {
        this.totalcredit = totalcredit;
    }

    @Basic
    @Column(name = "GUEST")
    public Boolean getGuest() {
        return guest;
    }

    public void setGuest(Boolean guest) {
        this.guest = guest;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_CITY_ID", referencedColumnName = "TYPE_CITY_ID", insertable = false, updatable = false)
    public TypeCityEntity getTypeCityEntity() {
        return typeCityEntity;
    }

    public void setTypeCityEntity(TypeCityEntity typeCityEntity) {
        this.typeCityEntity = typeCityEntity;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "PERSON_ROLE", schema = "ROUTAUSER", joinColumns = @JoinColumn(name = "PERSON_ID", referencedColumnName = "PERSON_ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID", nullable = false))
    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

}
