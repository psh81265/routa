package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "APP_VERSION", schema = "ROUTAUSER")
public class AppVersionEntity {
    private int appVersionID;
    private Integer appType;
    private String version;
    private String url;
    private Timestamp release;
    private Boolean stable;
    private Boolean force;

    @Id
    @Column(name = "APP_VERSION_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AppVersion_Sequence")
    @SequenceGenerator(name = "AppVersion_Sequence", sequenceName = "APP_VERSION_SEQ", allocationSize = 1)
    public int getAppVersionID() {
        return appVersionID;
    }

    public void setAppVersionID(int appVersionID) {
        this.appVersionID = appVersionID;
    }



    @Basic
    @Column(name = "APP_TYPE")
    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    @Basic
    @Column(name = "Version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "URL")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "Release")
    public Timestamp getRelease() {
        return release;
    }

    public void setRelease(Timestamp release) {
        this.release = release;
    }

    @Basic
    @Column(name = "Stable")
    public Boolean getStable() {
        return stable;
    }

    public void setStable(Boolean stable) {
        this.stable = stable;
    }

    @Basic
    @Column(name = "Force")
    public Boolean getForce() {
        return force;
    }

    public void setForce(Boolean force) {
        this.force = force;
    }
}
