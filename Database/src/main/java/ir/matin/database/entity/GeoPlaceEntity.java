package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "GEO_PLACE", schema = "ROUTAUSER")
public class GeoPlaceEntity {
    private int geoPlaceID;
    private Integer averageRate;

    @Id
    @Column(name = "GEO_PLACE_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GeoPlace_Sequence")
    @SequenceGenerator(name = "GeoPlace_Sequence", sequenceName = "GEO_PLACE_SEQ", allocationSize = 1)
    public int getGeoPlaceID() {
        return geoPlaceID;
    }

    public void setGeoPlaceID(int geoPlaceID) {
        this.geoPlaceID = geoPlaceID;
    }

    @Basic
    @Column(name = "AVERAGE_RATE")
    public Integer getAverageRate() {
        return averageRate;
    }

    public void setAverageRate(Integer averageRate) {
        this.averageRate = averageRate;
    }



}
