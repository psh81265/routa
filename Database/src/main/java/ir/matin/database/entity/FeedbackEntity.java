package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "FEEDBACK", schema = "ROUTAUSER")
public class FeedbackEntity {
    private int feedbackID;
    private Integer personID;
    private Integer typeFeedbackStatusId;
    private Integer typeFeedbackId;
    private String comment;
    private Integer rate;
    private Timestamp date;
    private Double latitude;
    private Double longitude;
    private PersonEntity personEntity;
    private TypeFeedbackStatusEntity typeFeedbackStatusEntity;
    private TypeFeedbackEntity typeFeedbackEntity;

    @Id
    @Column(name = "FEEDBACK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Feedback_Sequence")
    @SequenceGenerator(name = "Feedback_Sequence", sequenceName = "FEEDBACK_SEQ", allocationSize = 1)
    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    @Basic
    @Column(name = "PERSON_ID")
    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    @Basic
    @Column(name = "TYPE_FEEDBACK_STATUS_ID")
    public Integer getTypeFeedbackStatusId() {
        return typeFeedbackStatusId;
    }

    public void setTypeFeedbackStatusId(Integer typeFeedbackStatusId) {
        this.typeFeedbackStatusId = typeFeedbackStatusId;
    }

    @Basic
    @Column(name = "TYPE_FEEDBACK_ID")
    public Integer getTypeFeedbackId() {
        return typeFeedbackId;
    }

    public void setTypeFeedbackId(Integer typeFeedbackId) {
        this.typeFeedbackId = typeFeedbackId;
    }

    @Basic
    @Column(name = "FEEDBACK_COMMENT")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "RATE")
    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    @Basic
    @Column(name = "FEEDBACK_DATE")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PERSON_ID", referencedColumnName = "PERSON_ID", insertable = false, updatable = false)
    public PersonEntity getPersonEntity() {
        return personEntity;
    }

    public void setPersonEntity(PersonEntity personEntity) {
        this.personEntity = personEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_FEEDBACK_STATUS_ID", referencedColumnName = "TYPE_FEEDBACK_STATUS_ID", insertable = false, updatable = false)
    public TypeFeedbackStatusEntity getTypeFeedbackStatusEntity() {
        return typeFeedbackStatusEntity;
    }

    public void setTypeFeedbackStatusEntity(TypeFeedbackStatusEntity typeFeedbackStatusEntity) {
        this.typeFeedbackStatusEntity = typeFeedbackStatusEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_FEEDBACK_ID", referencedColumnName = "TYPE_FEEDBACK_ID", insertable = false, updatable = false)
    public TypeFeedbackEntity getTypeFeedbackEntity() {
        return typeFeedbackEntity;
    }

    public void setTypeFeedbackEntity(TypeFeedbackEntity typeFeedbackEntity) {
        this.typeFeedbackEntity = typeFeedbackEntity;
    }
}
