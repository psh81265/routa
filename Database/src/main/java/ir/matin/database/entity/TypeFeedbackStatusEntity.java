package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "TYPE_FEEDBACK_STATUS", schema = "ROUTAUSER")
public class TypeFeedbackStatusEntity {
    private int typeFeedbackStatusID;
    private String name;
    private Boolean enabled;
    private Boolean deleted;

    @Id
    @Column(name = "TYPE_FEEDBACK_STATUS_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TypeFeedbackStatus_Sequence")
    @SequenceGenerator(name = "TypeFeedbackStatus_Sequence", sequenceName = "TYPE_FEEDBACK_STATUS_SEQ", allocationSize = 1)
    public int getTypeFeedbackStatusID() {
        return typeFeedbackStatusID;
    }

    public void setTypeFeedbackStatusID(int typeFeedbackStatusID) {
        this.typeFeedbackStatusID = typeFeedbackStatusID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "ENABLED")
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "DELETED")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
}
