package ir.matin.database.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "DEVICE_FEEDBACK", schema = "ROUTAUSER")
public class DeviceFeedbackEntity {
    private int deviceFeedbackId;
    private Integer deviceId;
    private Integer typeFeedbackStatusId;
    private String comment;
    private Integer rate;
    private Timestamp date;
    private Double latitude;
    private Double longitude;
    private Integer typeFeedbackId;

    private DeviceEntity deviceEntity;
    private TypeFeedbackEntity typeFeedbackEntity;
    private TypeFeedbackStatusEntity typeFeedbackStatusEntity;

    @Id
    @Column(name = "DEVICE_FEEDBACK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Device_Feedback_Sequence")
    @SequenceGenerator(name = "Device_Feedback_Sequence", sequenceName = "DEVICE_FEEDBACK_SEQ", allocationSize = 1)
    public int getDeviceFeedbackId() {
        return deviceFeedbackId;
    }

    public void setDeviceFeedbackId(int deviceFeedbackId) {
        this.deviceFeedbackId = deviceFeedbackId;
    }

    @Basic
    @Column(name = "DEVICE_ID")
    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "TYPE_FEEDBACK_STATUS_ID")
    public int getTypeFeedbackStatusId() {
        return typeFeedbackStatusId;
    }

    public void setTypeFeedbackStatusId(Integer typeFeedbackStatusId) {
        this.typeFeedbackStatusId = typeFeedbackStatusId;
    }

    @Basic
    @Column(name = "FEEDBACK_COMMENT")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "RATE")
    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    @Basic
    @Column(name = "FEEDBACK_DATE")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "TYPE_FEEDBACK_ID")
    public Integer getTypeFeedbackId() {
        return typeFeedbackId;
    }

    public void setTypeFeedbackId(Integer typeFeedbackId) {
        this.typeFeedbackId = typeFeedbackId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_FEEDBACK_ID", referencedColumnName = "TYPE_FEEDBACK_ID", insertable = false, updatable = false)
    public TypeFeedbackEntity getTypeFeedbackEntity() {
        return typeFeedbackEntity;
    }

    public void setTypeFeedbackEntity(TypeFeedbackEntity typeFeedbackEntity) {
        this.typeFeedbackEntity = typeFeedbackEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DEVICE_ID", referencedColumnName = "DEVICE_ID", insertable = false, updatable = false)
    public DeviceEntity getDeviceEntity() {
        return deviceEntity;
    }

    public void setDeviceEntity(DeviceEntity deviceEntity) {
        this.deviceEntity = deviceEntity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_FEEDBACK_STATUS_ID", referencedColumnName = "TYPE_FEEDBACK_STATUS_ID", insertable = false, updatable = false)
    public TypeFeedbackStatusEntity getTypeFeedbackStatusEntity() {
        return typeFeedbackStatusEntity;
    }

    public void setTypeFeedbackStatusEntity(TypeFeedbackStatusEntity typeFeedbackStatusEntity) {
        this.typeFeedbackStatusEntity = typeFeedbackStatusEntity;
    }
}
