package ir.matin.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "BOOKMARK", schema = "ROUTAUSER")
public class BookmarkEntity {
    private int bookmarkID;
    private Integer personID;
    private Integer typeBookmarkID;
    private String name;
    private Double latitude;
    private Double longitude;


    private TypeBookmarkEntity typeBookmarkEntity;
    private PersonEntity personEntity;

    @Id
    @Column(name = "BOOKMARK_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Bookmark_Sequence")
    @SequenceGenerator(name = "Bookmark_Sequence", sequenceName = "BOOKMARK_SEQ", allocationSize = 1)
    public int getBookmarkID() {
        return bookmarkID;
    }

    public void setBookmarkID(int bookmarkID) {
        this.bookmarkID = bookmarkID;
    }

    @Basic
    @Column(name="PERSON_ID")
    public Integer getPersonID() {
        return personID;
    }

    public void setPersonID(Integer personID) {
        this.personID = personID;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "LATITUDE")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "LONGITUDE")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Basic
    @Column(name = "TYPE_BOOKMARK_ID")
    public Integer getTypeBookmarkID() {
        return typeBookmarkID;
    }

    public void setTypeBookmarkID(Integer typeBookmarkID) {
        this.typeBookmarkID = typeBookmarkID;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PERSON_ID", referencedColumnName = "PERSON_ID", insertable = false, updatable = false)
    public PersonEntity getPersonEntity() {
        return personEntity;
    }

    public void setPersonEntity(PersonEntity personEntity) {
        this.personEntity = personEntity;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_BOOKMARK_ID", referencedColumnName = "TYPE_BOOKMARK_ID", insertable = false, updatable = false)
    public TypeBookmarkEntity getTypeBookmarkEntity() {
        return typeBookmarkEntity;
    }

    public void setTypeBookmarkEntity(TypeBookmarkEntity typeBookmarkEntity) {
        this.typeBookmarkEntity = typeBookmarkEntity;
    }
}
