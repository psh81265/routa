package ir.matin.database.data.type;

/**
 * Created by pooria on 12/12/2018.
 */
public enum DeviceStateType {
    REGISTER_DEVICE(1),
    LOGIN(2);




    private final int value;

    DeviceStateType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    }
