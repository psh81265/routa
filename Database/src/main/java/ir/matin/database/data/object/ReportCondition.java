package ir.matin.database.data.object;

import ir.matin.utility.data.object.SortOption;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportCondition {

    private Integer pageSize;
    private Integer pageNumber; // start from 1
    private List<SortOption> sortOptions;
    private Boolean export;

    private Map<String, Object> equalCondition;
    private Map<String, String> likeCondition;

    private Map<String, Number> minNumberCondition;
    private Map<String, Number> maxNumberCondition;

    private Map<String, Timestamp> minTimestampCondition;
    private Map<String, Timestamp> maxTimestampCondition;

    public ReportCondition(Integer pageSize, Integer pageNumber, List<SortOption> sortOptions, Boolean export) {
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        this.sortOptions = sortOptions;
        this.export = export;

        this.equalCondition = new HashMap<>();
        this.likeCondition = new HashMap<>();
        this.minNumberCondition = new HashMap<>();
        this.maxNumberCondition = new HashMap<>();
        this.minTimestampCondition = new HashMap<>();
        this.maxTimestampCondition = new HashMap<>();
    }

    //    private Map<String, Object> equalPatterns;
//    private Map<String, List> iNPatterns;
//    private Map<String, String> likeOnLastPatterns;
//    private Map<String, Timestamp> TimestampPatterns;
//    private Map<String, Boolean> booleanPatterns;
//    private Map<String, Number> minNumberPatterns;
//    private Map<String, Number> maxNumberPatterns;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public List<SortOption> getSortOptions() {
        return sortOptions;
    }

    public void setSortOptions(List<SortOption> sortOptions) {
        this.sortOptions = sortOptions;
    }

    public Boolean getExport() {
        return export;
    }

    public void setExport(Boolean export) {
        this.export = export;
    }

    public Map<String, Object> getEqualCondition() {
        return equalCondition;
    }

    public void setEqualCondition(Map<String, Object> equalCondition) {
        this.equalCondition = equalCondition;
    }

    public Map<String, String> getLikeCondition() {
        return likeCondition;
    }

    public void setLikeCondition(Map<String, String> likeCondition) {
        this.likeCondition = likeCondition;
    }

    public Map<String, Number> getMinNumberCondition() {
        return minNumberCondition;
    }

    public void setMinNumberCondition(Map<String, Number> minNumberCondition) {
        this.minNumberCondition = minNumberCondition;
    }

    public Map<String, Number> getMaxNumberCondition() {
        return maxNumberCondition;
    }

    public void setMaxNumberCondition(Map<String, Number> maxNumberCondition) {
        this.maxNumberCondition = maxNumberCondition;
    }

    public Map<String, Timestamp> getMinTimestampCondition() {
        return minTimestampCondition;
    }

    public void setMinTimestampCondition(Map<String, Timestamp> minTimestampCondition) {
        this.minTimestampCondition = minTimestampCondition;
    }

    public Map<String, Timestamp> getMaxTimestampCondition() {
        return maxTimestampCondition;
    }

    public void setMaxTimestampCondition(Map<String, Timestamp> maxTimestampCondition) {
        this.maxTimestampCondition = maxTimestampCondition;
    }
}
