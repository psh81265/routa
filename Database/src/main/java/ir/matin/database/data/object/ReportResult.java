package ir.matin.database.data.object;

import java.util.List;

public class ReportResult<T> {
    private Integer countAll;
    private List<T> result;

    public ReportResult(Integer countAll, List<T> result) {
        this.countAll = countAll;
        this.result = result;
    }

    public Integer getCountAll() {
        return countAll;
    }

    public void setCountAll(Integer countAll) {
        this.countAll = countAll;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }
}
