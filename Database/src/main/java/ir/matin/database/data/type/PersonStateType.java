package ir.matin.database.data.type;

public enum PersonStateType {
    GENERAL(-1),

    ADD_USER(11),
    REGISTER_USER(12),

    UPDATE_INFO(21),
    UPDATE_PICTURE(22),
    UPDATE_PASSWORD(23),
    RESET_PASSWORD(24),

    LOGIN(51),
    LOGOUT(52),
    CONFIRM(61),
    UN_CONFIRM(62),
    ACTIVATE(71),
    DE_ACTIVATE(72),
    BLOCK(81),
    UN_BLOCK(82),
    LOGICAL_DELETE(91);

    private final int value;

    PersonStateType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
