package ir.matin.base.api;

import ir.matin.base.bl.BaseTypeService;
import ir.matin.base.data.dto.ProvinceCityDto;
import ir.matin.base.data.dto.type.TypeCityDto;
import ir.matin.base.data.dto.type.TypeProvinceDto;
import ir.matin.base.data.object.ProvinceCity;
import ir.matin.database.entity.TypeCityEntity;
import ir.matin.database.entity.TypeProvinceEntity;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/rest/public/base/type")
public class PublicBaseTypeController {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private BaseTypeService baseTypeService;

    @GetMapping(path = "/provinces", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<TypeProvinceDto>> provinces() {
        RestResult<List<TypeProvinceDto>> result = new RestResult<>();
        try {
            List<TypeProvinceEntity> provinces = this.baseTypeService.provinces();
            result.setObject(provinces.stream().map(TypeProvinceDto::new)
                    .collect(Collectors.toList()));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/provinceCities/{provinceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<TypeCityDto>> provinceCities(@PathVariable("provinceId") String provinceId) {
        RestResult<List<TypeCityDto>> result = new RestResult<>();
        try {
            Integer id = this.validationService.validateInteger(provinceId);
            List<TypeCityEntity> cities = this.baseTypeService.cities(id);
            result.setObject(cities.stream().map(TypeCityDto::new)
                    .collect(Collectors.toList()));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/cities", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<TypeCityDto>> allCities() {
        RestResult<List<TypeCityDto>> result = new RestResult<>();
        try {
            List<TypeCityEntity> cities = this.baseTypeService.cities(-1);
            result.setObject(cities.stream().map(TypeCityDto::new)
                    .collect(Collectors.toList()));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/cityProvince/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ProvinceCityDto> cityProvince(@PathVariable("city") String city) {
        RestResult<ProvinceCityDto> result = new RestResult<>();
        try {
            String name = this.validationService.validatePersianString(city);
            ProvinceCity provinceCity = this.baseTypeService.provinceByCity(name);
            result.setObject(new ProvinceCityDto(provinceCity));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/search/city", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<TypeCityDto>> searchCity(@RequestBody Map<String, Object> postParameters) {
        RestResult<List<TypeCityDto>> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            List<TypeCityEntity> cities = this.baseTypeService.ajaxSearchCity(name);
            result.setObject(cities.stream().map(TypeCityDto::new).collect(Collectors.toList()));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

//    @PostMapping(path = "/searchCompany", produces = MediaType.APPLICATION_JSON_VALUE)
//    public RestResult<List<CompanyBriefDto>> searchCompanyByScore(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
//        RestResult<List<CompanyBriefDto>> result = new RestResult<>();
//        List<CompanyBriefDto> dtos = new ArrayList<>();
//        List<TypeCityEntity> cities = new ArrayList<>();
//        try {
//            /*PersonEntity personEntity = this.accessService.getPublicUser(request);
//            Map<String, Object> getParameters = new HashMap<>();
//            getParameters.put("city", name);
//            if (personEntity == null)
//                auditLogger.log(NOTICE, createPublicLogPattern(request, getParameters));
//            else
//                auditLogger.log(NOTICE, createAuthenticatedLogPattern(personEntity, request, getParameters));*/
//
////            Integer minScore = this.validationService.validateInteger()
//            Integer minScore = postParameters.containsKey("min") ? (Integer) postParameters.get("min") : null;
//            Integer maxScore = postParameters.containsKey("max") ? (Integer) postParameters.get("max") : null;
//            for (CompanyEntity company : this.databaseService.searchCompanyByScore(minScore, maxScore))
//                dtos.add(new CompanyBriefDto(company));
//            result.setObject(dtos);
//        } catch (SystemException e) {
//            result.setError(e.getUserError());
//            result.setDescription(e.getDescription());
//        }
//        return result;
//    }
}
