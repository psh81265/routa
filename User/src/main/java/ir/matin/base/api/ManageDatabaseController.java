package ir.matin.base.api;

import ir.matin.database.bl.DatabaseResetService;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The General Managerial Controller and Rest Api with Mapping of {@literal "/rest/management/general"},
 * Containing Methods about General Managerial Rest Apis
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@RestController
@RequestMapping(path = "/rest/database/manage")
public class ManageDatabaseController {

    /**
     * The {@link DatabaseResetService} Instance Representing Database Reset Service
     */
    @Autowired
    private DatabaseResetService databaseResetService;

    /**
     * A Rest Api with Type of {@link RequestMethod#GET} and Mapping of {@literal "/rest/management/general/database/reset"} for
     * Resetting Database and Insert Default Values in Tables
     * <ul><li> Possible {@link SystemException} in Function Call of {@link DatabaseResetService#databaseCleanse()} </li></ul>
     *
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     */
    @GetMapping(path = "/cleanse/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> databaseCleanseAll() {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            this.databaseResetService.databaseCleanse();
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/cleanse/permission", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> databaseCleansePermission() {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            this.databaseResetService.databaseCleansePermission();
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/default", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> databaseDefault() {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            this.databaseResetService.databaseDefault();
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/sample", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> databaseSample() {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            this.databaseResetService.databaseSample();
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}
