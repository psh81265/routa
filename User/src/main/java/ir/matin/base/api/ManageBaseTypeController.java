package ir.matin.base.api;

import ir.matin.base.bl.BaseTypeService;
import ir.matin.base.bl.BaseValidationService;
import ir.matin.base.data.dto.type.*;
import ir.matin.base.data.type.BaseChangeState;
import ir.matin.base.data.type.BaseType;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.*;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The General Managerial Controller and Rest Api with Mapping of {@literal "/rest/general/manage"},
 * Containing Methods about General Managerial Rest Apis
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@RestController
@RequestMapping(path = "/rest/base/manage/type")
public class ManageBaseTypeController {
    /**
     * The {@link ValidationService} Instance Representing Validation Service
     */
    @Autowired
    private ValidationService validationService;

    @Autowired
    private BaseTypeService baseTypeService;

    @Autowired
    private BaseValidationService baseValidationService;


    @GetMapping(path = "/report/province", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ReportDto<TypeProvinceDto>> reportTypeProvince(@RequestParam("pageSize") Optional<String> qPageSize,
                                                                     @RequestParam("pageNumber") Optional<String> qPageNumber,
                                                                     @RequestParam("sortBy") Optional<String[]> qSortBy,
                                                                     @RequestParam("export") Optional<String> qExport,
                                                                     @RequestParam("query") Optional<String> qQuery,
                                                                     @RequestParam("name") Optional<String> qName,
                                                                     @RequestParam("latitudeMin") Optional<String> qLatitudeMin,
                                                                     @RequestParam("latitudeMax") Optional<String> qLatitudeMax,
                                                                     @RequestParam("longitudeMin") Optional<String> qLongitudeMin,
                                                                     @RequestParam("longitudeMax") Optional<String> qLongitudeMax) {
        RestResult<ReportDto<TypeProvinceDto>> result = new RestResult<>();
        try {
            Integer pageSize = this.validationService.validatePageSize(qPageSize);
            Integer pageNumber = this.validationService.validatePageNumber(qPageNumber);
            List<SortOption> sortBy = this.validationService.validateSortBy(qSortBy);
            Boolean export = this.validationService.validateExport(qExport);
            Pair<Class, Object> query = this.validationService.validateOptionalGeneral(qQuery);
            String name = this.validationService.validateOptionalPersianString(qName);
            Double latitudeMin = this.validationService.validateOptionalDouble(qLatitudeMin);
            Double latitudeMax = this.validationService.validateOptionalDouble(qLatitudeMax);
            Double longitudeMin = this.validationService.validateOptionalDouble(qLongitudeMin);
            Double longitudeMax = this.validationService.validateOptionalDouble(qLongitudeMax);

            ReportDto<TypeProvinceDto> report = this.baseTypeService.reportProvinceSimple(pageSize, pageNumber, sortBy, export, query,
                    name, latitudeMin, latitudeMax, longitudeMin, longitudeMax);
            result.setObject(report);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/report/city", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ReportDto<TypeCityDto>> reportTypeCity(@RequestParam("pageSize") Optional<String> qPageSize,
                                                             @RequestParam("pageNumber") Optional<String> qPageNumber,
                                                             @RequestParam("sortBy") Optional<String[]> qSortBy,
                                                             @RequestParam("export") Optional<String> qExport,
                                                             @RequestParam("query") Optional<String> qQuery,
                                                             @RequestParam("name") Optional<String> qName,
                                                             @RequestParam("latitudeMin") Optional<String> qLatitudeMin,
                                                             @RequestParam("latitudeMax") Optional<String> qLatitudeMax,
                                                             @RequestParam("longitudeMin") Optional<String> qLongitudeMin,
                                                             @RequestParam("longitudeMax") Optional<String> qLongitudeMax) {
        RestResult<ReportDto<TypeCityDto>> result = new RestResult<>();
        try {
            Integer pageSize = this.validationService.validatePageSize(qPageSize);
            Integer pageNumber = this.validationService.validatePageNumber(qPageNumber);
            List<SortOption> sortBy = this.validationService.validateSortBy(qSortBy);
            Boolean export = this.validationService.validateExport(qExport);
            Pair<Class, Object> query = this.validationService.validateOptionalGeneral(qQuery);
            String name = this.validationService.validateOptionalPersianString(qName);
            Double latitudeMin = this.validationService.validateOptionalDouble(qLatitudeMin);
            Double latitudeMax = this.validationService.validateOptionalDouble(qLatitudeMax);
            Double longitudeMin = this.validationService.validateOptionalDouble(qLongitudeMin);
            Double longitudeMax = this.validationService.validateOptionalDouble(qLongitudeMax);

            ReportDto<TypeCityDto> report = this.baseTypeService.reportCitySimple(pageSize, pageNumber, sortBy, export, query,
                    name, latitudeMin, latitudeMax, longitudeMin, longitudeMax);
            result.setObject(report);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/report/personReason", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ReportDto<TypePersonReasonDto>> reportTypePersonReason(@RequestParam("pageSize") Optional<String> qPageSize,
                                                                             @RequestParam("pageNumber") Optional<String> qPageNumber,
                                                                             @RequestParam("sortBy") Optional<String[]> qSortBy,
                                                                             @RequestParam("export") Optional<String> qExport,
                                                                             @RequestParam("query") Optional<String> qQuery,
                                                                             @RequestParam("name") Optional<String> qName,
                                                                             @RequestParam("type") Optional<String> qType) {
        RestResult<ReportDto<TypePersonReasonDto>> result = new RestResult<>();
        try {
            Integer pageSize = this.validationService.validatePageSize(qPageSize);
            Integer pageNumber = this.validationService.validatePageNumber(qPageNumber);
            List<SortOption> sortBy = this.validationService.validateSortBy(qSortBy);
            Boolean export = this.validationService.validateExport(qExport);
            Pair<Class, Object> query = this.validationService.validateOptionalGeneral(qQuery);
            String name = this.validationService.validateOptionalPersianString(qName);
            Integer type = this.validationService.validateOptionalInteger(qType);

            ReportDto<TypePersonReasonDto> report = this.baseTypeService.reportPersonReasonSimple(pageSize, pageNumber, sortBy, export, query,
                    name, type);
            result.setObject(report);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/add/province", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Integer> addProvince(@RequestBody Map<String, Object> postParameters) {
        RestResult<Integer> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("latitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("longitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            Double latitude = this.validationService.validateDouble(postParameters.get("latitude").toString());
            Double longitude = this.validationService.validateDouble(postParameters.get("longitude").toString());

            Integer provinceId = this.baseTypeService.addProvince(name, latitude, longitude);
            result.setObject(provinceId);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/add/city", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Integer> addCity(@RequestBody Map<String, Object> postParameters) {
        RestResult<Integer> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("provinceId"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "provinceId", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("latitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("longitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer provinceId = this.validationService.validateInteger(postParameters.get("provinceId").toString());
            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            Double latitude = this.validationService.validateDouble(postParameters.get("latitude").toString());
            Double longitude = this.validationService.validateDouble(postParameters.get("longitude").toString());

            Integer cityId = this.baseTypeService.addCity(provinceId, name, latitude, longitude);
            result.setObject(cityId);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/add/personReason", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Integer> addPersonReason(@RequestBody Map<String, Object> postParameters) {
        RestResult<Integer> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("type"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "type", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            Integer type = this.validationService.validateInteger(postParameters.get("type").toString());

            Integer personReasonId = this.baseTypeService.addPersonReason(name, type);
            result.setObject(personReasonId);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/edit/province", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> editProvince(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("provinceId"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "provinceId", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("latitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("longitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer provinceId = this.validationService.validateInteger(postParameters.get("provinceId").toString());
            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            Double latitude = this.validationService.validateDouble(postParameters.get("latitude").toString());
            Double longitude = this.validationService.validateDouble(postParameters.get("longitude").toString());

            this.baseTypeService.editProvince(provinceId, name, latitude, longitude);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/edit/city", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> editCity(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("cityId"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "cityId", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("provinceId"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "provinceId", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("latitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "latitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("longitude"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "longitude", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer cityId = this.validationService.validateInteger(postParameters.get("cityId").toString());
            Integer provinceId = this.validationService.validateInteger(postParameters.get("provinceId").toString());
            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            Double latitude = this.validationService.validateDouble(postParameters.get("latitude").toString());
            Double longitude = this.validationService.validateDouble(postParameters.get("longitude").toString());

            this.baseTypeService.editCity(cityId, provinceId, name, latitude, longitude);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/edit/PersonReason", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> editPersonReason(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("personReasonId"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "personReasonId", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("name"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "name", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("type"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "type", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer personReasonId = this.validationService.validateInteger(postParameters.get("personReasonId").toString());
            String name = this.validationService.validatePersianString(postParameters.get("name").toString());
            Integer type = this.validationService.validateInteger(postParameters.get("type").toString());

            this.baseTypeService.editPersonReason(personReasonId, name, type);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/enable/{type_str}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> enable(@PathVariable("type_str") String type_str,
                                            @RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("id"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "id", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer id = this.validationService.validateInteger(postParameters.get("id").toString());
            BaseType type = this.baseValidationService.validateBaseType(type_str);

            this.baseTypeService.changeState(type, id, BaseChangeState.ENABLE);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/disable/{type_str}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> disable(@PathVariable("type_str") String type_str,
                                             @RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("id"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "id", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer id = this.validationService.validateInteger(postParameters.get("id").toString());
            BaseType type = this.baseValidationService.validateBaseType(type_str);

            this.baseTypeService.changeState(type, id, BaseChangeState.DISABLE);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/delete/{type_str}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> delete(@PathVariable("type_str") String type_str,
                                            @RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("id"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "id", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer id = this.validationService.validateInteger(postParameters.get("id").toString());
            BaseType type = this.baseValidationService.validateBaseType(type_str);

            this.baseTypeService.changeState(type, id, BaseChangeState.DELETE);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}
