package ir.matin.base.api;

import ir.matin.base.bl.BaseSystemService;
import ir.matin.base.data.dto.AppVersionDto;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.*;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * The General Managerial Controller and Rest Api with Mapping of {@literal "/rest/general/manage"},
 * Containing Methods about General Managerial Rest Apis
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@RestController
@RequestMapping(path = "/rest/base/manage/system")
public class ManageBaseSystemController {
    /**
     * The {@link ValidationService} Instance Representing Validation Service
     */
    @Autowired
    private ValidationService validationService;

    @Autowired
    private BaseSystemService baseSystemService;

    @GetMapping(path = "/report/appVersion", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ReportDto<AppVersionDto>> reportAppVersion(@RequestParam("pageSize") Optional<String> qPageSize,
                                                                 @RequestParam("pageNumber") Optional<String> qPageNumber,
                                                                 @RequestParam("sortBy") Optional<String[]> qSortBy,
                                                                 @RequestParam("export") Optional<String> qExport,
                                                                 @RequestParam("query") Optional<String> qQuery,
                                                                 @RequestParam("appType") Optional<String> qAppType,
                                                                 @RequestParam("version") Optional<String> qVersion,
                                                                 @RequestParam("releaseMin") Optional<String> qReleaseMin,
                                                                 @RequestParam("releaseMax") Optional<String> qReleaseMax,
                                                                 @RequestParam("stable") Optional<String> qStable,
                                                                 @RequestParam("force") Optional<String> qForce) {
        RestResult<ReportDto<AppVersionDto>> result = new RestResult<>();
        try {
            Integer pageSize = this.validationService.validatePageSize(qPageSize);
            Integer pageNumber = this.validationService.validatePageNumber(qPageNumber);
            List<SortOption> sortBy = this.validationService.validateSortBy(qSortBy);
            Boolean export = this.validationService.validateExport(qExport);
            Pair<Class, Object> query = this.validationService.validateOptionalGeneral(qQuery);
            Integer appType = this.validationService.validateOptionalInteger(qAppType);
            String version = this.validationService.validateOptionalPersianString(qVersion);
            Timestamp releaseMin = this.validationService.validateOptionalTimestamp(qReleaseMin);
            Timestamp releaseMax = this.validationService.validateOptionalTimestamp(qReleaseMax);
            Boolean stable = this.validationService.validateOptionalBoolean(qStable);
            Boolean force = this.validationService.validateOptionalBoolean(qForce);

            ReportDto<AppVersionDto> report = this.baseSystemService.reportAppVersionSimple(pageSize, pageNumber, sortBy, export, query,
                    appType, version, releaseMin, releaseMax, stable, force);
            result.setObject(report);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/add/appVersion", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Integer> addAppVersion(@RequestBody Map<String, Object> postParameters) {
        RestResult<Integer> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("appType"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "type", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("version"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "version", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("url"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "url", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("stable"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "stable", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("force"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "force", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer appType = this.validationService.validateInteger(postParameters.get("appType").toString());
            String version = this.validationService.validatePersianString(postParameters.get("version").toString());
            String url = this.validationService.validatePersianString(postParameters.get("url").toString());
            Boolean stable = this.validationService.validateBoolean(postParameters.get("stable").toString());
            Boolean force = this.validationService.validateBoolean(postParameters.get("force").toString());

            Integer appTypeId = this.baseSystemService.addAppType(appType, version, url, stable, force);
            result.setObject(appTypeId);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/edit/appVersion", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> editAppVersion(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey("appVersionId"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "appVersionId", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("appType"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "type", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("version"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "version", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("url"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "url", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("stable"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "stable", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("force"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "force", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer appVersionId = this.validationService.validateInteger(postParameters.get("appVersionId").toString());
            Integer appType = this.validationService.validateInteger(postParameters.get("appType").toString());
            String version = this.validationService.validatePersianString(postParameters.get("version").toString());
            String url = this.validationService.validatePersianString(postParameters.get("url").toString());
            Boolean stable = this.validationService.validateBoolean(postParameters.get("stable").toString());
            Boolean force = this.validationService.validateBoolean(postParameters.get("force").toString());

            this.baseSystemService.editAppType(appVersionId, appType, version, url, stable, force);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

}
