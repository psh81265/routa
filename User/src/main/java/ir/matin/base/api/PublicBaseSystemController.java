package ir.matin.base.api;

import ir.matin.base.bl.BaseSystemService;
import ir.matin.base.bl.BaseValidationService;
import ir.matin.base.data.dto.BriefAppVersionDto;
import ir.matin.base.data.type.AppType;
import ir.matin.database.entity.AppVersionEntity;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/rest/public/base/system")
public class PublicBaseSystemController {

    @Autowired
    private BaseValidationService baseValidationService;

    @Autowired
    private BaseSystemService baseSystemService;

    @GetMapping(path = "/app/version/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<BriefAppVersionDto> getAppVersion(@PathVariable("type") String type) {
        RestResult<BriefAppVersionDto> result = new RestResult<>();
        try {
            AppType appType = this.baseValidationService.validateAppType(type);
            AppVersionEntity appVersionEntity = baseSystemService.getLatestVersion(appType);
            result.setObject(new BriefAppVersionDto(appVersionEntity));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}
