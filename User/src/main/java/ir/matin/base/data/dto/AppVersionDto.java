package ir.matin.base.data.dto;

import ir.matin.database.entity.AppVersionEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;
import java.sql.Timestamp;

public class AppVersionDto implements Serializable {
    private Integer id;
    private Integer appType;
    private String version;
    private String url;
    private Timestamp release;
    private Boolean stable;
    private Boolean force;

    public AppVersionDto(AppVersionEntity appVersionEntity) {
        if (appVersionEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.id = appVersionEntity.getAppVersionID();
        this.appType = appVersionEntity.getAppType();
        this.version = appVersionEntity.getVersion();
        this.url = appVersionEntity.getUrl();
        this.release = appVersionEntity.getRelease();
        this.stable = appVersionEntity.getStable();
        this.force = appVersionEntity.getForce();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAppType() {
        return appType;
    }

    public void setAppType(Integer appType) {
        this.appType = appType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getRelease() {
        return release;
    }

    public void setRelease(Timestamp release) {
        this.release = release;
    }

    public Boolean getStable() {
        return stable;
    }

    public void setStable(Boolean stable) {
        this.stable = stable;
    }

    public Boolean getForce() {
        return force;
    }

    public void setForce(Boolean force) {
        this.force = force;
    }
}
