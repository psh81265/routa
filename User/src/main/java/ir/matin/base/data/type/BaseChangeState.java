package ir.matin.base.data.type;

public enum BaseChangeState {
    ENABLE,
    DISABLE,
    DELETE
}
