package ir.matin.base.data.dto;

import ir.matin.database.entity.AppVersionEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;

public class BriefAppVersionDto implements Serializable {
    private String version;
    private String url;
    private Boolean force;

    public BriefAppVersionDto(AppVersionEntity appVersionEntity) {
        if (appVersionEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.version = appVersionEntity.getVersion();
        this.url = appVersionEntity.getUrl();
        this.force = appVersionEntity.getForce();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getForce() {
        return force;
    }

    public void setForce(Boolean force) {
        this.force = force;
    }
}
