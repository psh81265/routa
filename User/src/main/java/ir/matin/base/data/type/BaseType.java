package ir.matin.base.data.type;

public enum BaseType {
    PROVINCE,
    CITY,
    PERSON_REASON
}
