package ir.matin.base.data.dto.type;

import ir.matin.database.entity.TypeCityEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;

public class TypeCityDto implements Serializable {
    private Integer cityId;
    private Integer provinceId;
    private String cityName;
    private String provinceName;
    private Double latitude;
    private Double longitude;
    private Boolean enabled;

    public TypeCityDto(TypeCityEntity typeCityEntity) {
        if (typeCityEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.cityId = typeCityEntity.getTypeCityID();
        this.provinceId = typeCityEntity.getTypeProvinceID();
        this.cityName = typeCityEntity.getName();
        this.provinceName = typeCityEntity.getTypeProvinceEntity().getName();
        this.latitude = typeCityEntity.getLatitude();
        this.longitude = typeCityEntity.getLongitude();
        this.enabled = typeCityEntity.getEnabled();
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
