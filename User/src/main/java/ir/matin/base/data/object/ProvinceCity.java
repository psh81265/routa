package ir.matin.base.data.object;

import ir.matin.database.entity.TypeCityEntity;
import ir.matin.database.entity.TypeProvinceEntity;

import java.util.List;

public class ProvinceCity {

    private TypeProvinceEntity typeProvinceEntity;
    private TypeCityEntity typeCityEntity;
    private List<TypeCityEntity> typeCityEntities;

    public ProvinceCity(TypeProvinceEntity typeProvinceEntity, TypeCityEntity typeCityEntity, List<TypeCityEntity> typeCityEntities) {
        this.typeProvinceEntity = typeProvinceEntity;
        this.typeCityEntity = typeCityEntity;
        this.typeCityEntities = typeCityEntities;
    }

    public TypeProvinceEntity getTypeProvinceEntity() {
        return typeProvinceEntity;
    }

    public void setTypeProvinceEntity(TypeProvinceEntity typeProvinceEntity) {
        this.typeProvinceEntity = typeProvinceEntity;
    }

    public TypeCityEntity getTypeCityEntity() {
        return typeCityEntity;
    }

    public void setTypeCityEntity(TypeCityEntity typeCityEntity) {
        this.typeCityEntity = typeCityEntity;
    }

    public List<TypeCityEntity> getTypeCityEntities() {
        return typeCityEntities;
    }

    public void setTypeCityEntities(List<TypeCityEntity> typeCityEntities) {
        this.typeCityEntities = typeCityEntities;
    }
}
