package ir.matin.base.data.dto;

import ir.matin.base.data.dto.type.TypeCityDto;
import ir.matin.base.data.dto.type.TypeProvinceDto;
import ir.matin.base.data.object.ProvinceCity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class ProvinceCityDto implements Serializable {
    private TypeProvinceDto province;
    private TypeCityDto city;
    private List<TypeCityDto> otherCities;

    public ProvinceCityDto(ProvinceCity provinceCity) {
        if (provinceCity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.province = new TypeProvinceDto(provinceCity.getTypeProvinceEntity());
        this.city = new TypeCityDto(provinceCity.getTypeCityEntity());
        this.otherCities = provinceCity.getTypeCityEntities().stream().map(TypeCityDto::new).collect(Collectors.toList());
    }

    public TypeProvinceDto getProvince() {
        return province;
    }

    public void setProvince(TypeProvinceDto province) {
        this.province = province;
    }

    public TypeCityDto getCity() {
        return city;
    }

    public void setCity(TypeCityDto city) {
        this.city = city;
    }

    public List<TypeCityDto> getOtherCities() {
        return otherCities;
    }

    public void setOtherCities(List<TypeCityDto> otherCities) {
        this.otherCities = otherCities;
    }
}
