package ir.matin.base.data.dto.type;

import ir.matin.database.entity.TypePersonReasonEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;

public class TypePersonReasonDto implements Serializable {
    private Integer id;
    private String name;
    private Integer type;
    private Boolean enabled;

    public TypePersonReasonDto(TypePersonReasonEntity typePersonReasonEntity) {
        if (typePersonReasonEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.id = typePersonReasonEntity.getTypePersonReasonID();
        this.name = typePersonReasonEntity.getName();
        this.type = typePersonReasonEntity.getType();
        this.enabled = typePersonReasonEntity.getEnabled();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
