package ir.matin.base.data.dto.type;

import ir.matin.database.entity.TypeProvinceEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;

public class TypeProvinceDto implements Serializable {
    private Integer provinceId;
    private String name;
    private Double latitude;
    private Double longitude;
    private Boolean enabled;

    public TypeProvinceDto(TypeProvinceEntity typeProvinceEntity) {
        if (typeProvinceEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.provinceId = typeProvinceEntity.getTypeProvinceID();
        this.name = typeProvinceEntity.getName();
        this.latitude = typeProvinceEntity.getLatitude();
        this.longitude = typeProvinceEntity.getLongitude();
        this.enabled = typeProvinceEntity.getEnabled();
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
