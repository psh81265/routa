package ir.matin.base.data.type;

public enum AppType {
    DRIVER(100),
    CUSTOMER(200);

    private final int value;

    AppType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
