package ir.matin.base.bl;

import ir.matin.base.data.dto.type.*;
import ir.matin.base.data.object.ProvinceCity;
import ir.matin.base.data.type.BaseChangeState;
import ir.matin.base.data.type.BaseType;
import ir.matin.database.bl.ReportTypeService;
import ir.matin.database.dao.*;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.*;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Database Service Class,
 * Containing Methods about Database
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class BaseTypeService {

    @Autowired
    private TypeProvinceDao typeProvinceDao;

    @Autowired
    private TypeCityDao typeCityDao;

    @Autowired
    private TypePersonReasonDao typePersonReasonDao;

    @Autowired
    private ReportTypeService reportTypeService;

    /* ********************************************************************************** */

    public List<TypeProvinceEntity> provinces() {
        List<TypeProvinceEntity> provinces = this.typeProvinceDao.listEnabled();
        if (provinces.isEmpty())
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        return provinces;
    }

    public List<TypeCityEntity> cities(Integer provinceId) {
        List<TypeCityEntity> cities = (provinceId <= 0) ? this.typeCityDao.listEnabled() : this.typeCityDao.listEnabled(provinceId);
        if (cities.isEmpty())
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        return cities;
    }

    public ProvinceCity provinceByCity(String city) {
        TypeCityEntity typeCityEntity = this.typeCityDao.getCityByName(city);
        if (typeCityEntity == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        List<TypeCityEntity> cities = this.typeCityDao.listEnabled(typeCityEntity.getTypeProvinceID());
        return new ProvinceCity(typeCityEntity.getTypeProvinceEntity(), typeCityEntity, cities);
    }

    /* ********************************************************************************** */

    public List<TypeCityEntity> ajaxSearchCity(String name) {
        if (name == null || name.length() <= 1)
            return new ArrayList<>();

        List result = this.typeCityDao.listLikeName(name);
        return (result != null) ? result : new ArrayList<>();
    }

    /* ********************************************************************************** */

    public ReportDto<TypeProvinceDto> reportProvinceSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                           String name, Double latitudeMin, Double latitudeMax,
                                                           Double longitudeMin, Double longitudeMax) {
        ReportResult<TypeProvinceEntity> report = this.reportTypeService.reportProvinceSimple(pageSize, pageNumber, sortBy, export, query,
                name, latitudeMin, latitudeMax, longitudeMin, longitudeMax);

        if (report.getCountAll() == 0 || report.getResult() == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        ReportDto<TypeProvinceDto> reportDto = new ReportDto<>(report.getCountAll(), pageSize, pageNumber);
        reportDto.setResult(report.getResult().stream().map(TypeProvinceDto::new).collect(Collectors.toList()));
        return reportDto;
    }

    public ReportDto<TypeCityDto> reportCitySimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                   String name, Double latitudeMin, Double latitudeMax,
                                                   Double longitudeMin, Double longitudeMax) {
        ReportResult<TypeCityEntity> report = this.reportTypeService.reportCitySimple(pageSize, pageNumber, sortBy, export, query,
                name, latitudeMin, latitudeMax, longitudeMin, longitudeMax);

        if (report.getCountAll() == 0 || report.getResult() == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        ReportDto<TypeCityDto> reportDto = new ReportDto<>(report.getCountAll(), pageSize, pageNumber);
        reportDto.setResult(report.getResult().stream().map(TypeCityDto::new).collect(Collectors.toList()));
        return reportDto;
    }

    public ReportDto<TypePersonReasonDto> reportPersonReasonSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                                   String name, Integer type) {
        ReportResult<TypePersonReasonEntity> report = this.reportTypeService.reportPersonReasonSimple(pageSize, pageNumber, sortBy, export, query,
                name, type);

        if (report.getCountAll() == 0 || report.getResult() == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        ReportDto<TypePersonReasonDto> reportDto = new ReportDto<>(report.getCountAll(), pageSize, pageNumber);
        reportDto.setResult(report.getResult().stream().map(TypePersonReasonDto::new).collect(Collectors.toList()));
        return reportDto;
    }

    /* ************************************************************************************** */

    public Integer addProvince(String name, Double latitude, Double longitude) {
        TypeProvinceEntity typeProvinceEntity = new TypeProvinceEntity();
        typeProvinceEntity.setName(name);
        typeProvinceEntity.setLatitude(latitude);
        typeProvinceEntity.setLongitude(longitude);
        typeProvinceEntity.setEnabled(true);
        typeProvinceEntity.setDeleted(false);

        return this.typeProvinceDao.add(typeProvinceEntity);
    }

    public Integer addCity(Integer provinceId, String name, Double latitude, Double longitude) {
        TypeCityEntity typeCityEntity = new TypeCityEntity();
        typeCityEntity.setTypeProvinceID(provinceId);
        typeCityEntity.setName(name);
        typeCityEntity.setLatitude(latitude);
        typeCityEntity.setLongitude(longitude);
        typeCityEntity.setEnabled(true);
        typeCityEntity.setDeleted(false);

        return this.typeCityDao.add(typeCityEntity);
    }

    public Integer addPersonReason(String name, Integer type) {
        TypePersonReasonEntity typePersonReasonEntity = new TypePersonReasonEntity();
        typePersonReasonEntity.setName(name);
        typePersonReasonEntity.setType(type);
        typePersonReasonEntity.setEnabled(true);
        typePersonReasonEntity.setDeleted(false);

        return this.typePersonReasonDao.add(typePersonReasonEntity);
    }

    /* ************************************************************************************** */

    public void editProvince(Integer provinceId, String name, Double latitude, Double longitude) {
        TypeProvinceEntity typeProvinceEntity = this.typeProvinceDao.get(provinceId);
        if (typeProvinceEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());

        typeProvinceEntity.setName(name);
        typeProvinceEntity.setLatitude(latitude);
        typeProvinceEntity.setLongitude(longitude);

        this.typeProvinceDao.update(typeProvinceEntity);
    }

    public void editCity(Integer cityId, Integer provinceId, String name, Double latitude, Double longitude) {
        TypeCityEntity typeCityEntity = this.typeCityDao.get(cityId);
        if (typeCityEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());

        typeCityEntity.setTypeProvinceID(provinceId);
        typeCityEntity.setName(name);
        typeCityEntity.setLatitude(latitude);
        typeCityEntity.setLongitude(longitude);

        this.typeCityDao.update(typeCityEntity);
    }

    public void editPersonReason(Integer personReasonId, String name, Integer type) {
        TypePersonReasonEntity typePersonReasonEntity = this.typePersonReasonDao.get(personReasonId);
        if (typePersonReasonEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());

        typePersonReasonEntity.setName(name);
        typePersonReasonEntity.setType(type);

        this.typePersonReasonDao.update(typePersonReasonEntity);
    }

    /* ************************************************************************************** */

    public void changeState(BaseType type, Integer id, BaseChangeState state) {
        switch (type) {
            case PROVINCE:
                TypeProvinceEntity typeProvinceEntity = this.typeProvinceDao.get(id);
                if (typeProvinceEntity == null)
                    throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
                switch (state) {
                    case ENABLE:
                        this.typeProvinceDao.enable(id);
                        break;
                    case DISABLE:
                        this.typeProvinceDao.disable(id);
                        break;
                    case DELETE:
                        this.typeProvinceDao.delete(id);
                        break;
                }
                break;
            case CITY:
                TypeCityEntity typeCityEntity = this.typeCityDao.get(id);
                if (typeCityEntity == null)
                    throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
                switch (state) {
                    case ENABLE:
                        this.typeCityDao.enable(id);
                        break;
                    case DISABLE:
                        this.typeCityDao.disable(id);
                        break;
                    case DELETE:
                        this.typeCityDao.delete(id);
                        break;
                }
                break;
            case PERSON_REASON:
                TypePersonReasonEntity typePersonReasonEntity = this.typePersonReasonDao.get(id);
                if (typePersonReasonEntity == null)
                    throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
                switch (state) {
                    case ENABLE:
                        this.typePersonReasonDao.enable(id);
                        break;
                    case DISABLE:
                        this.typePersonReasonDao.disable(id);
                        break;
                    case DELETE:
                        this.typePersonReasonDao.delete(id);
                        break;
                }
                break;
        }
    }
}
