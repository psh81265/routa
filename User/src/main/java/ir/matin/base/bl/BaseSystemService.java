package ir.matin.base.bl;

import ir.matin.base.data.dto.AppVersionDto;
import ir.matin.base.data.type.AppType;
import ir.matin.database.bl.ReportSystemService;
import ir.matin.database.dao.AppVersionDao;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.AppVersionEntity;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SortOption;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BaseSystemService {

    @Autowired
    private AppVersionDao appVersionDao;

    @Autowired
    private ReportSystemService reportSystemService;

    /* ********************************************************************************** */

    public ReportDto<AppVersionDto> reportAppVersionSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                           Integer appType, String version, Timestamp releaseMin, Timestamp releaseMax, Boolean stable, Boolean force) {
        ReportResult<AppVersionEntity> report = this.reportSystemService.reportAppVersion(pageSize, pageNumber, sortBy, export, query,
                appType, version, releaseMin, releaseMax, stable, force);

        if (report.getCountAll() == 0 || report.getResult() == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        ReportDto<AppVersionDto> reportDto = new ReportDto<>(report.getCountAll(), pageSize, pageNumber);
        reportDto.setResult(report.getResult().stream().map(AppVersionDto::new).collect(Collectors.toList()));
        return reportDto;
    }

    /* ********************************************************************************** */

    public Integer addAppType(Integer appType, String version, String url, Boolean stable, Boolean force) {
        AppVersionEntity appVersionEntity = new AppVersionEntity();
        appVersionEntity.setAppType(appType);
        appVersionEntity.setVersion(version);
        appVersionEntity.setUrl(url);
        appVersionEntity.setStable(stable);
        appVersionEntity.setForce(force);
        appVersionEntity.setRelease(new Timestamp(new Date().getTime()));

        return this.appVersionDao.add(appVersionEntity);
    }

    public void editAppType(Integer appVersionId, Integer appType, String version, String url, Boolean stable, Boolean force) {
        AppVersionEntity appVersionEntity = this.appVersionDao.get(appVersionId);
        if (appVersionEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());

        appVersionEntity.setAppType(appType);
        appVersionEntity.setVersion(version);
        appVersionEntity.setUrl(url);
        appVersionEntity.setStable(stable);
        appVersionEntity.setForce(force);

        this.appVersionDao.update(appVersionEntity);
    }

    /* ********************************************************************************** */

    public AppVersionEntity getLatestVersion(AppType appType) {
        AppVersionEntity appVersionEntity = appVersionDao.getLatestVersion(appType.getValue());
        if (appVersionEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        return appVersionEntity;
    }

}
