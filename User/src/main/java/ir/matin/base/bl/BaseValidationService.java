package ir.matin.base.bl;

import ir.matin.base.data.type.AppType;
import ir.matin.base.data.type.BaseType;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.stereotype.Service;

@Service
public class BaseValidationService {

    public AppType validateAppType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "driver":
                return AppType.DRIVER;
            case "customer":
                return AppType.CUSTOMER;
        }
        throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
    }

    public BaseType validateBaseType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "province":
                return BaseType.PROVINCE;
            case "city":
                return BaseType.CITY;
            case "personreason":
                return BaseType.PERSON_REASON;
        }
        throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
    }
}
