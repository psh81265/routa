package ir.matin.user.api;

import ir.matin.database.data.type.PersonStateType;
import ir.matin.database.entity.PersonEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.security.bl.*;
import ir.matin.security.data.dto.RoleDto;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.type.RoleCategoryType;
import ir.matin.user.bl.ProfileService;
import ir.matin.user.bl.UserHistoryService;
import ir.matin.user.bl.UserService;
import ir.matin.user.bl.UserValidationService;
import ir.matin.user.data.dto.GisManDTO;
import ir.matin.user.data.dto.OperatorDto;
import ir.matin.user.data.dto.PersonDto;
import ir.matin.user.data.type.SexType;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.*;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The User Managerial Controller and Rest Api with Mapping of {@literal "/rest/management/user"},
 * Containing Methods about User Managerial Rest Apis
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@RestController
@RequestMapping(path = "/rest/user/manage")
public class ManageUserController {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstname";
    private static final String LAST_NAME = "lastname";
    private static final String MOBILE = "mobile";
    private static final String SEX = "sex";
    private static final String VALIDATION_CODE = "validationCode";
    private static final String ROLE_CATEGORY = "roleCategory";
    private static final String ROLE_ID = "roleId";
    private static final String NATIONAL_ID = "nationalId";
    private static final String CITY_ID = "cityId";
    private static final String ADDRESS = "address";
    private static final String ZIP_CODE = "zipCode";
    private static final String PHONE = "phone";
    private static final String FATHER = "father";
    private static final String BIRTHDAY = "birthday";
    private static final String USER_ID = "userId";
    private static final String REASON_ID = "reasonId";

    @Autowired
    private RolePermissionService rolePermissionService;

    /**
     * The {@link ProfileService} Instance Representing Profile Based Service
     */
    @Autowired
    private ProfileService profileService;

    /**
     * The {@link UserService} Instance Representing General User Service
     */
    @Autowired
    private UserService userService;

    @Autowired
    private HeaderService headerService;
    /**
     * The {@link ValidationService} Instance Representing Validation Service
     */
    @Autowired
    private ValidationService validationService;

    @Autowired
    private SecurityValidationService securityValidationService;

    /**
     * The {@link UserValidationService} Instance Representing User Validation Service
     */
    @Autowired
    private UserValidationService userValidationService;
    @Autowired
    private AccessService accessService;
    @Autowired
    private UserHistoryService userHistoryService;
    @Autowired
    private AvailableService availableService;

    /* ************************************************************************************** */

    @GetMapping(path = "/list/role", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<List<RoleDto>> listRole() {
        RestResult<List<RoleDto>> result = new RestResult<>();
        try {
            List<RoleEntity> roles = this.rolePermissionService.listRole();
            result.setObject(roles.stream().map(RoleDto::new).collect(Collectors.toList()));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @GetMapping(path = "/report/operator", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ReportDto<OperatorDto>> reportOperator(@RequestParam("pageSize") Optional<String> qPageSize,
                                                             @RequestParam("pageNumber") Optional<String> qPageNumber,
                                                             @RequestParam("sortBy") Optional<String[]> qSortBy,
                                                             @RequestParam("export") Optional<String> qExport,
                                                             @RequestParam("query") Optional<String> qQuery,
                                                             @RequestParam("confirm") Optional<String> qConfirm,
                                                             @RequestParam("active") Optional<String> qActive,
                                                             @RequestParam("block") Optional<String> qBlock,
                                                             @RequestParam("selfRegister") Optional<String> qSelfRegister,
                                                             @RequestParam(USERNAME) Optional<String> qUsername,
                                                             @RequestParam(FIRST_NAME) Optional<String> qFirstname,
                                                             @RequestParam(LAST_NAME) Optional<String> qLastname,
                                                             @RequestParam(NATIONAL_ID) Optional<String> qNationalId,
                                                             @RequestParam(MOBILE) Optional<String> qMobile,
                                                             @RequestParam(PHONE) Optional<String> qPhone,
                                                             @RequestParam(EMAIL) Optional<String> qEmail,
                                                             @RequestParam(FATHER) Optional<String> qFather,
                                                             @RequestParam("province") Optional<String> qProvince,
                                                             @RequestParam("city") Optional<String> qCity,
                                                             @RequestParam(ADDRESS) Optional<String> qAddress,
                                                             @RequestParam(ZIP_CODE) Optional<String> qZipCode,
                                                             @RequestParam("birthdayMin") Optional<String> qBirthdayMin,
                                                             @RequestParam("birthdayMax") Optional<String> qBirthdayMax,
                                                             @RequestParam("sex") Optional<String> qSex,
                                                             @RequestParam("creditMin") Optional<String> qCreditMin,
                                                             @RequestParam("creditMax") Optional<String> qCreditMax,
                                                             @RequestParam("speciality") Optional<String> qSpeciality,
                                                             @RequestParam("salaryMin") Optional<String> qSalaryMin,
                                                             @RequestParam("salaryMax") Optional<String> qSalaryMax) {
        RestResult<ReportDto<OperatorDto>> result = new RestResult<>();
        try {
            Integer pageSize = this.validationService.validatePageSize(qPageSize);
            Integer pageNumber = this.validationService.validatePageNumber(qPageNumber);
            List<SortOption> sortBy = this.validationService.validateSortBy(qSortBy);
            Boolean export = this.validationService.validateExport(qExport);
            Pair<Class, Object> query = this.validationService.validateOptionalGeneral(qQuery);
            Boolean confirm = this.validationService.validateOptionalBoolean(qConfirm);
            Boolean active = this.validationService.validateOptionalBoolean(qActive);
            Boolean block = this.validationService.validateOptionalBoolean(qBlock);
            Boolean selfRegister = this.validationService.validateOptionalBoolean(qSelfRegister);
            String username = this.userValidationService.validateOptionalUsername(qUsername);
            String firstname = this.validationService.validateOptionalPersianString(qFirstname);
            String lastname = this.validationService.validateOptionalPersianString(qLastname);
            String nationalId = this.userValidationService.validateOptionalNationalId(qNationalId);
            String mobile = this.validationService.validateOptionalMobile(qMobile);
            String phone = this.validationService.validateOptionalPhoneFax(qPhone);
            String email = this.validationService.validateOptionalEmail(qEmail);
            String father = this.validationService.validateOptionalPersianString(qFather);
            String address = this.validationService.validateOptionalPersianString(qAddress);
            String zipCode = this.validationService.validateOptionalZipCode(qZipCode);
            Timestamp birthdayMin = this.validationService.validateOptionalTimestamp(qBirthdayMin);
            Timestamp birthdayMax = this.validationService.validateOptionalTimestamp(qBirthdayMax);
            SexType sexType = this.userValidationService.validateOptionalSexType(qSex);
            Integer creditMin = this.validationService.validateOptionalInteger(qCreditMin);
            Integer creditMax = this.validationService.validateOptionalInteger(qCreditMax);
            String speciality = this.validationService.validateOptionalPersianString(qSpeciality);
            Integer salaryMin = this.validationService.validateOptionalInteger(qSalaryMin);
            Integer salaryMax = this.validationService.validateOptionalInteger(qSalaryMax);

            ReportDto<OperatorDto> report = this.userService.reportOperatorSimple(pageSize, pageNumber, sortBy, export, query,
                    confirm, active, block, selfRegister, username, firstname, lastname, nationalId, mobile, phone,
                    email, father, address, zipCode, birthdayMin, birthdayMax, sexType, creditMin, creditMax, speciality,
                    salaryMin, salaryMax);
            result.setObject(report);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @GetMapping(path = "/report/gisman", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<ReportDto<GisManDTO>> reportGisman(@RequestParam("pageSize") Optional<String> qPageSize,
                                                         @RequestParam("pageNumber") Optional<String> qPageNumber,
                                                         @RequestParam("sortBy") Optional<String[]> qSortBy,
                                                         @RequestParam("export") Optional<String> qExport,
                                                         @RequestParam("query") Optional<String> qQuery,
                                                         @RequestParam("confirm") Optional<String> qConfirm,
                                                         @RequestParam("active") Optional<String> qActive,
                                                         @RequestParam("block") Optional<String> qBlock,
                                                         @RequestParam("selfRegister") Optional<String> qSelfRegister,
                                                         @RequestParam(USERNAME) Optional<String> qUsername,
                                                         @RequestParam(FIRST_NAME) Optional<String> qFirstname,
                                                         @RequestParam(LAST_NAME) Optional<String> qLastname,
                                                         @RequestParam(NATIONAL_ID) Optional<String> qNationalId,
                                                         @RequestParam(MOBILE) Optional<String> qMobile,
                                                         @RequestParam(PHONE) Optional<String> qPhone,
                                                         @RequestParam(EMAIL) Optional<String> qEmail,
                                                         @RequestParam(FATHER) Optional<String> qFather,
                                                         @RequestParam("province") Optional<String> qProvince,
                                                         @RequestParam("city") Optional<String> qCity,
                                                         @RequestParam(ADDRESS) Optional<String> qAddress,
                                                         @RequestParam(ZIP_CODE) Optional<String> qZipCode,
                                                         @RequestParam("birthdayMin") Optional<String> qBirthdayMin,
                                                         @RequestParam("birthdayMax") Optional<String> qBirthdayMax,
                                                         @RequestParam("sex") Optional<String> qSex,
                                                         @RequestParam("creditMin") Optional<String> qCreditMin,
                                                         @RequestParam("creditMax") Optional<String> qCreditMax,
                                                         @RequestParam("licenseType") Optional<String> qLicenseType,
                                                         @RequestParam("licenseSerial") Optional<String> qLicenseSerial,
                                                         @RequestParam("licenseExpirationMin") Optional<String> qLicenseExpirationMin,
                                                         @RequestParam("licenseExpirationMax") Optional<String> qLicenseExpirationMax,
                                                         @RequestParam("speciality") Optional<String> qSpeciality,
                                                         @RequestParam("salaryMin") Optional<String> qSalaryMin,
                                                         @RequestParam("salaryMax") Optional<String> qSalaryMax) {

        RestResult<ReportDto<GisManDTO>> result = new RestResult<>();
        try {
            Integer pageSize = this.validationService.validatePageSize(qPageSize);
            Integer pageNumber = this.validationService.validatePageNumber(qPageNumber);
            List<SortOption> sortBy = this.validationService.validateSortBy(qSortBy);
            Boolean export = this.validationService.validateExport(qExport);
            Pair<Class, Object> query = this.validationService.validateOptionalGeneral(qQuery);
            Boolean confirm = this.validationService.validateOptionalBoolean(qConfirm);
            Boolean active = this.validationService.validateOptionalBoolean(qActive);
            Boolean block = this.validationService.validateOptionalBoolean(qBlock);
            Boolean selfRegister = this.validationService.validateOptionalBoolean(qSelfRegister);
            String username = this.userValidationService.validateOptionalUsername(qUsername);
            String firstname = this.validationService.validateOptionalPersianString(qFirstname);
            String lastname = this.validationService.validateOptionalPersianString(qLastname);
            String nationalId = this.userValidationService.validateOptionalNationalId(qNationalId);
            String mobile = this.validationService.validateOptionalMobile(qMobile);
            String phone = this.validationService.validateOptionalPhoneFax(qPhone);
            String email = this.validationService.validateOptionalEmail(qEmail);
            String father = this.validationService.validateOptionalPersianString(qFather);
            String address = this.validationService.validateOptionalPersianString(qAddress);
            String zipCode = this.validationService.validateOptionalZipCode(qZipCode);
            Timestamp birthdayMin = this.validationService.validateOptionalTimestamp(qBirthdayMin);
            Timestamp birthdayMax = this.validationService.validateOptionalTimestamp(qBirthdayMax);
            SexType sexType = this.userValidationService.validateOptionalSexType(qSex);
            Integer creditMin = this.validationService.validateOptionalInteger(qCreditMin);
            Integer creditMax = this.validationService.validateOptionalInteger(qCreditMax);
            String speciality = this.validationService.validateOptionalPersianString(qSpeciality);
            Integer salaryMin = this.validationService.validateOptionalInteger(qSalaryMin);
            Integer salaryMax = this.validationService.validateOptionalInteger(qSalaryMax);

            ReportDto<GisManDTO> report = this.userService.reportGisManSimple(pageSize, pageNumber, sortBy, export, query,
                    confirm, active, block, selfRegister, username, firstname, lastname, nationalId, mobile, phone,
                    email, father, address, zipCode, birthdayMin, birthdayMax, sexType, creditMin, creditMax, speciality,
                    salaryMin, salaryMax);
            result.setObject(report);

        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }
    /* ************************************************************************************** */

    @PostMapping(path = "/add/customer", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Integer> addCustomer(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<Integer> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(PASSWORD))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, PASSWORD, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(FIRST_NAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, FIRST_NAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(LAST_NAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LAST_NAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(MOBILE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, MOBILE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(SEX))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, SEX, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            String password = this.userValidationService.validatePassword(postParameters.get(PASSWORD).toString());
            String firstname = this.validationService.validatePersianString(postParameters.get(FIRST_NAME).toString());
            String lastname = this.validationService.validatePersianString(postParameters.get(LAST_NAME).toString());
            String mobile = this.validationService.validateMobile(postParameters.get(MOBILE).toString());
            SexType sex = this.userValidationService.validateSexType(postParameters.get(SEX).toString());
            AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
            String email = null;
            if (postParameters.containsKey(EMAIL) && postParameters.get(EMAIL) != null)
                email = this.validationService.validateEmail(postParameters.get(EMAIL).toString());

            Pair<Integer, Integer> userId_validationCode = this.userService.addCustomer(false, username,mobile,email,authRequestHeader.getClientSerialNumber());
            this.userService.sendValidationMessage(mobile, email, userId_validationCode.getSecond());

            PersonEntity user = this.userService.get(userId_validationCode.getFirst());
            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.ADD_USER, user.getPersonid(), null);
            this.userHistoryService.updateInfo(operator.getPersonid(), PersonStateType.ADD_USER, user);
            result.setObject(userId_validationCode.getFirst());
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/confirm/customer", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> confirmCustomer(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(VALIDATION_CODE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, VALIDATION_CODE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            Integer validationCode = this.validationService.validateInteger(postParameters.get(VALIDATION_CODE).toString());

            PersonEntity user = this.availableService.getUserByUsername(username, false, true, validationCode);
            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.confirm(user.getPersonid());
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.CONFIRM, user.getPersonid(), null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/add/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Integer> addUser(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<Integer> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(ROLE_CATEGORY))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, ROLE_CATEGORY, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(ROLE_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, ROLE_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(PASSWORD))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, PASSWORD, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(FIRST_NAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, FIRST_NAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(LAST_NAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LAST_NAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(MOBILE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, MOBILE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(SEX))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, SEX, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(NATIONAL_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, NATIONAL_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(CITY_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, CITY_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(ADDRESS))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, ADDRESS, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(ZIP_CODE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, ZIP_CODE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            RoleCategoryType roleCategory = this.securityValidationService.validateRoleCategoryType(postParameters.get(ROLE_CATEGORY).toString());
            Integer roleId = this.validationService.validateInteger(postParameters.get(ROLE_ID).toString());
            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            String password = this.userValidationService.validatePassword(postParameters.get(PASSWORD).toString());
            String firstname = this.validationService.validatePersianString(postParameters.get(FIRST_NAME).toString());
            String lastname = this.validationService.validatePersianString(postParameters.get(LAST_NAME).toString());
            String mobile = this.validationService.validateMobile(postParameters.get(MOBILE).toString());
            SexType sex = this.userValidationService.validateSexType(postParameters.get(SEX).toString());
            String nationalId = this.userValidationService.validateNationalId(postParameters.get(NATIONAL_ID).toString());
            Integer cityId = this.validationService.validateInteger(postParameters.get(CITY_ID).toString());
            String address = this.validationService.validatePersianString(postParameters.get(ADDRESS).toString());
            String zipCode = this.validationService.validateZipCode(postParameters.get(ZIP_CODE).toString());

            String email = null;
            String phone = null;
            String father = null;
            Timestamp birthday = null;
            if (postParameters.containsKey(EMAIL) && postParameters.get(EMAIL) != null)
                email = this.validationService.validateEmail(postParameters.get(EMAIL).toString());
            if (postParameters.containsKey(PHONE) && postParameters.get(PHONE) != null)
                phone = this.validationService.validatePhoneFax(postParameters.get(PHONE).toString());
            if (postParameters.containsKey(FATHER) && postParameters.get(FATHER) != null)
                father = this.validationService.validatePersianString(postParameters.get(FATHER).toString());
            if (postParameters.containsKey(BIRTHDAY) && postParameters.get(BIRTHDAY) != null)
                birthday = this.validationService.validateTimestamp(postParameters.get(BIRTHDAY).toString());

            Pair<Integer, Integer> userId_validationCode = this.userService.addUser(roleCategory, roleId, username,
                    password, firstname, lastname, mobile, email, phone, nationalId, father, birthday, sex, cityId,
                    address, zipCode);
//            this.userService.sendValidationMessage(mobile, email, userId_validationCode.getSecond());

            PersonEntity user = this.userService.get(userId_validationCode.getFirst());
            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.ADD_USER, user.getPersonid(), null);
            this.userHistoryService.updateInfo(operator.getPersonid(), PersonStateType.ADD_USER, user);
            result.setObject(userId_validationCode.getFirst());
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/upload/picture", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public RestResult<String> uploadPicture(@RequestParam("picture") MultipartFile picture,
                                            @RequestParam(USER_ID) String userId,
                                            HttpServletRequest request) {
        RestResult<String> result = new RestResult<>();
        try {
            Integer uid = this.validationService.validateInteger(userId);
            PersonEntity personEntity = this.userService.get(uid);
            String pictureUrl = this.profileService.updatePicture(personEntity.getPersonid(), picture);

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.UPDATE_PICTURE, uid, null);
            result.setObject(pictureUrl);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/confirm", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> confirm(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.confirm(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.CONFIRM, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/unconfirm", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> unConfirm(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.unConfirm(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.UN_CONFIRM, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/activate", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> activate(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.activate(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.ACTIVATE, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/deActivate", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> deActivate(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.deActivate(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.DE_ACTIVATE, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/block", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> block(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.block(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.BLOCK, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/unBlock", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> unBlock(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.unBlock(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.UN_BLOCK, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> logout(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.logout(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.LOGOUT, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> delete(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(REASON_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, REASON_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            Integer reasonId = this.validationService.validateInteger(postParameters.get(REASON_ID).toString());

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userService.logicalDelete(userId);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.LOGICAL_DELETE, userId, reasonId);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************** */

    @PostMapping(path = "/get/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<PersonDto> getUser(@RequestBody Map<String, Object> postParameters) {
        RestResult<PersonDto> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            PersonEntity user = this.userService.get(userId);
            result.setObject(new PersonDto(user));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/edit/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> editInfo(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(FIRST_NAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, FIRST_NAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(LAST_NAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, LAST_NAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(MOBILE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, MOBILE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            String firstname = this.validationService.validatePersianString(postParameters.get(FIRST_NAME).toString());
            String lastname = this.validationService.validatePersianString(postParameters.get(LAST_NAME).toString());
            String mobile = this.validationService.validateMobile(postParameters.get(MOBILE).toString());

            String email = null;
            String phone = null;
            String nationalId = null;
            String father = null;
            Timestamp birthday = null;
            Integer cityId = null;
            String address = null;
            String zipCode = null;

            if (postParameters.containsKey(EMAIL) && postParameters.get(EMAIL) != null)
                email = this.validationService.validateEmail(postParameters.get(EMAIL).toString());
            if (postParameters.containsKey(PHONE) && postParameters.get(PHONE) != null)
                phone = this.validationService.validatePhoneFax(postParameters.get(PHONE).toString());
            if (postParameters.containsKey(NATIONAL_ID) && postParameters.get(NATIONAL_ID) != null)
                nationalId = this.userValidationService.validateNationalId(postParameters.get(NATIONAL_ID).toString());
            if (postParameters.containsKey(FATHER) && postParameters.get(FATHER) != null)
                father = this.validationService.validatePersianString(postParameters.get(FATHER).toString());
            if (postParameters.containsKey(BIRTHDAY) && postParameters.get(BIRTHDAY) != null)
                birthday = this.validationService.validateTimestamp(postParameters.get(BIRTHDAY).toString());
            if (postParameters.containsKey(CITY_ID) && postParameters.get(CITY_ID) != null)
                cityId = this.validationService.validateInteger(postParameters.get(CITY_ID).toString());
            if (postParameters.containsKey(ADDRESS) && postParameters.get(ADDRESS) != null)
                address = this.validationService.validatePersianString(postParameters.get(ADDRESS).toString());
            if (postParameters.containsKey(ZIP_CODE) && postParameters.get(ZIP_CODE) != null)
                zipCode = this.validationService.validateZipCode(postParameters.get(ZIP_CODE).toString());

            this.userService.editUser(userId, firstname, lastname, mobile, email, phone, nationalId, father,
                    birthday, cityId, address, zipCode);

            PersonEntity user = this.userService.get(userId);
            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.UPDATE_INFO, user.getPersonid(), null);
            this.userHistoryService.updateInfo(operator.getPersonid(), PersonStateType.UPDATE_INFO, user);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/edit/password", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> editPassword(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USER_ID))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USER_ID, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(PASSWORD))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, PASSWORD, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            Integer userId = this.validationService.validateInteger(postParameters.get(USER_ID).toString());
            String password = this.userValidationService.validatePassword(postParameters.get(PASSWORD).toString());

            this.userService.editPassword(userId, password);

            PersonEntity operator = this.accessService.getLoggedInUser(request);
            this.userHistoryService.updateState(operator.getPersonid(), PersonStateType.UPDATE_PASSWORD, userId, null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

}
