package ir.matin.user.api;

import ir.matin.database.data.type.PersonStateType;
import ir.matin.database.entity.PersonEntity;
import ir.matin.security.bl.AccessService;
import ir.matin.security.bl.HeaderService;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.security.data.type.ClientType;
import ir.matin.user.bl.ProfileService;
import ir.matin.user.bl.UserHistoryService;
import ir.matin.user.bl.UserValidationService;
import ir.matin.user.data.dto.PersonDto;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.Element;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Map;

/**
 * The User Profile Controller and Rest Api with Mapping of {@literal "/rest/user/profile"},
 * Containing Methods about User Profile Rest Apis
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@RestController
@RequestMapping(path = "/rest/user/profile")
public class UserProfileController {

    /**
     * The {@link ValidationService} Instance Representing Validation Service
     */
    @Autowired
    private ValidationService validationService;

    /**
     * The {@link UserValidationService} Instance Representing User Validation Service
     */
    @Autowired
    private UserValidationService userValidationService;

    @Autowired
    private AccessService accessService;

    @Autowired
    private HeaderService headerService;

    /**
     * The {@link ProfileService} Instance Representing Profile Based Service
     */
    @Autowired
    private ProfileService profileService;
    @Autowired
    private UserHistoryService userHistoryService;

    @GetMapping(path = "/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<Element> status(HttpServletRequest request) {
        RestResult<Element> result = new RestResult<>();
        try {
            PersonEntity personEntity = this.accessService.getLoggedInUser(request);
            //String fullName = personEntity.getFirstname().concat(" ").concat(personEntity.getLastname());
            String username = personEntity.getUsername();
            result.setObject(new Element(personEntity.getPersonid(), username, personEntity.getPicture()));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#GET} and Mapping of {@literal "/rest/user/profile/logout"} for
     * Logging out a User in System
     * <ul><li> Possible {@link SystemException} in Function Call of {@link AccessService#getLoggedInUser(HttpServletRequest)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link ProfileService#logout(PersonEntity, ClientType)} </li></ul>
     *
     * @param request A {@link HttpServletRequest} Instance Representing Http Request,
     *                This Parameter is Fetched implicitly and There is no Need to Send this Parameter to Function,
     *                Also is Used for getting Logged in User from Request Header
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     */
    @PostMapping(path = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> logout(HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = this.accessService.getLoggedInUser(request);
            AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
            this.profileService.logout(personEntity, authRequestHeader.getClientType());

            this.userHistoryService.updateState(null, PersonStateType.LOGOUT, personEntity.getPersonid(), null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#GET} and Mapping of {@literal "/rest/user/profile/show"} for
     * Getting Profile of Current User
     * <ul><li> Possible {@link SystemException} in Function Call of {@link AccessService#getLoggedInUser(HttpServletRequest)} </li></ul>
     *
     * @param request A {@link HttpServletRequest} Instance Representing Http Request,
     *                This Parameter is Fetched implicitly and There is no Need to Send this Parameter to Function,
     *                Also is Used for getting Logged in User from Request Header
     * @return A {@link PersonDto} Instance Representing Current User's Profile
     */
    @GetMapping(path = "/show", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<PersonDto> show(HttpServletRequest request) {
        RestResult<PersonDto> result = new RestResult<>();
        try {
            PersonEntity personEntity = this.accessService.getLoggedInUser(request);
            result.setObject(new PersonDto(personEntity));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/user/profile/edit"} for
     * Editing Fields of Current User's Profile
     * <ul><li> Possible {@link SystemException} in Function Call of {@link AccessService#getLoggedInUser(HttpServletRequest)} </li>
     * <li> Possible {@link SystemException} in Function Call of </li></ul>
     *
     * @param postParameters A {@link Map} Instance Representing {@link RequestBody} with no Required Fields
     * @param request        A {@link HttpServletRequest} Instance Representing Http Request,
     *                       This Parameter is Fetched implicitly and There is no Need to Send this Parameter to Function,
     *                       Also is Used for getting Logged in User from Request Header
     * @return A {@link PersonDto} Instance Representing Edited Version of Current User's Profile
     */
    @PostMapping(path = "/update/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<PersonDto> updateInfo(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<PersonDto> result = new RestResult<>();
        try {
            PersonEntity personEntity = this.accessService.getLoggedInUser(request);

            String mobile = null;
            String firstname = null;
            String lastname = null;
            String email = null;
            String phone = null;
            String nationalId = null;
            String father = null;
            Timestamp birthday = null;
            Integer cityId = null;
            String address = null;
            String zipCode = null;
            Integer sex = null;

            if (postParameters.containsKey("firstname") && postParameters.get("firstname") != null)
                mobile = this.validationService.validateMobile(postParameters.get("mobile").toString());
            if (postParameters.containsKey("firstname") && postParameters.get("firstname") != null)
                firstname = this.validationService.validatePersianString(postParameters.get("firstname").toString());
            if (postParameters.containsKey("lastname") && postParameters.get("lastname") != null)
                lastname = this.validationService.validatePersianString(postParameters.get("lastname").toString());
            if (postParameters.containsKey("email") && postParameters.get("email") != null)
                email = this.validationService.validateEmail(postParameters.get("email").toString());
            if (postParameters.containsKey("phone") && postParameters.get("phone") != null)
                phone = this.validationService.validatePhoneFax(postParameters.get("phone").toString());
            if (postParameters.containsKey("nationalId") && postParameters.get("nationalId") != null)
                nationalId = this.userValidationService.validateNationalId(postParameters.get("nationalId").toString());
            if (postParameters.containsKey("father") && postParameters.get("father") != null)
                father = this.validationService.validatePersianString(postParameters.get("father").toString());
            if (postParameters.containsKey("birthday") && postParameters.get("birthday") != null)
                birthday = this.validationService.validateTimestamp(postParameters.get("birthday").toString());
            if (postParameters.containsKey("cityId") && postParameters.get("cityId") != null)
                cityId = this.validationService.validateInteger(postParameters.get("cityId").toString());
            if (postParameters.containsKey("address") && postParameters.get("address") != null)
                address = this.validationService.validatePersianString(postParameters.get("address").toString());
            if (postParameters.containsKey("zipCode") && postParameters.get("zipCode") != null)
                zipCode = this.validationService.validateZipCode(postParameters.get("zipCode").toString());
            if (postParameters.containsKey("sex") && postParameters.get("sex") != null)
                sex = this.validationService.validateInteger(postParameters.get("sex").toString());

            PersonEntity editedPersonEntity = this.profileService.updateInfo(personEntity, firstname, lastname, mobile,
                    email, phone, nationalId, father, birthday, cityId, address, zipCode,sex);

            this.userHistoryService.updateState(null, PersonStateType.UPDATE_INFO, personEntity.getPersonid(), null);
            this.userHistoryService.updateInfo(null, PersonStateType.UPDATE_INFO, personEntity);
            result.setObject(new PersonDto(editedPersonEntity));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/user/profile/password/change"} for
     * Changing PASSWORD of Current User
     * <ul><li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validatePassword(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link AccessService#getLoggedInUser(HttpServletRequest)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link ProfileService#updatePassword(PersonEntity, String, String)} </li></ul>
     *
     * @param postParameters A {@link Map} Instance Representing {@link RequestBody} with These Required Fields
     *                       <ul><li> {@literal oldPassword}: A {@link String} Instance Representing User's Old PASSWORD </li>
     *                       <li> {@literal newPassword}: A {@link String} Instance Representing User's New PASSWORD </li></ul>
     * @param request        A {@link HttpServletRequest} Instance Representing Http Request,
     *                       This Parameter is Fetched implicitly and There is no Need to Send this Parameter to Function,
     *                       Also is Used for getting Logged in User from Request Header
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#REQUIRED_FIELDS_NOT_SET} when Required Fields are not Set
     */
    @PostMapping(path = "/update/password", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> updatePassword(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            PersonEntity personEntity = this.accessService.getLoggedInUser(request);
            if (!postParameters.containsKey("oldPassword"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "oldPassword", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey("newPassword"))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "newPassword", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String oldPassword = this.userValidationService.validatePassword(postParameters.get("oldPassword").toString());
            String newPassword = this.userValidationService.validatePassword(postParameters.get("newPassword").toString());

            this.profileService.updatePassword(personEntity, oldPassword, newPassword);

            this.userHistoryService.updateState(null, PersonStateType.UPDATE_PASSWORD, personEntity.getPersonid(), null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/user/profile/email/change"} for
     * Changing Email of Current User
     * <ul><li> Possible {@link SystemException} in Function Call of {@link ValidationService#validateEmail(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link AccessService#getLoggedInUser(HttpServletRequest)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link ProfileService#updatePassword(PersonEntity, String, String)} </li></ul>
     *
     * @param request A {@link HttpServletRequest} Instance Representing Http Request,
     *                This Parameter is Fetched implicitly and There is no Need to Send this Parameter to Function,
     *                Also is Used for getting Logged in User from Request Header
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#REQUIRED_FIELDS_NOT_SET} when Required Fields are not Set
     */
    @PostMapping(path = "/update/picture", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public RestResult<String> updatePicture(@RequestParam("picture") MultipartFile picture, HttpServletRequest request) {
        RestResult<String> result = new RestResult<>();
        try {
            PersonEntity personEntity = this.accessService.getLoggedInUser(request);
            String pictureUrl = this.profileService.updatePicture(personEntity.getPersonid(), picture);

            this.userHistoryService.updateState(null, PersonStateType.UPDATE_PICTURE, personEntity.getPersonid(), null);
            result.setObject(pictureUrl);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}
