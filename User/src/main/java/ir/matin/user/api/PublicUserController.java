package ir.matin.user.api;

import ir.matin.database.data.type.DeviceStateType;
import ir.matin.database.data.type.PersonStateType;
import ir.matin.database.entity.DeviceEntity;
import ir.matin.database.entity.GuestEntity;
import ir.matin.database.entity.PersonEntity;
import ir.matin.security.bl.AvailableService;
import ir.matin.security.bl.HeaderService;
import ir.matin.security.bl.JwtService;
import ir.matin.security.data.object.AuthRequestHeader;
import ir.matin.user.bl.*;
import ir.matin.user.data.dto.DeviceDto;
import ir.matin.user.data.dto.GuestDto;
import ir.matin.user.data.dto.LoginDto;
import ir.matin.user.data.type.SexType;
import ir.matin.utility.bl.ValidationService;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * The Public Controller and Rest Api with Mapping of {@literal "/rest/public"},
 * Containing Methods about Public Rest Apis
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@RestController
@RequestMapping(path = "/rest/public/user")
public class PublicUserController {
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstname";
    private static final String LAST_NAME = "lastname";
    private static final String MOBILE = "mobile";
    private static final String SEX = "sex";
    private static final String VALIDATION_CODE = "validationCode";
    private static final String RESET_CODE = "resetCode";
    private static final String CLIENT_CATEGORY = "clientCategory";

    /**
     * The {@link ValidationService} Instance Representing Validation Service
     */
    @Autowired
    private ValidationService validationService;

    /**
     * The {@link UserValidationService} Instance Representing User Validation Service
     */
    @Autowired
    private UserValidationService userValidationService;

    @Autowired
    private HeaderService headerService;

    /**
     * The {@link JwtService} Instance Representing Json Web Token Service
     */
    @Autowired
    private JwtService jwtService;

    /**
     * The {@link UserService} Instance Representing User Based Service
     */
    @Autowired
    private UserService userService;
    @Autowired
    private GuestService guestService;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private ProfileService profileService;

    @Autowired
    private AvailableService availableService;
    @Autowired
    private UserHistoryService userHistoryService;
    @Autowired
    private DeviceHistoryService deviceHistoryService;
    /* ************************************************************************************************************** */

    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/public/login"} for
     * Logging in a User in System
     * <ul><li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validateUsername(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validatePassword(String)} </li></ul>
     * <li> Possible {@link SystemException} in Function Call of {@link JwtService#create(Integer, AuthRequestHeader)}  </li></ul>
     *
     * @param  {@link Map} Instance Representing {@link RequestBody} with These Required Fields
     *                       <ul><li> {@literal username}: A {@link String} Instance Representing Username of User </li>
     *                       <li> {@literal password}: A {@link String} Instance Representing PASSWORD of User </li></ul>
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#REQUIRED_FIELDS_NOT_SET} when Required Fields are not Set
     */
    /*@PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<LoginDto> login(@RequestBody Map<String, Object> postParameters, HttpServletRequest request) {
        RestResult<LoginDto> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(PASSWORD))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, PASSWORD, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            String password = this.userValidationService.validatePassword(postParameters.get(PASSWORD).toString());

            PersonEntity personEntity = this.availableService.getUserByLogin(username,
                    authRequestHeader.getClientSerialNumber(), authRequestHeader.getClientType());
            this.profileService.login(personEntity, password, authRequestHeader.getClientSerialNumber(),
                    authRequestHeader.getClientType());
            Pair<String, String> tokens = this.jwtService.create(personEntity.getPersonid(), authRequestHeader);

            this.userHistoryService.updateState(null, PersonStateType.LOGIN, personEntity.getPersonid(), null);
            result.setObject(new LoginDto(tokens.getFirst(), tokens.getSecond(), personEntity));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }*/

    @PostMapping(path = "/register/device", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DeviceDto> registerDevice(HttpServletRequest request) {
        RestResult<DeviceDto> result = new RestResult<>();
        try {
            AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
            Pair<Integer, Integer> userId_validationCode = this.deviceService.addDevice(authRequestHeader.getClientSerialNumber());
            DeviceEntity device = this.deviceService.getDevice(userId_validationCode.getFirst());
            this.deviceHistoryService.updateState(device.getDeviceid(), DeviceStateType.REGISTER_DEVICE);
            this.deviceHistoryService.updateInfo(DeviceStateType.REGISTER_DEVICE, device);
            //this.profileService.login(personEntity, password, authRequestHeader.getClientSerialNumber(),
            //        authRequestHeader.getClientType());

            this.deviceService.login(device, authRequestHeader.getClientSerialNumber(),authRequestHeader.getClientType());
            Pair<String, String> tokens = this.jwtService.create(device.getDeviceid(), authRequestHeader);
            this.deviceHistoryService.updateState(device.getDeviceid(), DeviceStateType.LOGIN);
            result.setObject(new DeviceDto(tokens.getFirst(), tokens.getSecond(), device));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }

    @PostMapping(path = "/register/guest", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<GuestDto> registerGuest(@RequestBody Map<String, Object> postParameters,HttpServletRequest request) {
        RestResult<GuestDto> result = new RestResult<>();
        try {
            if (!(postParameters.containsKey("guest")&&postParameters.get("guest").equals("1")))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, "guest", SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
            Pair<Integer, Integer> userId_validationCode = this.guestService.addGuest(authRequestHeader.getClientSerialNumber());
            GuestEntity guest = this.guestService.getGuest(userId_validationCode.getFirst());
            //this.deviceHistoryService.updateState(guest.getPersonid(), DeviceStateType.REGISTER_DEVICE);
            //this.deviceHistoryService.updateInfo(DeviceStateType.REGISTER_DEVICE, guest);
            //this.profileService.login(personEntity, password, authRequestHeader.getClientSerialNumber(),
            //        authRequestHeader.getClientType());
            PersonEntity personEntity = this.availableService.getGuestbySerialNumber(authRequestHeader.getClientSerialNumber(), authRequestHeader.getClientType());
            this.profileService.login(personEntity,authRequestHeader.getClientSerialNumber(),
                    authRequestHeader.getClientType());
            Pair<String, String> tokens = this.jwtService.create(guest.getPersonid(), authRequestHeader);
            this.deviceHistoryService.updateState(guest.getPersonid(), DeviceStateType.LOGIN);
            result.setObject(new GuestDto(tokens.getFirst(), tokens.getSecond(), guest));
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }

        return result;
    }
    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/public/refresh/{type}"} for
     * Refreshing Access Token with Refresh Token
     * <ul><li> Possible {@link SystemException} in Function Call of {@link HeaderService#extractAuthTokenClient(HttpServletRequest)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link JwtService#refresh(AuthRequestHeader)} </li></ul>
     *
     * @param request A {@link HttpServletRequest} Instance Representing Http Request,
     *                This Parameter is Fetched implicitly and There is no Need to Send this Parameter to Function,
     *                Also is Used for getting Refresh Token in Response Header
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     */
    @GetMapping(path = "/refresh", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<String> refresh(HttpServletRequest request) {
        RestResult<String> result = new RestResult<>();
        try {
            AuthRequestHeader authRequestHeader = this.headerService.extractAuthTokenClient(request);
            String accessToken = this.jwtService.refresh(authRequestHeader);

            result.setObject(accessToken);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************************************** */

    @PostMapping(path = "/register/customer", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> registerCustomer(@RequestBody Map<String, Object> postParameters,HttpServletRequest request) {
        RestResult<DefaultResult> result = new RestResult<>();
            try {
                if (!postParameters.containsKey(MOBILE))
                    throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, MOBILE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
                String mobile = this.validationService.validateMobile(postParameters.get(MOBILE).toString());
                String username = mobile;
                String email = null;
                AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
                if (postParameters.containsKey(EMAIL) && postParameters.get(EMAIL) != null)
                    email = this.validationService.validateEmail(postParameters.get(EMAIL).toString());
                Pair<Integer, Integer> userId_validationCode = this.userService.addCustomer(true, mobile,username, email,authRequestHeader.getClientSerialNumber());
                this.userService.sendValidationMessage(mobile, email, userId_validationCode.getSecond());

                PersonEntity user = this.userService.get(userId_validationCode.getFirst());
                this.userHistoryService.updateState(null, PersonStateType.REGISTER_USER, user.getPersonid(), null);
                this.userHistoryService.updateInfo(null, PersonStateType.REGISTER_USER, user);
                result.setObject(DefaultResult.SUCCESS);
            } catch (SystemException e) {
                result.setError(e.getUserError());
                result.setDescription(e.getDescription());
            }
        return result;
    }

    @PostMapping(path = "/reset/confirmation", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> resetConfirmation(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());

            PersonEntity personEntity = this.availableService.getUserByUsername(username, false, true);
            Integer validationCode = this.profileService.resetValidation(personEntity);
            this.userService.sendValidationMessage(personEntity.getMobile(), null, validationCode);

            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/reset/activation", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> resetActivation(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());

            PersonEntity personEntity = this.availableService.getUserByUsername(username, true, false);
            Integer validationCode = this.profileService.resetValidation(personEntity);
            this.userService.sendValidationMessage(personEntity.getMobile(), null, validationCode);

            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/confirm", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<LoginDto> confirm(@RequestBody Map<String, Object> postParameters,HttpServletRequest request) {
        RestResult<LoginDto> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(VALIDATION_CODE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, VALIDATION_CODE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            AuthRequestHeader authRequestHeader = this.headerService.extractClient(request);
            Integer validationCode = this.validationService.validateInteger(postParameters.get(VALIDATION_CODE).toString());

            PersonEntity personEntity = this.availableService.getCustomerbySerialNumber(authRequestHeader.getClientSerialNumber(),authRequestHeader.getClientType());
            this.profileService.confirm(personEntity);

            this.userHistoryService.updateState(null, PersonStateType.CONFIRM, personEntity.getPersonid(), null);
            // add login block to return token
            this.profileService.login(personEntity,authRequestHeader.getClientSerialNumber(),
                    authRequestHeader.getClientType());
            Pair<String, String> tokens = this.jwtService.create(personEntity.getPersonid(), authRequestHeader);

            this.userHistoryService.updateState(null, PersonStateType.LOGIN, personEntity.getPersonid(), null);
            result.setObject(new LoginDto(tokens.getFirst(), tokens.getSecond(), personEntity));
            // end login block

        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    @PostMapping(path = "/activate", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> activate(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(VALIDATION_CODE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, VALIDATION_CODE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            Integer validationCode = this.validationService.validateInteger(postParameters.get(VALIDATION_CODE).toString());

            PersonEntity personEntity = this.availableService.getUserByUsername(username, true, false, validationCode);
            this.profileService.activate(personEntity);

            this.userHistoryService.updateState(null, PersonStateType.ACTIVATE, personEntity.getPersonid(), null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /* ************************************************************************************************************** */

    @PostMapping(path = "/reset/validation", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> resetValidation(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());

            PersonEntity personEntity = this.availableService.getUserByUsername(username, true, true);
            Integer validationCode = this.profileService.resetValidation(personEntity);
            this.userService.sendValidationMessage(personEntity.getMobile(), null, validationCode);

            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/public/reset/login"} for
     * Resetting LOGIN Status of User and Log Out Him/Her
     * <ul><li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validateUsername(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validatePassword(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link ValidationService#validateInteger(String)} </li></ul>
     *
     * @param postParameters A {@link Map} Instance Representing {@link RequestBody} with These Required Fields
     *                       <ul><li> {@literal username}: A {@link String} Instance Representing Username of User </li>
     *                       <li> {@literal resetCode}: A {@link String} Instance Representing Reset Code of User's Account </li></ul>
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#REQUIRED_FIELDS_NOT_SET} when Required Fields are not Set
     */
    @PostMapping(path = "/reset/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> resetLogin(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(RESET_CODE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, RESET_CODE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            Integer resetCode = this.validationService.validateInteger(postParameters.get(RESET_CODE).toString());

            PersonEntity personEntity = this.availableService.getUserByUsername(username, true, true, resetCode);
            this.profileService.resetLogin(personEntity);

            this.userHistoryService.updateState(null, PersonStateType.LOGOUT, personEntity.getPersonid(), null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }

    /**
     * A Rest Api with Type of {@link RequestMethod#POST} and Mapping of {@literal "/rest/public/reset/password"} for
     * Resetting PASSWORD of User with Given PASSWORD and Log Out Him/Her
     * <ul><li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validateUsername(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link UserValidationService#validatePassword(String)} </li>
     * <li> Possible {@link SystemException} in Function Call of {@link ValidationService#validateInteger(String)} </li></ul>
     *
     * @param postParameters A {@link Map} Instance Representing {@link RequestBody} with These Required Fields
     *                       <ul><li> {@literal username}: A {@link String} Instance Representing Username of User </li>
     *                       <li> {@literal password}: A {@link String} Instance Representing PASSWORD of User </li>
     *                       <li> {@literal resetCode}: A {@link String} Instance Representing Reset Code of User's Account </li></ul>
     * @return A {@link DefaultResult} Instance Representing String Value of Default Result ({@link DefaultResult#SUCCESS} & {@link DefaultResult#FAILURE})
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#REQUIRED_FIELDS_NOT_SET} when Required Fields are not Set
     */
    @PostMapping(path = "/reset/password", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> resetPassword(@RequestBody Map<String, Object> postParameters) {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            if (!postParameters.containsKey(USERNAME))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, USERNAME, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(PASSWORD))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, PASSWORD, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());
            if (!postParameters.containsKey(RESET_CODE))
                throw new SystemException(SystemError.REQUIRED_FIELDS_NOT_SET, RESET_CODE, SystemError.REQUIRED_FIELDS_NOT_SET.getValue());

            String username = this.userValidationService.validateUsername(postParameters.get(USERNAME).toString());
            String password = this.userValidationService.validatePassword(postParameters.get(PASSWORD).toString());
            Integer resetCode = this.validationService.validateInteger(postParameters.get(RESET_CODE).toString());

            PersonEntity personEntity = this.availableService.getUserByUsername(username, true, true, resetCode);
            this.profileService.resetPassword(personEntity, password);

            this.userHistoryService.updateState(null, PersonStateType.RESET_PASSWORD, personEntity.getPersonid(), null);
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}
