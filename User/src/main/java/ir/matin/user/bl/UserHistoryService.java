package ir.matin.user.bl;

import ir.matin.database.dao.HisPersonInfoDao;
import ir.matin.database.dao.HisPersonStateDao;
import ir.matin.database.data.type.PersonStateType;
import ir.matin.database.entity.HisPersonInfoEntity;
import ir.matin.database.entity.HisPersonStateEntity;
import ir.matin.database.entity.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class UserHistoryService {

    @Autowired
    private HisPersonStateDao hisPersonStateDao;
    @Autowired
    private HisPersonInfoDao hisPersonInfoDao;

    public Integer updateState(Integer operatorId, PersonStateType personStateType, Integer personId,
                               Integer typePersonReasonId) {
//        if (operatorId != null) {
//            OperatorEntity operatorEntity = this.operatorDao.get(operatorId);
//            if (operatorEntity == null)
//                throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
//        }
        Timestamp now = new Timestamp(new Date().getTime());
        HisPersonStateEntity hisPersonStateEntity = new HisPersonStateEntity();
        hisPersonStateEntity.setOperatorID(operatorId);
        hisPersonStateEntity.setTime(now);
        hisPersonStateEntity.setState(personStateType.getValue());

        hisPersonStateEntity.setPersonID(personId);
        hisPersonStateEntity.setTypePersonReasonID(typePersonReasonId);

        return this.hisPersonStateDao.add(hisPersonStateEntity);
    }

    public Integer updateInfo(Integer operatorId, PersonStateType personStateType, PersonEntity personEntity) {
//        if (operatorId != null) {
//            OperatorEntity operatorEntity = this.operatorDao.get(operatorId);
//            if (operatorEntity == null)
//                throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
//        }
        Timestamp now = new Timestamp(new Date().getTime());
        HisPersonInfoEntity hisPersonInfoEntity = new HisPersonInfoEntity();
        hisPersonInfoEntity.setOperatorID(operatorId);
        hisPersonInfoEntity.setTime(now);
        hisPersonInfoEntity.setState(personStateType.getValue());

        hisPersonInfoEntity.setPersonID(personEntity.getPersonid());
        hisPersonInfoEntity.setUsername(personEntity.getUsername());
        hisPersonInfoEntity.setPassword(personEntity.getPassword());
        hisPersonInfoEntity.setConfirm(personEntity.getConfirm());
        hisPersonInfoEntity.setActive(personEntity.getActive());
        hisPersonInfoEntity.setBlock(personEntity.getBlock());
        hisPersonInfoEntity.setLogin(personEntity.getLogin());
        hisPersonInfoEntity.setSelfRegister(personEntity.getSelfregister());
        hisPersonInfoEntity.setValidationCode(personEntity.getValidationcode());
        hisPersonInfoEntity.setValidationExpireTime(personEntity.getValidationexpiretime());
        hisPersonInfoEntity.setClientSerialNumber(personEntity.getClientserialnumber());
        hisPersonInfoEntity.setClientType(personEntity.getClienttype());
        hisPersonInfoEntity.setNationalID(personEntity.getNationalid());
        hisPersonInfoEntity.setFirstName(personEntity.getFirstname());
        hisPersonInfoEntity.setLastName(personEntity.getLastname());
        hisPersonInfoEntity.setMobile(personEntity.getMobile());
        hisPersonInfoEntity.setPhone(personEntity.getPhone());
        hisPersonInfoEntity.setEmail(personEntity.getEmail());
        hisPersonInfoEntity.setFather(personEntity.getFather());
        hisPersonInfoEntity.setTypeCityID(personEntity.getTypecityid());
        hisPersonInfoEntity.setAddress(personEntity.getAddress());
        hisPersonInfoEntity.setZipcode(personEntity.getZipcode());
        hisPersonInfoEntity.setBirthday(personEntity.getBirthday());
        hisPersonInfoEntity.setSex(personEntity.getSex());
        hisPersonInfoEntity.setPicture(personEntity.getPicture());
        hisPersonInfoEntity.setTotalCredit(personEntity.getTotalcredit());
        hisPersonInfoEntity.setDeleted(personEntity.getDeleted());

        return this.hisPersonInfoDao.add(hisPersonInfoEntity);
    }

}
