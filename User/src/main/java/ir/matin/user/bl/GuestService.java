package ir.matin.user.bl;

import ir.matin.database.dao.GuestDao;
import ir.matin.database.dao.PersonDao;
import ir.matin.database.dao.RoleDao;
import ir.matin.database.entity.GuestEntity;
import ir.matin.database.entity.PersonEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.security.data.type.ClientType;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.utility.bl.MailSmsService;
import ir.matin.utility.data.object.Pair;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;

/**
 * The User Service Class,
 * Containing Methods about User Management
 *
 * @author Pouria shiri
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class GuestService {
    @Autowired
    private GuestDao guestDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PersonDao personDao;
    @Autowired
    private UserValidationService userValidationService;

    /**
     * The {@link MailSmsService} Instance Representing Mail and Sms Service
     */
    /* ****************************************************************************************************************** */



    public GuestEntity getGuest(Integer id) {
        GuestEntity guestEntity = this.guestDao.get(id);
        if (guestEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        return guestEntity;
    }
    /* ****************************************************************************************************************** */
    public Pair<Integer, Integer> addGuest(String guestserialnumber) {
        PersonEntity personEntity = this.personDao.getGuestBySerialNumber(guestserialnumber);
        if (personEntity != null){
            return new Pair<>(personEntity.getPersonid(), null);
            //throw new SystemException(SystemError.USERNAME_ALREADY_EXIST, SystemError.USERNAME_ALREADY_EXIST.getValue());
        }
        Integer random = this.userValidationService.generateActivationCode();
        //Timestamp expirationTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(UserConstant.ACTIVATION_CODE_EXPIRATION_MINUTES));

        GuestEntity guestEntity = new GuestEntity();
        guestEntity.setClientserialnumber(guestserialnumber);
        guestEntity.setClienttype("m");
        guestEntity.setLogin(false);
        guestEntity.setDeleted(false);
        guestEntity.setConfirm(true);
        guestEntity.setBlock(false);
        guestEntity.setActive(true);
        guestEntity.setGuest(true);
        //customerEntity.setValidationexpiretime(expirationTime);

        RoleEntity guestRole = this.roleDao.getByName(SecurityConstant.ROLE_ADMIN_COMPANY);
        guestEntity.setRoles(new HashSet<>(Collections.singleton(guestRole)));

        Integer userId = this.guestDao.add(guestEntity);
        return new Pair<>(userId, random);
    }

    public void login(GuestEntity guestEntity, Long clientSerialNumber, ClientType clientType) {
        //String securePassword = this.hashService.hash(password);
        //if (!securePassword.equals(personEntity.getPassword()))
        //    throw new SystemException(SystemError.USERNAME_PASSWORD_NOT_MATCH, SystemError.USERNAME_PASSWORD_NOT_MATCH.getValue());

        //if (clientType == ClientType.MOBILE)
            this.guestDao.login(guestEntity.getPersonid(), clientSerialNumber);
    }
}