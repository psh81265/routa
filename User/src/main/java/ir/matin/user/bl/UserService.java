package ir.matin.user.bl;

import ir.matin.database.bl.ReportPersonService;
import ir.matin.database.dao.*;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.*;
import ir.matin.security.data.type.RoleCategoryType;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.user.data.dto.GisManDTO;
import ir.matin.user.data.dto.OperatorDto;
import ir.matin.user.data.type.SexType;
import ir.matin.user.data.type.UserConstant;
import ir.matin.utility.bl.HashService;
import ir.matin.utility.bl.MailSmsService;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The User Service Class,
 * Containing Methods about User Management
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class UserService {
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PersonDao personDao;
    @Autowired
    private OperatorDao operatorDao;
    @Autowired
    private GISManDao gisManDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private UserValidationService userValidationService;
    @Autowired
    private HashService hashService;

    /**
     * The {@link MailSmsService} Instance Representing Mail and Sms Service
     */
    @Autowired
    private MailSmsService mailSmsService;

    @Autowired
    private ReportPersonService reportPersonService;

    /* ****************************************************************************************************************** */

    public PersonEntity get(Integer id) {
        PersonEntity personEntity = this.personDao.get(id);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        return personEntity;
    }

    public List<OperatorEntity> listOperators() {
        List<OperatorEntity> operators = this.operatorDao.list();
        if (operators.isEmpty())
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());
        return operators;
    }

    /* ****************************************************************************************************************** */

    public ReportDto<OperatorDto> reportOperatorSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                       Boolean confirm, Boolean active, Boolean block, Boolean selfRegister,
                                                       String username, String firstname, String lastname, String nationalId,
                                                       String mobile, String phone, String email, String father, String address,
                                                       String zipCode, Timestamp birthdayMin, Timestamp birthdayMax,
                                                       SexType sexType, Integer creditMin, Integer creditMax, String speciality,
                                                       Integer salaryMin, Integer salaryMax) {
        Integer sex = (sexType != null) ? ((sexType == SexType.MAN) ? 1 : 0) : null;
        ReportResult<OperatorEntity> report = this.reportPersonService.reportOperatorSimple(pageSize, pageNumber, sortBy, export, query,
                confirm, active, block, selfRegister, username, firstname, lastname, nationalId, mobile, phone,
                email, father, address, zipCode, birthdayMin, birthdayMax, sex, creditMin, creditMax, speciality,
                salaryMin, salaryMax);

        if (report.getCountAll() == 0 || report.getResult() == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        ReportDto<OperatorDto> reportDto = new ReportDto<>(report.getCountAll(), pageSize, pageNumber);
        reportDto.setResult(report.getResult().stream().map(OperatorDto::new).collect(Collectors.toList()));
        return reportDto;
    }

    public ReportDto<GisManDTO> reportGisManSimple(Integer pageSize, Integer pageNumber, List<SortOption> sortBy, Boolean export, Pair<Class, Object> query,
                                                     Boolean confirm, Boolean active, Boolean block, Boolean selfRegister,
                                                     String username, String firstname, String lastname, String nationalId,
                                                     String mobile, String phone, String email, String father, String address,
                                                     String zipCode, Timestamp birthdayMin, Timestamp birthdayMax,
                                                     SexType sexType, Integer creditMin, Integer creditMax, String speciality,
                                                     Integer salaryMin, Integer salaryMax) {
        Integer sex = (sexType != null) ? ((sexType == SexType.MAN) ? 1 : 0) : null;
        ReportResult<GISManEntity> report = this.reportPersonService.reportGisManSimple(pageSize, pageNumber, sortBy, export, query,
                confirm, active, block, selfRegister, username, firstname, lastname, nationalId, mobile, phone,
                email, father, address, zipCode, birthdayMin, birthdayMax, sex, creditMin, creditMax, speciality,
                salaryMin, salaryMax);

        if (report.getCountAll() == 0 || report.getResult() == null)
            throw new SystemException(SystemError.EMPTY_RESULTS, SystemError.EMPTY_RESULTS.getValue());

        ReportDto<GisManDTO> reportDto = new ReportDto<>(report.getCountAll(), pageSize, pageNumber);
        reportDto.setResult(report.getResult().stream().map(GisManDTO::new).collect(Collectors.toList()));
        return reportDto;
    }

    /* ****************************************************************************************************************** */

    public Pair<Integer, Integer> addCustomer(Boolean selfRegister,String mobile,String username,String email,String clientSerialnumber) {
        Integer random = this.userValidationService.generateActivationCode();
        PersonEntity personEntity = this.personDao.getByUsername(username);
        if (personEntity != null){
            updatePersonEntity(personEntity,random);
            return new Pair<>(personEntity.getPersonid(), random);
        }
        Timestamp expirationTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(UserConstant.ACTIVATION_CODE_EXPIRATION_MINUTES));
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setConfirm(false);
        customerEntity.setActive(true);
        customerEntity.setBlock(false);
        customerEntity.setLogin(false);
        customerEntity.setSelfregister(selfRegister);
        customerEntity.setTotalcredit(0);
        customerEntity.setDeleted(false);
        customerEntity.setClientserialnumber(clientSerialnumber);
        customerEntity.setUsername(username);
        customerEntity.setMobile(mobile);
        customerEntity.setValidationcode(random);
        customerEntity.setValidationexpiretime(expirationTime);
        customerEntity.setGuest(false);

        RoleEntity customerRole = this.roleDao.getByName(SecurityConstant.ROLE_CUSTOMER);
        customerEntity.setRoles(new HashSet<>(Collections.singleton(customerRole)));

        Integer userId = this.customerDao.add(customerEntity);
        return new Pair<>(userId, random);
    }

    private void updatePersonEntity(PersonEntity personEntity, Integer random) {
        Timestamp expirationTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(UserConstant.ACTIVATION_CODE_EXPIRATION_MINUTES));
        CustomerEntity customerEntity = (CustomerEntity)personEntity;
        customerEntity.setConfirm(false);
        customerEntity.setActive(true);
        customerEntity.setBlock(false);
        customerEntity.setLogin(false);
        customerEntity.setSelfregister(false);
        customerEntity.setTotalcredit(0);
        customerEntity.setDeleted(false);
        customerEntity.setValidationcode(random);
        customerEntity.setValidationexpiretime(expirationTime);
        this.personDao.update(customerEntity);
    }

    public Pair<Integer, Integer> addUser(RoleCategoryType roleCategory, Integer roleId,
                                          String username, String password, String firstname, String lastname,
                                          String mobile, String email, String phone, String nationalId, String father,
                                          Timestamp birthday, SexType sexType, Integer cityId, String address, String zipCode) {
        if (this.personDao.getByUsername(username) != null)
            throw new SystemException(SystemError.USERNAME_ALREADY_EXIST, SystemError.USERNAME_ALREADY_EXIST.getValue());
//        if (this.personDao.getByTypeMobile(PersonType.Customer, mobile) != null) {
//            throw new SystemException(SystemError.USER_MOBILE_ALREADY_EXIST, SystemError.USER_MOBILE_ALREADY_EXIST.getValue());
//        }
        String securePassword = this.hashService.hash(password);
        Integer random = this.userValidationService.generateActivationCode();
        Timestamp expirationTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(UserConstant.ACTIVATION_CODE_EXPIRATION_MINUTES));
        Integer sex = (sexType == SexType.MAN) ? 1 : 0;

        Integer userId = null;
        if (roleCategory == RoleCategoryType.ADMIN_OWNER || roleCategory == RoleCategoryType.OPERATOR_OWNER ||
                roleCategory == RoleCategoryType.ADMIN_COMPANY || roleCategory == RoleCategoryType.OPERATOR_COMPANY) {
            OperatorEntity operatorEntity = new OperatorEntity();
            operatorEntity.setConfirm(true);
            operatorEntity.setActive(true);
            operatorEntity.setBlock(false);
            operatorEntity.setLogin(false);
            operatorEntity.setSelfregister(false);
            operatorEntity.setTotalcredit(0);
            operatorEntity.setDeleted(false);

            operatorEntity.setUsername(username);
            operatorEntity.setPassword(securePassword);
            operatorEntity.setFirstname(firstname);
            operatorEntity.setLastname(lastname);
            operatorEntity.setMobile(mobile);
            operatorEntity.setEmail(email);
            operatorEntity.setPhone(phone);
            operatorEntity.setNationalid(nationalId);
            operatorEntity.setFather(father);
            operatorEntity.setBirthday(birthday);
            operatorEntity.setSex(sex);
            operatorEntity.setTypecityid(cityId);
            operatorEntity.setAddress(address);
            operatorEntity.setZipcode(zipCode);

            operatorEntity.setValidationcode(random);
            operatorEntity.setValidationexpiretime(expirationTime);

            RoleEntity operatorRole = this.roleDao.get(roleId);
            operatorEntity.setRoles(new HashSet<>(Collections.singleton(operatorRole)));

            userId = this.operatorDao.add(operatorEntity);
        } else if (roleCategory == RoleCategoryType.GISMAN) {
            GISManEntity gisManEntity = new GISManEntity();
            gisManEntity.setConfirm(true);
            gisManEntity.setActive(true);
            gisManEntity.setBlock(false);
            gisManEntity.setLogin(false);
            gisManEntity.setSelfregister(false);
            gisManEntity.setTotalcredit(0);
            gisManEntity.setDeleted(false);

            gisManEntity.setUsername(username);
            gisManEntity.setPassword(securePassword);
            gisManEntity.setFirstname(firstname);
            gisManEntity.setLastname(lastname);
            gisManEntity.setMobile(mobile);
            gisManEntity.setEmail(email);
            gisManEntity.setPhone(phone);
            gisManEntity.setNationalid(nationalId);
            gisManEntity.setFather(father);
            gisManEntity.setBirthday(birthday);
            gisManEntity.setSex(sex);
            gisManEntity.setTypecityid(cityId);
            gisManEntity.setAddress(address);
            gisManEntity.setZipcode(zipCode);

            gisManEntity.setValidationcode(random);
            gisManEntity.setValidationexpiretime(expirationTime);

            RoleEntity gisManRole = this.roleDao.get(roleId);
            gisManEntity.setRoles(new HashSet<>(Collections.singleton(gisManRole)));

            userId = this.gisManDao.add(gisManEntity);
        }

        return new Pair<>(userId, random);
    }

    public void editUser(Integer userId, String firstname, String lastname, String mobile,
                         String email, String phone, String nationalId, String father,
                         Timestamp birthday, Integer cityId, String address, String zipCode) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());

        personEntity.setFirstname(firstname);
        personEntity.setLastname(lastname);
        personEntity.setMobile(mobile);
        personEntity.setEmail(email);
        personEntity.setPhone(phone);
        personEntity.setNationalid(nationalId);
        personEntity.setFather(father);
        personEntity.setBirthday(birthday);
        personEntity.setTypecityid(cityId);
        personEntity.setAddress(address);
        personEntity.setZipcode(zipCode);

        this.personDao.update(personEntity);
    }

    public void editPassword(Integer userId, String password) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());

        String securePassword = this.hashService.hash(password);
        this.personDao.updatePassword(userId, securePassword);
    }

    /* ****************************************************************************************************************** */

    public void confirm(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.confirm(userId);
    }

    public void unConfirm(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.unConfirm(userId);
    }

    public void activate(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.activate(userId);
    }

    public void deActivate(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.deActivate(userId);
    }

    public void block(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.block(userId);
    }

    public void unBlock(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.unBlock(userId);
    }

    public void logout(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.logout(userId);
    }

    public void logicalDelete(Integer userId) {
        PersonEntity personEntity = this.personDao.get(userId);
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        this.personDao.logicalDelete(userId);
    }

    /* ****************************************************************************************************************** */

    public void sendValidationMessage(String mobile, String email, Integer validationCode) {
        this.mailSmsService.sendSms(mobile, validationCode.toString());
        if (email != null) {
            String subject = "به روتا خوش آمدید";
            String content = "کد فعال سازی کاربر روتا: " + validationCode;
            String[] recipients = {email};
            this.mailSmsService.sendMailText(new MailData(subject, content, recipients));
        }
    }
}