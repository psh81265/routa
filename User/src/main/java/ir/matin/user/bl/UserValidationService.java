package ir.matin.user.bl;

import ir.matin.user.data.type.SexType;
import ir.matin.user.data.type.UserConstant;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class UserValidationService {
    /**
     * Validate Username of User
     *
     * @param value A {@link String} Instance Representing Username of User
     * @return A {@link String} Instance Representing Validated username
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_DATA_TYPE} when username is not valid
     */
    public String validateUsername(String value) {
        if (value == null || value.equals("") || !value.matches(UserConstant.USERNAME_PATTERN))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    /**
     * Validate PASSWORD of User
     *
     * @param value A {@link String} Instance Representing PASSWORD of User
     * @return A {@link String} Instance Representing Validated password
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_DATA_TYPE} when password is not valid
     */
    public String validatePassword(String value) {
        if (value == null || value.equals("") || !value.matches(UserConstant.PASSWORD_PATTERN))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        return value;
    }

    /**
     * Validate Person Type
     *
     * @param value A {@link String} Instance Representing Sex Type
     * @return A {@link SexType} Instance Representing Validated Sex Type
     * @throws SystemException A Customized {@link RuntimeException} with type of {@link SystemError#INVALID_DATA_TYPE} when sexType is not valid
     */
    public SexType validateSexType(String value) {
        if (value == null || value.equals(""))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        switch (value.toLowerCase()) {
            case "m":
            case "man":
            case "male":
                return SexType.MAN;
            case "w":
            case "woman":
            case "female":
                return SexType.WOMAN;
            default:
                throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
        }
    }

    public String validateNationalId(String value) {
        if (value == null || value.equals("") || !value.matches(UserConstant.NATIONALID_PATTERN))
            throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());

        if (value.length() == 8) value = "00" + value;
        else if (value.length() == 9) value = "0" + value;
        int sum = 0;
        for (int i = 0; i < 9; i++)
            sum += Integer.parseInt(value.substring(i, i + 1)) * (10 - i);
        sum = sum % 11;
        int parity = Integer.parseInt(value.substring(9, 10));
        if ((sum < 2 && parity == sum) || (sum >= 2 && parity == (11 - sum)))
            return value;
        throw new SystemException(SystemError.INVALID_DATA_TYPE, SystemError.INVALID_DATA_TYPE.getValue());
    }

    /* ****************************************************************************************************************** */

    public String validateOptionalUsername(Optional<String> value) {
        return value.map(this::validateUsername).orElse(null);
    }

    public String validateOptionalPassword(Optional<String> value) {
        return value.map(this::validatePassword).orElse(null);
    }

    public SexType validateOptionalSexType(Optional<String> value) {
        return value.map(this::validateSexType).orElse(null);
    }

    public String validateOptionalNationalId(Optional<String> value) {
        return value.map(this::validateNationalId).orElse(null);
    }

    /* ****************************************************************************************************************** */

    public int generateActivationCode() {
        return ThreadLocalRandom.current().nextInt(UserConstant.ACTIVATION_CODE_MINIMUM_VALUE,
                UserConstant.ACTIVATION_CODE_MAXIMUM_VALUE);
    }

}
