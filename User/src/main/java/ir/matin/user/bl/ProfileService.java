package ir.matin.user.bl;

import ir.matin.database.dao.PersonDao;
import ir.matin.database.entity.PersonEntity;
import ir.matin.security.data.type.ClientType;
import ir.matin.user.data.type.UserConstant;
import ir.matin.utility.bl.FileService;
import ir.matin.utility.bl.HashService;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.UtilityConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * The Profile Service Class,
 * Containing Methods about Users Profiles
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class ProfileService {
    @Autowired
    private PersonDao personDao;
    @Autowired
    private HashService hashService;
    @Autowired
    private UserValidationService userValidationService;

    /**
     * The {@link FileService} Instance Representing File Service
     */
    @Autowired
    private FileService fileService;

    /* ****************************************************************************************************************** */
    /* public rests */
    /* ****************************************************************************************************************** */

    public void login(PersonEntity personEntity,String clientSerialNumber, ClientType clientType) {
        this.personDao.login(personEntity.getPersonid(), clientSerialNumber);
    }

    public void confirm(PersonEntity personEntity) {
        this.personDao.confirm(personEntity.getPersonid());
    }

    public void activate(PersonEntity personEntity) {
        this.personDao.activate(personEntity.getPersonid());
    }

    public Integer resetValidation(PersonEntity personEntity) {
        int random = this.userValidationService.generateActivationCode();
        Timestamp expirationTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(UserConstant.ACTIVATION_CODE_EXPIRATION_MINUTES));

        this.personDao.updateValidation(personEntity.getPersonid(), random, expirationTime);
        return random;
    }

    public void resetLogin(PersonEntity personEntity) {
        this.personDao.logout(personEntity.getPersonid());
    }

    public void resetPassword(PersonEntity personEntity, String password) {
        String securePassword = this.hashService.hash(password);
        this.personDao.updatePassword(personEntity.getPersonid(), securePassword, false);
    }

    /* ****************************************************************************************************************** */
    /* auth rests */
    /* ****************************************************************************************************************** */

    public void logout(PersonEntity personEntity, ClientType clientType) {
        if (clientType == ClientType.MOBILE)
            this.personDao.logout(personEntity.getPersonid());
    }

    public PersonEntity updateInfo(PersonEntity personEntity, String firstname, String lastname, String mobile,
                                   String email, String phone, String nationalId, String father,
                                   Timestamp birthday, Integer cityId, String address, String zipCode,int sex) {
        personEntity.setFirstname(firstname);
        personEntity.setLastname(lastname);
        personEntity.setMobile(mobile);
        personEntity.setEmail(email);
        personEntity.setPhone(phone);
        personEntity.setNationalid(nationalId);
        personEntity.setFather(father);
        personEntity.setBirthday(birthday);
        personEntity.setTypecityid(cityId);
        personEntity.setAddress(address);
        personEntity.setZipcode(zipCode);
        personEntity.setSex(sex);

        this.personDao.update(personEntity);
        return personEntity;
    }

    public void updatePassword(PersonEntity personEntity, String oldPassword, String newPassword) {
        String secureOldPassword = this.hashService.hash(oldPassword);
        if (!personEntity.getPassword().equals(secureOldPassword))
            throw new SystemException(SystemError.USERNAME_PASSWORD_NOT_MATCH, SystemError.USERNAME_PASSWORD_NOT_MATCH.getValue());
        String secureNewPassword = this.hashService.hash(newPassword);
        this.personDao.updatePassword(personEntity.getPersonid(), secureNewPassword, true);
    }

    public String updatePicture(Integer personId, MultipartFile picture) {
        String fileName = this.fileService.storeFile(picture, personId, UtilityConstant.BASE_IMAGE_FOLDER + "/users");
        String pictureUrl = "users/" + fileName;
        this.personDao.updatePicture(personId, pictureUrl);
        return pictureUrl;
    }

}