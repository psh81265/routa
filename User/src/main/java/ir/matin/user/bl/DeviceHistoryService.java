package ir.matin.user.bl;
/**
 * Created by pooria on 12/6/2018.
 */

import ir.matin.database.dao.DeviceInfoDao;
import ir.matin.database.dao.DeviceStateDao;
import ir.matin.database.data.type.DeviceStateType;
import ir.matin.database.data.type.PersonStateType;
import ir.matin.database.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class DeviceHistoryService {

    @Autowired
    private DeviceStateDao deviceStateDao;
    @Autowired
    private DeviceInfoDao deviceInfoDao;

    public Integer updateState(Integer deviceId, DeviceStateType deviceStateType) {
//        if (operatorId != null) {
//            OperatorEntity operatorEntity = this.operatorDao.get(operatorId);
//            if (operatorEntity == null)
//                throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
//        }
        Timestamp now = new Timestamp(new Date().getTime());
        DeviceStateEntity deviceStateEntity = new DeviceStateEntity();
        deviceStateEntity.setDeviceID(deviceId);
        deviceStateEntity.setTime(now);
        deviceStateEntity.setState(deviceStateType.getValue());

        return this.deviceStateDao.add(deviceStateEntity);
    }



    public Integer updateInfo(DeviceStateType deviceStateType, DeviceEntity deviceEntity) {
//        if (operatorId != null) {
//            OperatorEntity operatorEntity = this.operatorDao.get(operatorId);
//            if (operatorEntity == null)
//                throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
//        }
        Timestamp now = new Timestamp(new Date().getTime());
        DeviceInfoEntity deviceInfoEntity = new DeviceInfoEntity();
        deviceInfoEntity.setTime(now);
        deviceInfoEntity.setState(deviceStateType.getValue());

        deviceInfoEntity.setDeviceID(deviceEntity.getDeviceid());
        deviceInfoEntity.setLogin(deviceEntity.getLogin());
        deviceInfoEntity.setDeleted(deviceEntity.getDeleted());
        deviceInfoEntity.setDeviceSerialNumber(deviceEntity.getDeviceserialnumber());
        deviceInfoEntity.setDeviceType(deviceEntity.getDevicetype());

        return this.deviceInfoDao.add(deviceInfoEntity);
    }
}
