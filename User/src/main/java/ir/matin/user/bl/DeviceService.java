package ir.matin.user.bl;

import ir.matin.database.bl.ReportPersonService;
import ir.matin.database.dao.*;
import ir.matin.database.data.object.ReportResult;
import ir.matin.database.entity.*;
import ir.matin.security.data.type.ClientType;
import ir.matin.security.data.type.RoleCategoryType;
import ir.matin.security.data.type.SecurityConstant;
import ir.matin.user.data.dto.GisManDTO;
import ir.matin.user.data.dto.OperatorDto;
import ir.matin.user.data.type.SexType;
import ir.matin.user.data.type.UserConstant;
import ir.matin.utility.bl.HashService;
import ir.matin.utility.bl.MailSmsService;
import ir.matin.utility.data.dto.ReportDto;
import ir.matin.utility.data.object.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The User Service Class,
 * Containing Methods about User Management
 *
 * @author Pouria shiri
 * @version 1.0
 * @since 2016-09-22
 */
@Service
public class DeviceService {
    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserValidationService userValidationService;

    /**
     * The {@link MailSmsService} Instance Representing Mail and Sms Service
     */
    /* ****************************************************************************************************************** */



    public DeviceEntity getDevice(Integer id) {
        DeviceEntity deviceEntity = this.deviceDao.get(id);
        if (deviceEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.DATA_NOT_FOUND.getValue());
        return deviceEntity;
    }
    /* ****************************************************************************************************************** */
    public Pair<Integer, Integer> addDevice(String deviceserialnumber) {
        if (this.deviceDao.getBySerialNumber(deviceserialnumber) != null)
            throw new SystemException(SystemError.USERNAME_ALREADY_EXIST, SystemError.USERNAME_ALREADY_EXIST.getValue());

        Integer random = this.userValidationService.generateActivationCode();
        //Timestamp expirationTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(UserConstant.ACTIVATION_CODE_EXPIRATION_MINUTES));

        DeviceEntity deviceEntity = new DeviceEntity();
        deviceEntity.setDeviceserialnumber(deviceserialnumber);
        deviceEntity.setDevicetype("m");
        deviceEntity.setLogin(false);
        deviceEntity.setDeleted(false);
        //customerEntity.setValidationexpiretime(expirationTime);

        RoleEntity deviceRole = this.roleDao.getByName(SecurityConstant.ROLE_ADMIN_COMPANY);
        deviceEntity.setRoles(new HashSet<>(Collections.singleton(deviceRole)));

        Integer userId = this.deviceDao.add(deviceEntity);
        return new Pair<>(userId, random);
    }

    public void login(DeviceEntity deviceEntity, String clientSerialNumber, ClientType clientType) {
        //String securePassword = this.hashService.hash(password);
        //if (!securePassword.equals(personEntity.getPassword()))
        //    throw new SystemException(SystemError.USERNAME_PASSWORD_NOT_MATCH, SystemError.USERNAME_PASSWORD_NOT_MATCH.getValue());

        //if (clientType == ClientType.MOBILE)
            this.deviceDao.login(deviceEntity.getDeviceid(), clientSerialNumber);
    }
}