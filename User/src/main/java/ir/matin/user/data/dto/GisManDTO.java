package ir.matin.user.data.dto;

import ir.matin.database.entity.GISManEntity;
import ir.matin.database.entity.PersonEntity;

import java.io.Serializable;

public class GisManDTO extends PersonDto implements Serializable {
    public GisManDTO(GISManEntity gisManEntity) {
        super(gisManEntity);

    }
}
