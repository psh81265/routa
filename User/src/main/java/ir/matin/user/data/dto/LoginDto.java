package ir.matin.user.data.dto;

import ir.matin.database.entity.PersonEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

public class LoginDto implements Serializable {

    private String accessToken;
    private String refreshToken;
    private Set<String> roles;
    private String roleCategory;
    private Boolean manageView;
//    private Integer companyId;

    private String firstname;
    private String lastname;
    private String mobile;
    private String email;
    private String picture;
    private Integer totalCredit;

    public LoginDto(String accessToken, String refreshToken, PersonEntity personEntity) {
        if (personEntity == null)
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        //this.roles = personEntity.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toSet());
        this.roleCategory = "CUSTOMER";
        this.manageView = false;
        for (RoleEntity roleEntity : personEntity.getRoles()) {
            this.roleCategory = roleEntity.getCategory();
            this.manageView = roleEntity.getManageview();
        }

        this.firstname = personEntity.getFirstname();
        this.lastname = personEntity.getLastname();
        this.mobile = personEntity.getMobile();
        this.email = personEntity.getEmail();
        this.picture = personEntity.getPicture();
        this.totalCredit = (personEntity.getTotalcredit() != null) ? personEntity.getTotalcredit() : 0;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getRoleCategory() {
        return roleCategory;
    }

    public void setRoleCategory(String roleCategory) {
        this.roleCategory = roleCategory;
    }

    public Boolean getManageView() {
        return manageView;
    }

    public void setManageView(Boolean manageView) {
        this.manageView = manageView;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Integer totalCredit) {
        this.totalCredit = totalCredit;
    }
}
