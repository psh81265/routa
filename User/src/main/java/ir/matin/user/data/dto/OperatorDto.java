package ir.matin.user.data.dto;

import ir.matin.database.entity.OperatorEntity;

import java.io.Serializable;

public class OperatorDto extends PersonDto implements Serializable {

    private String speciality;
    private Integer salary;

    public OperatorDto(OperatorEntity operatorEntity) {
        super(operatorEntity);

        this.speciality = operatorEntity.getSpeciality();
        this.salary = operatorEntity.getSalary();
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
