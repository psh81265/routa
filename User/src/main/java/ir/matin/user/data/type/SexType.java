package ir.matin.user.data.type;

public enum SexType {
    MAN(1),
    WOMAN(0);

    private final int value;

    SexType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
