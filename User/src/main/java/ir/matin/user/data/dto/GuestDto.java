package ir.matin.user.data.dto;

import ir.matin.database.entity.GuestEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by pooria on 12/12/2018.
 */
public class GuestDto implements Serializable {

    private String accessToken;
    private String refreshToken;
    private Set<String> roles;
    private String roleCategory;

    public GuestDto(String accessToken, String refreshToken, GuestEntity guestEntity) {
        if (guestEntity == null){
            throw new SystemException(SystemError.USER_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        }
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        //this.roles = personEntity.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toSet());
        this.roleCategory = "GUEST";
        //for (RoleEntity roleEntity : personEntity.getRoles()) {
        //    this.roleCategory = roleEntity.getCategory();
       //    this.manageView = roleEntity.getManageview();
       // }
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getRoleCategory() {
        return roleCategory;
    }

    public void setRoleCategory(String roleCategory) {
        this.roleCategory = roleCategory;
    }
}
