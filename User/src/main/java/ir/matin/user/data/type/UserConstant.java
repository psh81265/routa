package ir.matin.user.data.type;

import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

/**
 * User Constants,
 * All User Constants are Declared in UserConstant Class
 *
 * @author Bijan Ghahremani
 * @version 1.0
 * @since 2016-09-22
 */
public class UserConstant {

    public static final String NATIONALID_PATTERN = "(?=.*[0-9]).{8,10}";
    public static final String USERNAME_PATTERN = "(?=.*[0-9a-zA-Z]).{6,}";
    public static final String PASSWORD_PATTERN = "(?=.*[0-9a-zA-Z]).{6,}";
    public static final String SSN_PATTERN = "(?=.*[0-9])(?=.*[a-zA-Z]).{10,}";

    public static final int ACTIVATION_CODE_MINIMUM_VALUE = 1000;
    public static final int ACTIVATION_CODE_MAXIMUM_VALUE = 10000;
    public static final int ACTIVATION_CODE_EXPIRATION_MINUTES = 60;

    private UserConstant() {
        throw new SystemException(SystemError.ILLEGAL_CONSTRUCTOR, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
    }
}
