package ir.matin.user.data.dto;

import ir.matin.database.entity.PersonEntity;
import ir.matin.database.entity.RoleEntity;
import ir.matin.database.entity.TypeCityEntity;
import ir.matin.utility.data.object.SystemError;
import ir.matin.utility.data.object.SystemException;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;
import java.util.stream.Collectors;

public class PersonDto implements Serializable {

    private Integer personId;
    private String username;
    //    private String password;
    private Boolean confirm;
    private Boolean active;
    private Boolean block;
    private Boolean login;
    private Boolean selfRegister;
    //    private Integer validationCode;
//    private Timestamp validationExpireTime;
//    private Long clientSerialNumber;
//    private String clientType;
    private String nationalId;
    private String firstname;
    private String lastname;
    private String mobile;
    private String phone;
    private String email;
    private String father;
    private String province;
    private String city;
    private String address;
    private String zipCode;
    private Timestamp birthday;
    private Integer sex;
    private String picture;
    private Integer totalCredit;
    private Set<String> roles;

    public PersonDto(PersonEntity personEntity) {
        if (personEntity == null)
            throw new SystemException(SystemError.DATA_NOT_FOUND, SystemError.SERVER_ERROR, SystemError.SERVER_ERROR.getValue());
        this.personId = personEntity.getPersonid();
        this.username = personEntity.getUsername();

        this.confirm = personEntity.getConfirm();
        this.active = personEntity.getActive();
        this.block = personEntity.getBlock();
        this.login = personEntity.getLogin();
        this.selfRegister = personEntity.getSelfregister();

        this.nationalId = personEntity.getNationalid();
        this.firstname = personEntity.getFirstname();
        this.lastname = personEntity.getLastname();
        this.mobile = personEntity.getMobile();
        this.phone = personEntity.getPhone();
        this.email = personEntity.getEmail();
        this.father = personEntity.getFather();

        TypeCityEntity typeCityEntity = personEntity.getTypeCityEntity();
        if (typeCityEntity == null) {
            this.province = null;
            this.city = null;
        } else {
            this.province = typeCityEntity.getTypeProvinceEntity().getName();
            this.city = typeCityEntity.getName();
        }

        this.address = personEntity.getAddress();
        this.zipCode = personEntity.getZipcode();
        this.birthday = personEntity.getBirthday();
        this.sex = personEntity.getSex();
        this.picture = personEntity.getPicture();

        this.totalCredit = (personEntity.getTotalcredit() != null) ? personEntity.getTotalcredit() : 0;
        this.roles = personEntity.getRoles().stream().map(RoleEntity::getName).collect(Collectors.toSet());
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getConfirm() {
        return confirm;
    }

    public void setConfirm(Boolean confirm) {
        this.confirm = confirm;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    public Boolean getSelfRegister() {
        return selfRegister;
    }

    public void setSelfRegister(Boolean selfRegister) {
        this.selfRegister = selfRegister;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getTotalCredit() {
        return totalCredit;
    }

    public void setTotalCredit(Integer totalCredit) {
        this.totalCredit = totalCredit;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
