package ir.matin.application.config;

import ir.matin.database.entity.PersonEntity;
import ir.matin.security.bl.AccessService;
import ir.matin.utility.data.object.SystemException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;

import java.io.PrintWriter;
import java.io.StringWriter;

import static ir.matin.security.log.AuditLogService.*;
import static ir.matin.utility.bl.DevelopmentLogService.createCriticalLogPattern;
import static ir.matin.utility.bl.DevelopmentLogService.createServerErrorLogPattern;

@Aspect
public class AppLogger {

    private static final Logger auditLogger = LogManager.getLogger("audit");
    private static final Logger errorLogger = LogManager.getLogger("error");
    private static final Logger devLogger = LogManager.getLogger(AppLogger.class);

    @Autowired
    private AccessService accessService;

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController * || @org.springframework.stereotype.Controller *)")
    public void controller() {
    }

    @Pointcut("within(@org.springframework.stereotype.Service *)")
    public void service() {
    }

    @Pointcut("execution(public * *(..))")
    public void allMethod() {
    }


    @Before("controller() && args(..,request)")
    public void auditLog(JoinPoint point, HttpServletRequest request) {
        PersonEntity personEntity = this.accessService.getPublicUser(request);
        if (personEntity == null)
            auditLogger.log(NOTICE, createPublicLogPattern(request, point.getArgs()));
        else
            auditLogger.log(NOTICE, createAuthenticatedLogPattern(personEntity, request, point.getArgs()));
    }

    @AfterThrowing(pointcut = "(service() || controller()) && allMethod()", throwing = "exception")
    public void logAfterThrowing(JoinPoint joinPoint, Exception exception) {
        if (exception instanceof SystemException){
            devLogger.error(createCriticalLogPattern(joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), ((SystemException) exception).getArgument()) + ((SystemException) exception).getSystemError());
        }
        else if (exception instanceof AuthenticationException || exception instanceof AccessDeniedException){

        }
        else{
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            exception.printStackTrace(printWriter);
            String stackTrace = stringWriter.toString();
            errorLogger.log(Level.ERROR, createServerErrorLogPattern(joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), stackTrace));
        }

    }
}
