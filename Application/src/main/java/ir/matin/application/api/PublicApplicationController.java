package ir.matin.application.api;

import ir.matin.utility.data.object.RestResult;
import ir.matin.utility.data.object.SystemException;
import ir.matin.utility.data.type.DefaultResult;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/rest/public/application")
public class PublicApplicationController {

    @GetMapping(path = "/ok", produces = MediaType.APPLICATION_JSON_VALUE)
    public RestResult<DefaultResult> ok() {
        RestResult<DefaultResult> result = new RestResult<>();
        try {
            result.setObject(DefaultResult.SUCCESS);
        } catch (SystemException e) {
            result.setError(e.getUserError());
            result.setDescription(e.getDescription());
        }
        return result;
    }
}
